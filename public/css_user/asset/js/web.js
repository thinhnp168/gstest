$(document).on('click', '.labellop', function (e) {
    e.preventDefault();
   
    var id = $(this).attr('data-id');
    var ten = $(this).attr('data-ten');
    var tenbodau = $(this).attr('data-tenbodau');
    var apen = '<span  class="lopdaygs" data-id="'+id+'" data-ten="'+ten+'" data-tenbodau="'+tenbodau+'"><span class="icon icon-x xlopdaygs"></span>'+ten+'\
                    <input class="displaynone" type="checkbox" name="idlop[]" id="'+tenbodau+'" value="'+id+'" checked="">\
                </span>\
    ';
    $(".area-lopchon").append(apen);
    $(this).remove();
    if($('#chonlop').find('span').length != 0){
        $('#chonlop').addClass('show');
        $('#chonlop').parent('.dropdown').addClass('show');
        return false;
    }
})
$(document).on('click', '.xlopdaygs', function(e){
    e.preventDefault();
    var id = $(this).parent('.lopdaygs').attr('data-id');
    var ten = $(this).parent('.lopdaygs').attr('data-ten');
    var tenbodau = $(this).parent('.lopdaygs').attr('data-tenbodau');
    var apen = '<span class="labellop" data-ten="'+ten+'" data-tenbodau="'+tenbodau+'" data-id="'+id+'">'+ten+'</span>';
    $("#chonlop").append(apen);
    $(this).parent('.lopdaygs').remove();
    $(this).parent('.lopdaygs').children('.displaynone').remove();
})

$(document).on('click', '.labelmon', function (e) {
    e.preventDefault();
   
    var id = $(this).attr('data-id');
    var ten = $(this).attr('data-ten');
    var tenbodau = $(this).attr('data-tenbodau');
    var apen = '<span  class="mondaygs" data-id="'+id+'" data-ten="'+ten+'" data-tenbodau="'+tenbodau+'"><span class="icon icon-x xmondaygs"></span> '+ten+'\
                <input class="displaynone" type="checkbox" name="idmon[]" id="'+tenbodau+'" value="'+id+'" checked>\
                </span>\
    ';
    $(".area-monchon").append(apen);
    $(this).remove();
    if($('#chonmon').find('span').length != 0){
        $('#chonmon').addClass('show');
        $('#chonmon').parent('.dropdown').addClass('show');
        return false;
    }
    
})
$(document).on('click', '.xmondaygs', function(e){
    e.preventDefault();
    var id = $(this).parent('.mondaygs').attr('data-id');
    var ten = $(this).parent('.mondaygs').attr('data-ten');
    var tenbodau = $(this).parent('.mondaygs').attr('data-tenbodau');
    var apen = '<span class="labelmon" data-ten="'+ten+'" data-tenbodau="'+tenbodau+'"  data-id="'+id+'">'+ten+'</span>';
    $("#chonmon").append(apen);
    $(this).parent('.mondaygs').remove();
    $(this).parent('.mondaygs').children('.displaynone').remove();
})
$(document).on('click', '.labelquan', function (e) {
    e.preventDefault();
   
    var id = $(this).attr('data-id');
    var ten = $(this).attr('data-ten');
    var tenbodau = $(this).attr('data-tenbodau');
    var tentimkiem = $(this).attr('data-tentimkiem');
    var apen = '<span  class="quandaygs" data-id="'+id+'" data-ten="'+ten+'" data-tentimkiem="'+tentimkiem+'" data-tenbodau="'+tenbodau+'"><span class="icon icon-x xquandaygs"></span> '+ten+'\
                <input class="displaynone" type="checkbox" name="idquan[]" id="'+tenbodau+'" value="'+id+'" checked>\
                </span>\
    ';
    $(".area-quanchon").append(apen);
    $(this).remove();
 
    if($('#chonquan').find('span').length != 0){
        $('#chonquan').addClass('show');
        $('#chonquan').parent('.dropdown').addClass('show');
        return false;
    }
  
})
$(document).on('click', '.xquandaygs', function(){
    var id = $(this).parent('.quandaygs').attr('data-id');
    var ten = $(this).parent('.quandaygs').attr('data-ten');
    var tenbodau = $(this).parent('.quandaygs').attr('data-tenbodau');
    var tentimkiem = $(this).parent('.quandaygs').attr('data-tentimkiem');
    var apen = '<span class="labelquan" data-ten="'+ten+'" data-tenbodau="'+tenbodau+'" data-id="'+id+'">'+ten+'<p class="displaynone">'+tentimkiem+'</p></span>';
    $("#chonquan").append(apen);
    $(this).parent('.quandaygs').remove();
    $(this).parent('.quandaygs').children('.displaynone').remove();
})

function xoa_dau(str) {
    str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
    str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
    str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
    str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
    str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
    str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
    str = str.replace(/đ/g, "d");
    str = str.replace(/À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ/g, "A");
    str = str.replace(/È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ/g, "E");
    str = str.replace(/Ì|Í|Ị|Ỉ|Ĩ/g, "I");
    str = str.replace(/Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ/g, "O");
    str = str.replace(/Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ/g, "U");
    str = str.replace(/Ỳ|Ý|Ỵ|Ỷ|Ỹ/g, "Y");
    str = str.replace(/Đ/g, "D");
    return str;
}
function timquan1() {
    textvalue= $('#timquan').val();
    textvalue = xoa_dau(textvalue).toLowerCase();
    if(textvalue != ''){
        $('.labelquan').hide();
        $('.displaynone:contains("'+textvalue+'")').parents('.labelquan').show();
    }else{
        $('.labelquan').show();
    } 
}
function searchgiasu(){
    textvalue= $('#timgiasu').val();
    textvalue = xoa_dau(textvalue).toLowerCase();
    if(textvalue != ''){
        $('.item-giasu').hide();
        $('.displaynone:contains("'+textvalue+'")').parents('.item-giasu').show();
    }else{
        $('.item-giasu').show();
    } 
}
$(document).on('click','.btn-guithongtin',function(e){
    e.preventDefault();
    toastr.clear();
    toastr.options = {
        "closeButton": true,
        "timeOut": "5000",
        "positionClass": "toast-top-right"
    }
    const __this = this;
    const _token = $('input[name="_token"]').val();
    $(__this).html('Đang gửi thông tin...');
    $(__this).prop('disabled', true);
    ten = $('#ten').val();
    dienthoai = $('#dienthoai').val();
    email = $('#email').val();
    magioithieu = $('#magioithieu').val();
    bangcap = $('#bangcap').val();
    congtac = $('#congtac').val();
    thulao = $('#thulao').val();
    thongtin = $('#thongtin').val();
    var formData = new FormData();
    var file_data = $('#anh')[0].files[0];
    
    
    
    let arrQuan = []; let arrMon = [] ; let arrLop = []; 
    $('input[name="idquan[]"]:checked').each((k, v) => {
        arrQuan.push($(v).val())
    })    
    $('input[name="idmon[]"]:checked').each((k, v) => {
        arrMon.push($(v).val())
    })    
    $('input[name="idlop[]"]:checked').each((k, v) => {
        arrLop.push($(v).val())
    })   
    
    if(ten == ''){
        toastr.error('Tên không được để trống.');
        $(__this).html('Gửi thông tin');
        $(__this).prop('disabled', false);
        return false;
    }
    if(dienthoai == ''){
        toastr.error('Số điện thoại không được để trống.');
        $(__this).html('Gửi thông tin');
        $(__this).prop('disabled', false);
        return false;
    }
    if(email == ''){
        toastr.error('Email không được để trống.');
        $(__this).html('Gửi thông tin');
        $(__this).prop('disabled', false);
        return false;
    }
    if(bangcap == '' || bangcap === undefined){
        toastr.error('Học vị, bằng cấp không được để trống.');
        $(__this).html('Gửi thông tin');
        $(__this).prop('disabled', false);
        return false;
    }
    if(congtac == ''){
        toastr.error('Đơn vị công tác không được để trống.');
        $(__this).html('Gửi thông tin');
        $(__this).prop('disabled', false);
        return false;
    }
    if(thulao == ''){
        toastr.error('Mức thù lao không được để trống.');
        $(__this).html('Gửi thông tin');
        $(__this).prop('disabled', false);
        return false;
    }
    if(thongtin == ''){
        toastr.error('Thông tin gia sư không được để trống.');
        $(__this).html('Gửi thông tin');
        $(__this).prop('disabled', false);
        return false;
    }
    if(arrQuan.length == 0){
        toastr.error('Bạn phải chọn ít nhất 1 khu vực có thể dạy.');
        $(__this).html('Gửi thông tin');
        $(__this).prop('disabled', false);
        return false;
    }
    if(arrMon.length == 0){
        toastr.error('Bạn phải chọn ít nhất 1 môn học có thể dạy.');
        $(__this).html('Gửi thông tin');
        $(__this).prop('disabled', false);
        return false;
    }
    if(arrLop.length == 0){
        toastr.error('Bạn phải chọn ít nhất 1 lớp có thể dạy.');
        $(__this).html('Gửi thông tin');
        $(__this).prop('disabled', false);
        return false;
    }
    
    if(thulao<=0){
        thulao =1;
    }
    formData.append("anh", file_data);
    formData.append("ten", ten);
    formData.append("dienthoai", dienthoai);
    formData.append("email", email);
    formData.append("magioithieu", magioithieu);
    formData.append("bangcap", bangcap);
    formData.append("congtac", congtac);
    formData.append("thulao", thulao);
    formData.append("thongtin", thongtin);
    formData.append("idquan", arrQuan);
    formData.append("idmon", arrMon);
    formData.append("idlop", arrLop);
    data_ = formData;
    var request = $.ajax({
        url: "dangkygiasu",
        type: "POST",
        data: data_,
        dataType: "json",
        processData: false,
        contentType: false,
        headers: {
            'X-CSRF-TOKEN': $('input[name="_token"]').val()
        }
    });
    request.done(function (msg) {
       if(msg.type == 1){
            toastr.success(msg.mess);
            setTimeout(function(){ 
                window.location='/camon';
             }, 2000);
       }else if(msg.type == 0){
            toastr.error(msg.mess);
            $(__this).html('Gửi thông tin');
            $(__this).prop('disabled', false);
       }else if(msg.type == 2){
            toastr.error(msg.mess);
            $(__this).html('Gửi thông tin');
            $(__this).prop('disabled', false);
       }else if(msg.type == 3){
            toastr.error(msg.mess);
            $(__this).html('Gửi thông tin');
            $(__this).prop('disabled', false);
       }
    });
    request.fail(function (jqXHR, textStatus) {
        alert("Không thể gửi yêu cầu mã lỗi : " + textStatus);
    });

    request.always(function(){
        $(__this).html('Gửi thông tin');
    })
})

$(document).on('click','.btn-guiyeucau',function(e){
    e.preventDefault();
    toastr.clear();
    toastr.options = {
        "closeButton": true,
        "timeOut": "5000",
        "positionClass": "toast-top-right"
    }
    const __this = this;
    $(__this).html('Đang gửi thông tin...');
    $(__this).prop('disabled', true);
    ten = $('#ten').val();
    dienthoai = $('#dienthoai').val();
    diachi = $('#diachi').val();
    lop = $('#lop').val();
    mon = $('#mon').val();
    hocphimin = $('#hocphimin').val();
    hocphimax = $('#hocphimax').val();
    yeucautrinhdo = $('#yeucautrinhdo').val();
    const _token = $('input[name="_token"]').val();

    if(ten == ''){
        toastr.error('Tên không được để trống.');
        $(__this).html('Gửi yêu cầu');
        $(__this).prop('disabled', false);
        return false;
    }
    if(dienthoai == ''){
        toastr.error('Số điện thoại không được để trống.');
        $(__this).html('Gửi yêu cầu');
        $(__this).prop('disabled', false);
        return false;
    }
    if(diachi == ''){
        toastr.error('Địa chỉ không được để trống.');
        $(__this).html('Gửi yêu cầu');
        $(__this).prop('disabled', false);
        return false;
    }
    
    data_ = {
        'ten': ten,
        'dienthoai': dienthoai,
        'diachi': diachi,
        'lop': lop,
        'mon': mon,
        'hocphimin': hocphimin,
        'hocphimax': hocphimax,
        'yeucautrinhdo': yeucautrinhdo,
        '_token':_token
    }
    var request = $.ajax({
        url: "/timgiasu",
        type: "POST",
        data: data_,
        dataType: "json"
    });
    request.done(function (msg) {
        if(msg.type == 1){
            toastr.success(msg.mess);
            setTimeout(function(){ 
                window.location='/camon';
            }, 2000);
        }else if(msg.type ==2){
            toastr.error(msg.mess);
            $(__this).html('Gửi yêu cầu');
            $(__this).prop('disabled', false);
        }
     });
     request.fail(function (jqXHR, textStatus) {
         alert("Không thể gửi yêu cầu mã lỗi : " + textStatus);
     });
     request.always(function(){
        $(__this).html('Gửi yêu cầu');
    })
})
$(document).on('click', '.labelbangcap', function (e) {
    e.preventDefault();
    
    var id = $(this).attr('data-id');
    var ten = $(this).attr('data-ten');
    var tenbodau = $(this).attr('data-tenbodau');
    var apen = '<span class="bangcapgs" data-id="'+id+'" data-ten="'+ten+'" data-tenbodau="'+tenbodau+'"><span class="icon icon-x xbangcapgs"></span>'+ten+'\
                    <input class="displaynone" type="number" name="bangcap" id="bangcap" value="'+id+'">\
                </span>\
    ';
    $(".area-bangcapchon").append(apen);
    $(this).remove();
    $(".labelbangcap").prop('disabled', true);
})
$(document).on('click', '.xbangcapgs', function(e){
    e.preventDefault();
    var id = $(this).parent('.bangcapgs').attr('data-id');
    var ten = $(this).parent('.bangcapgs').attr('data-ten');
    var tenbodau = $(this).parent('.bangcapgs').attr('data-tenbodau');
    var apen = '<span class="labelbangcap" data-ten="'+ten+'" data-tenbodau="'+tenbodau+'"  data-id="'+id+'">'+ten+'</span>';
    $("#chonbang").append(apen);
    $(this).parent('.bangcapgs').remove();
    $(this).parent('.bangcapgs').children('.displaynone').remove();
    $(".labelbangcap").prop('disabled', false);
})
$(document).on('click','.btn-guithongtin-ctv',function(e){
    e.preventDefault();
    toastr.clear();
    toastr.options = {
        "closeButton": true,
        "timeOut": "5000",
        "positionClass": "toast-top-right"
    }
    const __this = this;
    const _token = $('input[name="_token"]').val();
    $(__this).html('Đang gửi thông tin...');
    $(__this).prop('disabled', true);
    ten = $('#ten').val();
    dienthoai = $('#dienthoai').val();
    email = $('#email').val();
    var formData = new FormData();
    var file_data = $('#anh')[0].files[0];

    
    if(ten == ''){
        toastr.error('Tên không được để trống.');
        $(__this).html('Gửi thông tin');
        $(__this).prop('disabled', false);
        return false;
    }
    if(dienthoai == ''){
        toastr.error('Số điện thoại không được để trống.');
        $(__this).html('Gửi thông tin');
        $(__this).prop('disabled', false);
        return false;
    }
    if(email == '' ){
        toastr.error('Email không được để trống.');
        $(__this).html('Gửi thông tin');
        $(__this).prop('disabled', false);
        return false;
    }
    
    formData.append("anh", file_data);
    formData.append("ten", ten);
    formData.append("dienthoai", dienthoai);
    formData.append("email", email);
    
    data_ = formData;
    var request = $.ajax({
        url: "congtacvien",
        type: "POST",
        data: data_,
        dataType: "json",
        processData: false,
        contentType: false,
        headers: {
            'X-CSRF-TOKEN': $('input[name="_token"]').val()
        }
    });
    request.done(function (msg) {
        if(msg.type == 1){
            toastr.success(msg.mess);
            setTimeout(function(){ 
                window.location='/camon';
                }, 2000);
        }else if(msg.type == 2){
            toastr.error(msg.mess);
            $(__this).prop('disabled', false);
        }else if(msg.type == 3){
            toastr.error(msg.mess);
            $(__this).prop('disabled', false);
        }
    });
    request.fail(function (jqXHR, textStatus) {
        alert("Không thể gửi yêu cầu mã lỗi : " + textStatus);
    });

    request.always(function(){
        $(__this).html('Gửi thông tin');
    })
})