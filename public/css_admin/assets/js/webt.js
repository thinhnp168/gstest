
$(document).on('click', '.btn-duyetgs', function (e) {
    e.preventDefault();
    toastr.clear();
    toastr.options = {
        "closeButton": true,
        "timeOut": "5000",
        "positionClass": "toast-top-right"
    }
    const __this = this;
    $(__this).prop('disabled', true);
    var id = $(this).attr('data-id');
    const __token = $('meta[name="__token"]').attr('content');
    data_ = {
        _token: __token
    }
    var request = $.ajax({
        url: "duyetgiasu/" + id,
        type: "POST",
        data: data_,
        dataType: "json"
    });
    request.done(function (msg) {
        if (msg.type == 1) {
            toastr.success(msg.mess);
            location.reload();
        }
        return false;
    });

    request.fail(function (jqXHR, textStatus) {
        alert("Không thể gửi yêu cầu mã lỗi : " + textStatus);
    });
})
$(document).on('click', '.label-mon', function (e) {
    e.preventDefault();
    var id = $(this).attr('data-id');
    var ten = $(this).attr('data-ten');
    var tenbodau = $(this).attr('data-tenbodau');
    var apen = '<span data-id="' + id + '" data-ten="' + ten + '" data-tenbodau="' + tenbodau + '" for="' + tenbodau + '" class="label-monchon"><span class="icon icon-x xlabel-monchon"></span>' + ten + '\
                    <input class="displaynone" type="checkbox" name="idmon[]" id="'+ tenbodau + '" value="' + id + '" checked>\
                </span>';
    $('.area-monchon').append(apen);
    $(this).remove();
    if ($('#chonmon').find('span').length != 0) {
        $('#chonmon').addClass('show');
        $('#chonmon').parent('.tdropdown').addClass('show');
        return false;
    }
})
$(document).on('click', '.xlabel-monchon', function (e) {
    e.preventDefault();
    var id = $(this).parent('.label-monchon').attr('data-id');
    var ten = $(this).parent('.label-monchon').attr('data-ten');
    var tenbodau = $(this).parent('.label-monchon').attr('data-tenbodau');
    var apen = '<span data-id="' + id + '" data-ten="' + ten + '" data-tenbodau="' + tenbodau + '" class="label-mon form-control">' + ten + '</span>';
    $('.chonmon').append(apen);
    $(this).parent('.label-monchon').remove();
    $(this).parent('.label-monchon').children('.displaynone').remove();
})
$(document).on('click', '.label-lop', function (e) {
    e.preventDefault();
    var id = $(this).attr('data-id');
    var ten = $(this).attr('data-ten');
    var tenbodau = $(this).attr('data-tenbodau');
    var apen = '<span data-id="' + id + '" data-ten="' + ten + '" data-tenbodau="' + tenbodau + '"  class="label-lopchon"><span class="icon icon-x xlabel-lopchon"></span>' + ten + '\
                    <input class="displaynone" type="checkbox" name="idlop[]" id="'+ tenbodau + '" value="' + id + '" checked>\
                </span>';
    $('.area-lopchon').append(apen);
    $(this).remove();
    if ($('#chonlop').find('span').length != 0) {
        $('#chonlop').addClass('show');
        $('#chonlop').parent('.tdropdown').addClass('show');
        return false;
    }
})

$(document).on('click', '.xlabel-lopchon', function (e) {
    e.preventDefault();
    var id = $(this).parent('.label-lopchon').attr('data-id');
    var ten = $(this).parent('.label-lopchon').attr('data-ten');
    var tenbodau = $(this).parent('.label-lopchon').attr('data-tenbodau');
    var apen = '<span data-id="' + id + '" data-ten="' + ten + '" data-tenbodau="' + tenbodau + '" class="label-lop form-control">' + ten + '</span>';
    $('.chonlop').append(apen);
    $(this).parent('.label-lopchon').remove();
    $(this).parent('.label-lopchon').children('.displaynone').remove();
})

$(document).on('click', '.label-quan', function (e) {
    e.preventDefault();
    var id = $(this).attr('data-id');
    var ten = $(this).attr('data-ten');
    var tenbodau = $(this).attr('data-tenbodau');
    var tentimkiem = $(this).attr('data-tentimkiem');
    var apen = '<span data-id="' + id + '" data-ten="' + ten + '" data-tentimkiem="' + tentimkiem + '" data-tenbodau="' + tenbodau + '" for="' + tenbodau + '" class="label-quanchon"><span class="icon icon-x xlabel-quanchon"></span>' + ten + '\
                    <input class="displaynone" type="checkbox" name="idquan[]" id="'+ tenbodau + '" value="' + id + '" checked>\
                </span>';
    $('.area-quanchon').append(apen);
    $(this).remove();
    if ($('#chonquan').find('span').length != 0) {
        $('#chonquan').addClass('show');
        $('#chonquan').parent('.tdropdown').addClass('show');
        return false;
    }
})
$(document).on('click', '.xlabel-quanchon', function (e) {
    e.preventDefault();
    var id = $(this).parent('.label-quanchon').attr('data-id');
    var ten = $(this).parent('.label-quanchon').attr('data-ten');
    var tenbodau = $(this).parent('.label-quanchon').attr('data-tenbodau');
    var tentimkiem = $(this).parent('.label-quanchon').attr('data-tentimkiem');
    var apen = '<span data-id="' + id + '" data-ten="' + ten + '" data-tentimkiem="' + tentimkiem + '" data-tenbodau="' + tenbodau + '" class="label-quan form-control">' + ten + '<p class="displaynone">' + tentimkiem + '</p></span>';
    $('.chonquan').append(apen);
    $(this).parent('.label-quanchon').remove();
    $(this).parent('.label-quanchon').children('.displaynone').remove();
})

function xoa_dau(str) {
    str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
    str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
    str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
    str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
    str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
    str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
    str = str.replace(/đ/g, "d");
    str = str.replace(/À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ/g, "A");
    str = str.replace(/È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ/g, "E");
    str = str.replace(/Ì|Í|Ị|Ỉ|Ĩ/g, "I");
    str = str.replace(/Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ/g, "O");
    str = str.replace(/Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ/g, "U");
    str = str.replace(/Ỳ|Ý|Ỵ|Ỷ|Ỹ/g, "Y");
    str = str.replace(/Đ/g, "D");
    return str;
}
function timquan1() {
    textvalue = $('#timquan').val();
    textvalue = xoa_dau(textvalue).toLowerCase();
    if (textvalue != '') {
        $('.label-quan').hide();
        $('.displaynone:contains("' + textvalue + '")').parents('.label-quan').show();
    } else {
        $('.label-quan').show();
    }
}

function redireactToUrl(url, params = {}) {
    params = $.param(params)
    location.href = `${url}?${params}`
}

function parseParamsFromUrlToObject(url = '') {
    let search;
    if (url == '') {
        search = location.search.substring(1);
    } else {
        search = new URL(url).search.substring(1);
    }
    result = decodeURI(search).replace(/"/g, '\\"').replace(/&/g, '","').replace(/=/g, '":"')
    try {
        return JSON.parse(`{"${result}"}`);
    } catch (e) { }
}

function search(_this) {
    let value = $(_this).val();
    let key = $(_this).attr('name');
    let paramSearch = parseParamsFromUrlToObject();
    
    if(paramSearch === undefined){
        paramSearch = {};
    }
    
    if (value == 0) {
        try {
            delete paramSearch[key];
        } catch (e) { }
    } else {
        paramSearch[key] = value;
    }
    redireactToUrl('', paramSearch);
}
$(document).on('click', '.label-bangcap', function (e) {
    e.preventDefault();
    var id = $(this).attr('data-id');
    var ten = $(this).attr('data-ten');
    var apen = '<span data-id="'+id+'" data-ten="'+ten+'"  class="label-bangcapchon">\
                    <span class="icon icon-x xlabel-bangcapchon"></span>'+ten+'\
                    <input class="displaynone" type="number" name="bangcap" id="bangcap" value="'+id+'">\
                </span>';
    $('.area-bangcapchon').append(apen);
    $(this).remove();
    $(".label-bangcap").prop('disabled', true);
})
$(document).on('click', '.xlabel-bangcapchon', function (e) {
    e.preventDefault();
    var id = $(this).parent('.label-bangcapchon').attr('data-id');
    var ten = $(this).parent('.label-bangcapchon').attr('data-ten');
    var apen = '<span data-id="' + id + '" data-ten="' + ten + '"  class="label-bangcap form-control">' + ten + '</span>';
    $('.chonbangcap').append(apen);
    $(this).parent('.label-bangcapchon').remove();
    $(this).parent('.label-bangcapchon').children('.displaynone').remove();
    $(".label-bangcap").prop('disabled', false);
})
// $(document).on('click', '.taomagioithieu', function (e) {
//     e.preventDefault();
//     var mgt = Math.floor(100000 + Math.random() * 900000);
//     $('#magioithieu').val(mgt);
// })
// $(document).on('click','.guimail',function(e){
//     e.preventDefault();
//     const __this = this;
//     $(__this).html('Đang gửi mail...');
//     $(__this).prop('disabled', true);
//     var id = $(this).data('id');
    
//     toastr.clear();
//     toastr.options = {
//         "closeButton": true,
//         "timeOut": "5000",
//         "positionClass": "toast-top-right"
//     }
//     var id = $(this).attr('data-id');
//     const __token = $('meta[name="__token"]').attr('content');
//     data_ = {
//         _token: __token
//     }
//     var request = $.ajax({
//         url: "/admin/guimailcongtacvien/" + id,
//         type: "POST",
//         data: data_,
//         dataType: "json"
//     });
//     request.done(function (msg) {
//         if (msg.type == 1) {
//             toastr.success(msg.mess);
//             $(__this).html('Gửi mail');
//             $(__this).prop('disabled', false);
//         }
//         return false;
//     });

//     request.fail(function (jqXHR, textStatus) {
//         alert("Không thể gửi yêu cầu mã lỗi : " + textStatus);
//     });
// })
$(document).on('change','.checklentrangchu',function(e){
    e.preventDefault();
    toastr.clear();
    toastr.options = {
        "closeButton": true,
        "timeOut": "5000",
        "positionClass": "toast-top-right"
    }
    const __this = this;
    $(__this).prop('disabled', true);
    var id = $(this).attr('data-id');
    const __token = $('meta[name="__token"]').attr('content');
    data_ = {
        _token: __token
    }
    var request = $.ajax({
        url: "lentrangchu/" + id,
        type: "POST",
        data: data_,
        dataType: "json"
    });
    request.done(function (msg) {
        if (msg.type == 1) {
            toastr.success(msg.mess);
            $(__this).prop('disabled', false);
        }
        return false;
    });

    request.fail(function (jqXHR, textStatus) {
        alert("Không thể gửi yêu cầu mã lỗi : " + textStatus);
    });
})
$(document).on('change','.chuyentrangthaigiasu',function(e){
    e.preventDefault();
    toastr.clear();
    toastr.options = {
        "closeButton": true,
        "timeOut": "5000",
        "positionClass": "toast-top-right"
    }
    const __this = this;
    $(__this).prop('disabled', true);
    var id = $(this).attr('data-id');
    const __token = $('meta[name="__token"]').attr('content');
    data_ = {
        _token: __token
    }
    var request = $.ajax({
        url: "chuyentrangthaigiasu/" + id,
        type: "POST",
        data: data_,
        dataType: "json"
    });
    request.done(function (msg) {
        if (msg.type == 1) {
            toastr.success(msg.mess);
            $(__this).prop('disabled', false);
            return false;
        }else{
            toastr.success(msg.mess);
            $(__this).prop('disabled', false);
            return false;
        }
        
    });

    request.fail(function (jqXHR, textStatus) {
        alert("Không thể gửi yêu cầu mã lỗi : " + textStatus);
    });
})
$(document).on('change','.trangthaictv',function(e){
    e.preventDefault();
    toastr.clear();
    toastr.options = {
        "closeButton": true,
        "timeOut": "5000",
        "positionClass": "toast-top-right"
    }
    const __this = this;
    $(__this).prop('disabled', true);
    var id = $(this).attr('data-id');
    const __token = $('meta[name="__token"]').attr('content');
    data_ = {
        _token: __token
    }
    var request = $.ajax({
        url: "duyetcongtacvien/" + id,
        type: "POST",
        data: data_,
        dataType: "json"
    });
    request.done(function (msg) {
        if (msg.type == 1) {
            toastr.success(msg.mess);
            $(__this).prop('disabled', false);
        }
        return false;
    });

    request.fail(function (jqXHR, textStatus) {
        alert("Không thể gửi yêu cầu mã lỗi : " + textStatus);
    });
})