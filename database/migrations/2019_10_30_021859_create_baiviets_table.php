<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBaivietsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('baiviets', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ten');
            $table->integer('idnguoiviet');
            $table->integer('xapxep');
            $table->string('link');
            $table->string('anh');
            $table->longText('mota');
            $table->string('title');
            $table->string('description');
            $table->string('keywords');
            $table->longText('noidung');
            $table->tinyInteger('trangthai');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('baiviets');
    }
}
