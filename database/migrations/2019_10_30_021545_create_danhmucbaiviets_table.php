<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDanhmucbaivietsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('danhmucbaiviets', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ten');
            $table->string('link');
            $table->string('anh');
            $table->tinyInteger('trangthai');
            $table->tinyInteger('loai');
            $table->string('title');
            $table->longText('mota');
            $table->string('description');
            $table->string('keywords');
            $table->Integer('danhmucme');
            $table->tinyInteger('xapxep');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('danhmucbaiviets');
    }
}
