<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHocsinhsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hocsinhs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ten');
            $table->string('dienthoai');
            $table->string('diachi');
            $table->string('lop');
            $table->string('mon');
            $table->longText('ghichu');
            $table->Integer('hocphimin');
            $table->Integer('hocphimax');
            $table->Integer('hocphi');
            $table->string('yeucautrinhdo');
            $table->tinyInteger('trangthai');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hocsinhs');
    }
}
