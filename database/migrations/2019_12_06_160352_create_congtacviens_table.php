<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCongtacviensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('congtacviens', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ten');
            $table->string('dienthoai');
            $table->string('email');
            $table->string('magioithieu');
            $table->string('anh');
            $table->tinyInteger('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('congtacviens');
    }
}
