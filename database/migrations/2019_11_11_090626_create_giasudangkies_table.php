<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGiasudangkiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('giasudangkies', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ten');
            $table->string('dienthoai');
            $table->string('email');
            $table->string('magioithieu');
            $table->string('bangcap');
            $table->string('congtac');
            $table->string('anh');
            $table->longText('thongtin');
            $table->Integer('thulao');
            $table->tinyInteger('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('giasudangkies');
    }
}
