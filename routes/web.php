<?php
date_default_timezone_set("Asia/Ho_Chi_Minh");
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'HomeController@Index')->name('home');
Route::post('/', 'HomeController@addHocsinh')->name('addHocsinh');
Route::get('/camon', 'HomeController@camon')->name('camon');
Route::get('/gioithieu', 'HomeController@gioithieu')->name('gioithieu');
Route::get('/timgiasu', 'HomeController@timgiasu')->name('timgiasu');
Route::post('/timgiasu', 'HomeController@addHocsinh')->name('posttimgiasu');

Route::get('/dangkygiasu', 'GiasuController@showaddGiasudk')->name('showadd-Giasudk');
Route::post('/dangkygiasu', 'GiasuController@addGiasudk')->name('add-Giasudk');
Route::get('/danhsachgiasu', 'GiasuController@frlistGiasudk')->name('frlist-Giasudk');

Route::get('/tintuc', 'BaivietController@tintuc')->name('tintuc');
Route::get('/danhmuctin/{slug}dst{id}.html', 'BaivietController@tintucdanhsach')->name('tintucdanhsach');
Route::get('/chitiet/{slug}ctt{id}.html', 'BaivietController@chitiettintuc')->name('chitiettintuc');

Route::get('/congtacvien','CongtacvienController@showaddCongtacvien')->name('showadd-Congtacvien');
Route::post('/congtacvien','CongtacvienController@addCongtacvien')->name('add-Congtacvien');

Route::get('/admin/login', 'ThanhvienController@showlogin')->name('showloginad');
Route::post('/admin/login', 'ThanhvienController@loginadmin')->name('loginad');
Route::get('/admin/logout', 'ThanhvienController@logoutadmin')->name('logoutad');

Route::group(['namespace' => 'Admin', 'prefix' => 'admin', 'middleware'=>'adminLogin'], function () {  
    Route::get('/', 'MasterController@home')->name('admin-home');
    
    Route::get('/adduser', 'UserController@showaddUser')->name('showadd-User');
    Route::post('/adduser', 'UserController@addUser')->name('add-User');
    Route::get('/listuser', 'UserController@listUser')->name('list-User');
    Route::get('/showeditUser/{id}', 'UserController@showeditUser')->name('showedit-User');
    Route::post('/showeditUser/{id}', 'UserController@editUser')->name('edit-User');
    Route::get('/delUser/{id}', 'UserController@delUser')->name('del-User');

    Route::get('/addcate', 'CategoriController@showaddCate')->name('showadd-Cate');
    Route::post('/addcate', 'CategoriController@addCate')->name('add-Cate');
    Route::get('/listcate', 'CategoriController@listCate')->name('list-Cate');
    Route::get('/editcate/{id}', 'CategoriController@showeditCate')->name('showedit-Cate');
    Route::post('/editcate/{id}', 'CategoriController@editCate')->name('edit-Cate');
    Route::get('/deletecate/{id}', 'CategoriController@deleteCate')->name('delete-Cate');

    Route::get('/addproduct','ProductController@showaddProduct')->name('showadd-Product');
    Route::post('/addproduct','ProductController@addProduct')->name('add-Product');
    Route::get('/listproduct','ProductController@listProduct')->name('list-Product');
    Route::get('/editproduct/{id}','ProductController@showeditProduct')->name('showedit-Product');
    Route::post('/editproduct/{id}','ProductController@editProduct')->name('edit-Product');
    Route::get('/deleteproduct/{id}','ProductController@delProduct')->name('del-Product');
    
    Route::get('/addmonhoc','MonhocController@showaddMonhoc')->name('showadd-Monhoc');
    Route::post('/addmonhoc','MonhocController@addMonhoc')->name('add-Monhoc');
    Route::get('/listmonhoc','MonhocController@listMonhoc')->name('list-Monhoc');
    Route::get('/editmonhoc/{id}','MonhocController@showeditMonhoc')->name('showedit-Monhoc');
    Route::post('/editmonhoc/{id}','MonhocController@editMonhoc')->name('edit-Monhoc');
    Route::get('/deletemonhoc/{id}','MonhocController@delMonhoc')->name('del-Monhoc');

    Route::get('/addlop','LopController@showaddLop')->name('showadd-Lop');
    Route::post('/addlop','LopController@addLop')->name('add-Lop');
    Route::get('/listlop','LopController@listLop')->name('list-Lop');
    Route::get('/editlop/{id}','LopController@showeditLop')->name('showedit-Lop');
    Route::post('/editlop/{id}','LopController@editLop')->name('edit-Lop');
    Route::get('/deletelop/{id}','LopController@delLop')->name('del-Lop');
    
    Route::get('/addquanhuyen','QuanhuyenController@showaddQuanhuyen')->name('showadd-Quanhuyen');
    Route::post('/addquanhuyen','QuanhuyenController@addQuanhuyen')->name('add-Quanhuyen');
    Route::get('/listquanhuyen','QuanhuyenController@listQuanhuyen')->name('list-Quanhuyen');
    Route::get('/editquanhuyen/{id}','QuanhuyenController@showeditQuanhuyen')->name('showedit-Quanhuyen');
    Route::post('/editquanhuyen/{id}','QuanhuyenController@editQuanhuyen')->name('edit-Quanhuyen');
    Route::get('/deletequanhuyen/{id}','QuanhuyenController@delQuanhuyen')->name('del-Quanhuyen');

    Route::get('/listhocsinh','AdhocsinhController@listHocsinh')->name('list-Hocsinh');
    Route::get('/edithocsinh/{id}','AdhocsinhController@showeditHocsinh')->name('showedit-Hocsinh');
    Route::post('/edithocsinh/{id}','AdhocsinhController@editHocsinh')->name('edit-Hocsinh');
    Route::get('/deletehocsinh/{id}','AdhocsinhController@delHocsinh')->name('del-Hocsinh');

    Route::get('/listgiasudk','AdgiasuController@listGiasudk')->name('list-Giasudk');
    Route::get('/deletegiasudk/{id}','AdgiasuController@delGiasudk')->name('del-Giasudk');
    Route::post('/duyetgiasu/{id}','AdgiasuController@duyetGiasu')->name('duyet-Giasu');
    Route::get('/listgiasu','AdgiasuController@listGiasu')->name('list-Giasu');
    Route::get('/editgiasu/{id}','AdgiasuController@showeditGiasu')->name('showedit-Giasu');
    Route::post('/editgiasu/{id}','AdgiasuController@editGiasu')->name('edit-Giasu');
    Route::post('/lentrangchu/{id}','AdgiasuController@lentrangchuGiasu')->name('lentrangchu-Giasu');
    Route::post('/chuyentrangthaigiasu/{id}','AdgiasuController@chuyentrangthaiGiasu')->name('chuyentrangthai-Giasu');
    Route::get('/deletegiasu/{id}','AdgiasuController@delGiasu')->name('del-Giasu');


    Route::get('/listcongtacvien','AdcongtacvienController@listCongtacvien')->name('list-Congtacvien');
    Route::get('/editcongtacvien/{id}','AdcongtacvienController@showeditCongtacvien')->name('showedit-Congtacvien');
    Route::post('/editcongtacvien/{id}','AdcongtacvienController@editCongtacvien')->name('edit-Congtacvien');
    // Route::post('/guimailcongtacvien/{id}','AdcongtacvienController@guimailCongtacvien')->name('guimail-Congtacvien');
    Route::post('/duyetcongtacvien/{id}','AdcongtacvienController@duyetCongtacvien')->name('duyet-Congtacvien');

    Route::get('/laravel-filemanager', '\UniSharp\LaravelFilemanager\Controllers\LfmController@show');
    Route::post('/laravel-filemanager/upload', '\UniSharp\LaravelFilemanager\Controllers\UploadController@upload');
});
    