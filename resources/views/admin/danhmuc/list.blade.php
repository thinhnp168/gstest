@extends('admin.master')
@section('tilte_site', 'Danh sách danh mục')
@section('content')
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Danh sách danh mục</h1>
            </div>
        </div>
    </div>
</div>
@if (session('success'))
    <div class="alert alert-success">
        {{ session('success') }}
    </div>
@endif
@if (session('fail'))
    <div class="alert alert-danger">
        {{ session('fail') }}
    </div>
@endif
<div class="container-fluid list-cate">
	<div class="row">
        <div class="card-body">
            <table class="table table-bordered">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Tên danh mục</th>
                        <th scope="col">Link</th>
                        <th scope="col">Ảnh đại diện</th>
                        <th scope="col">Trạng thái</th>
                        <!-- <th scope="col">Title</th> -->
                        <!-- <th scope="col">Mô tả</th> -->
                        <!-- <th scope="col">Description</th> -->
                        <!-- <th scope="col">Keywords</th> -->
                        <th scope="col">Danhmucme</th>
                        <th scope="col">Loại danh mục</th>
                        <th scope="col">Xắp xếp</th>
                        <th scope="col">#</th>
                        <th scope="col">#</th>
                    </tr>
                </thead>
                <tbody> 
                    @foreach($listCate as $u)
                        <tr>
                            <th scope="row">{{$u->id}}</th>
                            <td>{{$u->ten}}</td>
                            <td>{{$u->link}}</td>
                            <td>
                                <img class="avatar" src="{{url('/')}}/images/anhbiadanhmuc/{{$u->anh}}" >
                            </td>
                            @if($u->trangthai==1)
                            <td>Hiện</td>
                            @else
                            <td>Ẩn</td>
                            @endif
                            <!-- <td>{{$u->title}}</td> -->
                            <!-- <td>{{$u->mota}}</td>
                            <td>{!!$u->description!!}</td>
                            <td>{{$u->keywords}}</td> -->
                            <td>{{$u->danhmucme}}</td>
                            @if($u->loai==0)
                            <td>Danh mục bài viết</td>
                            @else
                            <td>Danh mục trang đơn</td>
                            @endif

                            <td>{{$u->xapxep}}</td>
                            <td>
                                <a href="{{route('delete-Cate',$u->id)}}" class="badge badge-danger" onclick="return confirm('Bạn chắc chắn muốn xoá?')">Xoá</a>
                            </td>
                            <td>
                                <a href="{{route('showedit-Cate',$u->id)}}" class="badge badge-primary">Sửa</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            {{$listCate->render()}}
        </div>
    </div>
</div>

@endsection