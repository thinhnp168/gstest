@extends('admin.master')
@section('tilte_site', 'Chỉnh sửa Danh mục')
@section('content')
<script src="/tinymce/tinymce.min.js?v=3"></script>
<script>
    tinymce.init({
        selector: '#mota',
        theme: 'modern',
        mode: "textareas",
        fontsize_formats: '8pt 10pt 12pt 14pt 18pt 24pt 36pt 72pt',
        plugins: [
            'advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker',
            'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
            'save table contextmenu directionality emoticons template paste textcolor responsivefilemanager'
        ],
        toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons qrcode youtube responsivefilemanager fontsizeselect',
        external_filemanager_path: "/filemanager/",
        filemanager_title: "Responsive Filemanager",
        external_plugins: { "filemanager": "/filemanager/plugin.min.js" }
    });
</script>
<div class="breadcrumbs">
    <div class="col-sm-12 col-md-12 col-lg-12  col-xl-6">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Chỉnh sửa Danh mục</h1>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">
<form action="" method="post" enctype="multipart/form-data">
    <input type="hidden" name="_token" value="{{csrf_token()}}" /> 
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
	<div class="row mgt50">
        <div class="col-sm-12 col-md-12 col-lg-12  col-xl-8">
            <div class="row form-group">
                <div class="col-sm-12 col-md-3">
                    <label for="ten" class=" form-control-label">Tên </label>
                </div>
                <div class="col-sm-12 col-md-9">
                    <input type="text" id="ten" name="ten" placeholder="Nhập tên danh mục" class="form-control-success form-control" value="{{$showeditCate->ten}}">
                </div>
            </div>
            <div class="row form-group">
                <div class="col-sm-12 col-md-3">
                    <label for="link" class=" form-control-label">Link</label>
                </div>
                <div class="col-sm-12 col-md-9">
                    <input type="text" id="link" name="link" placeholder="Nhập link" class="form-control-success form-control" value="{{$showeditCate->link}}">
                </div>
            </div>
            <div class="row form-group">
                <div class="col-sm-12 col-md-3">
                    <label for="danhmucme" class=" form-control-label">Danh mục mẹ</label>
                </div>
                <div class="col-sm-12 col-md-9">
                    <select name="danhmucme" id="danhmucme" class="form-control form-control-success">
                        @if($showeditCate->danhmucme != 0)
                            <option value="{{$showeditCate->danhmucme}}">{{$timdanhmucme->ten}}</option>
                            @foreach ($danhmucme as $d)
                                @if($d->id != $showeditCate->danhmucme)
                                    <option value="{{$d->id}}">{{$d->ten}}</option>
                                @endif
                            @endforeach
                        @else
                            <option value="0">Danh mục mẹ</option>
                            @foreach ($danhmucme as $d)
                                @if($d->id != $showeditCate->id)
                                    <option value="{{$d->id}}">{{$d->ten}}</option>
                                @endif
                            @endforeach
                        @endif
                     
                    </select>
                </div>
            </div>
            <div class="row form-group">
                <div class="col-sm-12 col-md-3">
                    <label for="xapxep" class=" form-control-label">Xắp xếp</label>
                </div>
                <div class="col-sm-12 col-md-9">
                    <input type="number" id="xapxep" name="xapxep" class="form-control-success form-control" value="{{$showeditCate->xapxep}}" >
                </div>
            </div>
            <div class="row form-group">
                <div class="col-sm-12 col-md-3">
                    <label class=" form-control-label">Trạng thái</label>
                </div>
                <div class="col-sm-12 col-md-9">
                    <div class="form-check-inline form-check">
                        @if( $showeditCate->trangthai == 1)
                            <label for="trangthai1" class="form-check-label mgr20">
                                <input type="radio" id="trangthai1" name="trangthai" value="1" class="form-check-input" checked>Hiện
                            </label>
                            <label for="trangthai0" class="form-check-label ">
                                <input type="radio" id="trangthai0" name="trangthai" value="0" class="form-check-input">Ẩn
                            </label>
                        @else
                            <label for="trangthai1" class="form-check-label mgr20">
                                <input type="radio" id="trangthai1" name="trangthai" value="1" class="form-check-input" >Hiện
                            </label>
                            <label for="trangthai0" class="form-check-label ">
                                <input type="radio" id="trangthai0" name="trangthai" value="0" class="form-check-input" checked>Ẩn
                            </label>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row form-group">
                <div class="col col-md-3">
                    <label for="loai" class=" form-control-label">Loại danh mục</label>
                </div>
                <div class="col-12 col-md-9">
                    <select name="loai" id="loai" class="form-control">
                        @if($showeditCate->loai == 0)
                            <option value="0">Danh mục bài viết</option>
                            <option value="1">Danh mục trang đơn</option>
                        @else
                            <option value="1">Danh mục bài viết</option>
                            <option value="0">Danh mục trang đơn</option>
                        @endif
                    </select>
                </div>
            </div>
            <div class="row form-group">
                <div class="col-sm-12 col-md-3">
                    <label class=" form-control-label">Ảnh</label>
                </div>
                <div class="col-sm-12 col-md-9">
                    <label for="anh" class=" form-control-label label-input-file">Chọn lại ảnh</label>
                    <input type="file" name="anh" id="anh" class="displaynone" onchange="preview_image(event)">
                </div>
            </div>
            <div class="row form-group">
                <div class="col-sm-12 col-md-3">

                </div>
                <div class="col-sm-12 col-md-9">
                    <img  id="blah" src="{{url('/')}}/images/anhbiadanhmuc/{{$showeditCate->anh}}" width='50%'/>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-4">
            <div class="row form-group">
                <div class="col-sm-12 col-md-3">
                    <label for="title" class=" form-control-label">Title</label>
                </div>
                <div class="col-sm-12 col-md-9">
                    <input type="text" id="title" name="title" placeholder="Nhập title" class="form-control-success form-control" value="{{$showeditCate->title}}">
                </div>
            </div>
            <div class="row form-group">
                <div class="col-sm-12 col-md-3">
                    <label for="keywords" class=" form-control-label">Keywords</label>
                </div>
                <div class="col-sm-12 col-md-9">
                    <input type="text" id="keywords" name="keywords" placeholder="Nhập keywords" class="form-control-success form-control" value="{{$showeditCate->keywords}}">
                </div>
            </div>
            <div class="row form-group">
                <div class="col-sm-12 col-md-3">
                    <label for="description" class=" form-control-label">Description</label>
                </div>
                <div class="col-sm-12 col-md-9">
                    <textarea class="form-control-success form-control" name="description" id="description">{{$showeditCate->description}}</textarea>
                </div>
            </div>
        </div>
    </div>
    <div class="row form-group">
        <div class="col-12">
            <label for="mota" class=" form-control-label">Mô tả</label>
            <textarea class="form-control-success form-control mota" name="mota" id="mota">{!!$showeditCate->mota!!}</textarea>
        </div>
    </div>
    <div class="row mgb50">
        <div class="col-12 d-flex justify-content-center">
            <button type="submit" class="btn btn-primary btn-sm mgr20">
                <i class="fa fa-dot-circle-o"></i> Lưu lại
            </button>
            <button type="reset" class="btn btn-danger btn-sm">
                <i class="fa fa-refresh"></i> Làm mới
            </button>
        </div>
    </div>
</form>
</div>


@endsection
@section('script')
<script>
    function preview_image(event) 
    {
        var reader = new FileReader();
        reader.onload = function()
        {
            var output = document.getElementById('blah');
            output.src = reader.result;
        }
        reader.readAsDataURL(event.target.files[0]);
    }
</script>
@endsection