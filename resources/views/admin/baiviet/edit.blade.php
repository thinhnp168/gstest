@extends('admin.master')
@section('tilte_site', 'Chỉnh sửa Bài viết')
@section('content')
<script src="/tinymce/tinymce.min.js?v=3"></script>
<script>
    tinymce.init({
        selector: '#noidung',
        theme: 'modern',
        mode: "textareas",
        plugins: [
            'advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker',
            'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
            'save table contextmenu directionality emoticons template paste textcolor responsivefilemanager'
        ],
        toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons qrcode youtube responsivefilemanager ',
        external_filemanager_path: "/filemanager/",
        filemanager_title: "Responsive Filemanager",
        external_plugins: { "filemanager": "/filemanager/plugin.min.js" }
    });
</script>

<form action="" method="post" enctype="multipart/form-data">
    <div class="breadcrumbs">
        <div class="container-fluid">
            <div class="col-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Chỉnh sửa Bài viết</h1>
                    </div>
                </div>
            </div>
            <div class="col-8 ">
                <div class="row d-flex justify-content-end pd10-0">
                    <button type="submit" class="btn btn-primary btn-sm mgr20">
                        <i class="fa fa-dot-circle-o"></i> Lưu lại
                    </button>
                    <button type="reset" class="btn btn-danger btn-sm">
                        <i class="fa fa-refresh"></i> Làm mới
                    </button>
                </div>
            </div>
        </div>
    </div>
        
    <div class="container-fluid mgb50">
            <input type="hidden" name="_token" value="{{csrf_token()}}" /> 
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        <div class="row">
            <div class="col-sm-6 col-md-6 col-lg-9  col-xl-10">
                <div class="row justify-content-start">
                    <div class="col-sm-12 col-md-12 col-lg-8  col-xl-6">
                        <div class="form-group">
                            <label for="ten" class=" form-control-label">Tên bài viết</label>
                            <input type="text" id="ten" name="ten" placeholder="Nhập tên bài viết" class="form-control" value="{{$showeditProduct->ten}}">
                        </div>
                    </div>
                </div>
                <div class="row justify-content-start">
                    <div class="col-sm-12 col-md-12 col-lg-8  col-xl-6">
                        <div class="form-group">
                            <label for="link" class=" form-control-label">Link</label>
                            <input type="text" id="link" name="link" placeholder="Nhập link" class="form-control" value="{{$showeditProduct->link}}">
                        </div>
                    </div>
                </div>
                <div class="row justify-content-start">
                    <div class="col-sm-12 col-md-12 col-lg-8  col-xl-6">
                        <div class="form-group">
                            <label for="title" class=" form-control-label ">Title</label>
                            <input type="text" name="title" id="title" placeholder="Nhập title" class=" form-control" value="{{$showeditProduct->title}}">
                        </div>
                    </div>
                </div>
                <div class="row justify-content-start">
                    <div class="col-sm-12 col-md-12 col-lg-8  col-xl-6">
                        <div class="form-group">
                            <label for="keywords" class=" form-control-label ">Keywords</label>
                            <input type="text" name="keywords" id="keywords" placeholder="Nhập keywords" class=" form-control" value="{{$showeditProduct->keywords}}">
                        </div>
                    </div>
                </div>
                <div class="row justify-content-start">
                    <div class="col-sm-12 col-md-12 col-lg-8  col-xl-6">
                        <div class="form-group">
                            <label for="mota" class=" form-control-label ">Mô tả</label>
                            <textarea name="mota" id="mota" class="form-control" rows="5" >{!!$showeditProduct->mota!!}</textarea>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="noidung" class=" form-control-label">Nội dung bài viết</label>
                    <textarea name="noidung" id="noidung" class="form-control" rows="10" >{!!$showeditProduct->noidung!!}</textarea>
                </div>
            </div>
            <div class="col-sm-6 col-md-6 col-lg-3  col-xl-2">
                <div class="form-group">
                    <label for="trangthai" class=" form-control-label ">Trạng thái</label>
                    <div class="form-check ">
                        @if($showeditProduct->trangthai == 1)
                            <label for="trangthai1" class="form-check-label mgr20">
                                <input type="radio" id="trangthai1" name="trangthai" value="1" class="form-check-input" checked>Hiện
                            </label>
                            <label for="trangthai0" class="form-check-label ">
                                <input type="radio" id="trangthai0" name="trangthai" value="0" class="form-check-input">Ẩn
                            </label>
                        @else
                            <label for="trangthai1" class="form-check-label mgr20">
                                <input type="radio" id="trangthai1" name="trangthai" value="1" class="form-check-input" >Hiện
                            </label>
                            <label for="trangthai0" class="form-check-label ">
                                <input type="radio" id="trangthai0" name="trangthai" value="0" class="form-check-input" checked>Ẩn
                            </label>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label for="xapxep" class=" form-control-label ">Xắp xếp</label>
                    <input type="number" name="xapxep" id="xapxep" class=" form-control" value="{{$showeditProduct->xapxep}}" >
                </div>
                <div class="form-group">
                    <label for="description" class=" form-control-label ">Description</label>
                    <textarea name="description" id="description" class="form-control" rows="3" >{!!$showeditProduct->description!!}</textarea>
                </div>
                <div class="has-success form-group">
                    <label for="anh" class=" form-control-label label-input-file">Ảnh đại diện</label>
                    <input type="file" name="anh" id="anh" class="displaynone" onchange="preview_image(event)">
                </div>
                <div class="has-success form-group">
                    <img  id="blah" src="{{url('/')}}/images/anhbiabaiviet/{{$showeditProduct->anh}}" width='100%'/>
                </div>
                <div class="row justify-content-start">
                    <div class="col-12">
                        <div class="form-group">
                            <input type="text" placeholder="Tìm danh mục" class="form-control" id="timdanhmuc" onchange="timdanhmuc2()" onkeyup="timdanhmuc2()"> 
                        </div>
                    </div>
                </div>
                <div class="has-success form-group">
                    <label for="iddanhmuc" class=" form-control-label ">Chọn danh mục</label>
                    <div class="form-check area-danhmuc">
                        @foreach($danhmuc as $d)
                            @if($d->danhmucme == 0)
                                <div class="checkbox">
                                    <label for="iddanhmuc{{$d->id}}" class="form-check-label ">
                                        <input type="checkbox" id="iddanhmuc{{$d->id}}" name="iddanhmuc[]" value="{{$d->id}}" class="form-check-input">{{$d->ten}}
                                    </label>
                                    <p class="displaynone">{{$d->tentimkiem}}</p>
                                </div>
                                @foreach($danhmuc as $a)
                                    @if($a->danhmucme == $d->id)
                                    <div class="checkbox mgl20">
                                        <label for="iddanhmuc{{$a->id}}" class="form-check-label ">
                                            <input type="checkbox" id="iddanhmuc{{$a->id}}" name="iddanhmuc[]" value="{{$a->id}}" class="form-check-input">{{$a->ten}}
                                        </label>
                                        <p class="displaynone">{{$a->tentimkiem}}</p>
                                    </div>
                                    @endif
                                @endforeach
                            @endif
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>


@endsection
@section('script')
<script>
    $(document).ready(()=>{
        let lienket = {!!$lienket!!};
        console.log(lienket)
        for(let lk of lienket){
            $(`#iddanhmuc${lk.iddanhmuc}`).attr('checked', true);
        }
    })
    function xoa_dau(str) {
            str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
            str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
            str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
            str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
            str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
            str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
            str = str.replace(/đ/g, "d");
            str = str.replace(/À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ/g, "A");
            str = str.replace(/È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ/g, "E");
            str = str.replace(/Ì|Í|Ị|Ỉ|Ĩ/g, "I");
            str = str.replace(/Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ/g, "O");
            str = str.replace(/Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ/g, "U");
            str = str.replace(/Ỳ|Ý|Ỵ|Ỷ|Ỹ/g, "Y");
            str = str.replace(/Đ/g, "D");
            return str;
        }
    function preview_image(event) 
    {
        var reader = new FileReader();
        reader.onload = function()
        {
            var output = document.getElementById('blah');
            output.src = reader.result;
        }
        reader.readAsDataURL(event.target.files[0]);
    }
    function timdanhmuc2() {
            textvalue= $('#timdanhmuc').val();
            textvalue = xoa_dau(textvalue).toLowerCase();
            if(textvalue != ''){
                $('div.checkbox').hide();
                $('.displaynone:contains("'+textvalue+'")').parents('div.checkbox').show();
            }else{
                $('div.checkbox').show();
            } 
        }
</script>
@endsection

