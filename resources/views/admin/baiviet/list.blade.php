@extends('admin.master')
@section('tilte_site', 'Danh sách Bài viết')
@section('content')
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Danh sách Bài viết</h1>
            </div>
        </div>
    </div>
</div>
@if (session('success'))
    <div class="alert alert-success">
        {{ session('success') }}
    </div>
@endif
<div class="container-fliud list-cate">
	<div class="row">
        <div class="card-body">
            <table class="table table-bordered">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Tên bài viết</th>
                        <th scope="col">Link</th>
                        <th scope="col">Ảnh đại diện</th>
                        <th scope="col">Trạng thái</th>
                        <th scope="col">Title</th>
                        <th scope="col">Description</th>
                        <th scope="col">Keywords</th>
                        <th scope="col">Xắp xếp</th>
                        <th scope="col">Người viết</th>
                        <th scope="col">#</th>
                        <th scope="col">#</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($listProduct as $u)
                        <tr>
                            <th scope="row">{{$u->id}}</th>
                            <td>{{$u->ten}}</td>
                            <td>{{$u->link}}</td>
                            <td>
                                <img class="avatar" src="{{url('/')}}/images/anhbiabaiviet/{{$u->anh}}" >
                            </td>
                            @if($u->trangthai==1)
                            <td>Hiện</td>
                            @else
                            <td>Ẩn</td>
                            @endif
                            <td>{{$u->title}}</td>
                            <td>{{$u->description}}</td>
                            <td>{{$u->keywords}}</td>
                            <td>{{$u->xapxep}}</td>
                            <td>{{$u->uservspro->name}}</td>

                            <td>
                                <a href="{{route('del-Product',$u->id)}}" class="badge badge-danger" onclick="return confirm('Bạn chắc chắn muốn xoá?')">Xoá</a>
                            </td>
                            <td>
                                <a href="{{route('showedit-Product',$u->id)}}" class="badge badge-primary">Sửa</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            {{$listProduct->render()}}
        </div>
    </div>
</div>

@endsection