@extends('admin.master')
@section('tilte_site', 'Danh sách gia sư ')
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-3">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>Danh sách gia sư</h1>
                </div>
            </div>
        </div>
        <div class="col-9">
            <div class="row">
                <div class="col-3">
                    <a href="{{route('list-Giasu')}}"  class="form-control" >Xoá bộ lọc</a>
                </div>
                <div class="col-3">
                    <select name="mon" id="" class="form-control locmon" onchange="search(this)">
                        <option value="0">Chọn môn</option>
                        @foreach ($monhoc as $m)
                        <option value="{{$m->id}}">{{$m->ten}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-3">
                    <select name="quan" onchange="search(this)" id="" class="form-control locquan">
                        <option value="0">Chọn quận</option>
                        @foreach ($quanhuyen as $m)
                        <option value="{{$m->id}}">{{$m->ten}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-3">
                    <select name="lop" id="" onchange="search(this)" class="form-control loclop">
                        <option value="0">Chọn lớp</option>
                        @foreach ($lop as $m)
                        <option value="{{$m->id}}">{{$m->ten}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
    </div>
</div>
@if (session('success'))
    <div class="alert alert-success">
        {{ session('success') }}
    </div>
@endif
@if (session('fail'))
    <div class="alert alert-danger">
        {{ session('fail') }}
    </div>
@endif
<div class="container-fluid list-cate">
	<div class="row">
        <div class="col-12 card-body">
            <table class="table table-bordered">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Thông tin chung</th>
                        <th scope="col" class="text-center">Thông tin lựa chọn</th>
                        <th scope="col" class="text-center">Lên trang chủ</th>
                        <th scope="col" class="text-center">Trạng thái</th>
                        <th scope="col">#</th>
                    </tr>
                </thead>
                <tbody id="listgiasu"> 
                    @foreach($listGiasu as $u)
                        <tr class="@php echo ($u->status == 1) ?'':'bgrc' @endphp">
                            <th scope="row">{{$u->id}}</th>
                            <td>
                                <p>{{$u->ten}}</p>
                                <img class="avatar" src="{{url('/')}}/images/anhgiasudangky/{{$u->anh}}" >
                                <p>{{$u->dienthoai}}</p>
                                <p>{{$u->magioithieu}}</p>
                            </td>
                            <td>
                                <p class=""> Môn: 
                                    @foreach($u->metamon as $mm)
                                        <span class="btn btn-outline-success lineh1 mgr5 mgb5">{{$mm->mon->ten }} </span>
                                    @endforeach
                                </p>
                                <p class=""> Quận:
                                    @foreach($u->metaquan as $mq)
                                        <span class="btn btn-outline-success lineh1 mgr5 mgb5">{{$mq->quan->ten }} </span>
                                    @endforeach
                                </p>
                                <p class=""> Lớp:
                                    @foreach($u->metalop as $mp)
                                        <span class="btn btn-outline-success lineh1 mgr5 mgb5">{{$mp->lop->ten }}</span>
                                    @endforeach
                                </p>
                            </td>
                            @if($u->lentrangchu == 0)
                                <td class="text-center">
                                    <input type="checkbox" data-id="{{$u->id}}" class="checklentrangchu" name="" id="" > 
                                </td>
                            @else
                                <td class="text-center">
                                    <input type="checkbox" data-id="{{$u->id}}" class="checklentrangchu" name="" id="" checked> 
                                </td>
                            @endif
                            @if($u->status==0 )
                                <td class="text-center">
                                    <input type="checkbox" name="" id="" class="chuyentrangthaigiasu"  data-id="{{$u->id}}">
                                </td>
                            @else
                                <td class="text-center">
                                    <input type="checkbox" name="" id="" class="chuyentrangthaigiasu" data-id="{{$u->id}}" checked>
                                </td>
                            @endif
                            <td>
                                <a href="{{route('showedit-Giasu',$u->id)}}" class="badge badge-info">Sửa</a>
                                <a href="{{route('del-Giasu',$u->id)}}" class="badge badge-danger" onclick="return confirm('Bạn chắc chắn muốn xoá?')">Xoá</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

@endsection
@section('script')
<script>
   $( document ).ready(function() {
        var url_string = location.href;
        var url = new URL(url_string);
        var mon = url.searchParams.get("mon");
        if(mon != null){
            $('.locmon').val(mon);
        }
        var quan = url.searchParams.get("quan");
        if(quan != null){
            $('.locquan').val(quan);
        }
        var lop = url.searchParams.get("lop");
        if(lop != null){
            $('.loclop').val(lop);
        }
    });
</script>
@endsection