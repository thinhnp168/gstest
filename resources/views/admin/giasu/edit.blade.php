@extends('admin.master')
@section('tilte_site', 'Chỉnh sửa thông tin gia sư')
@section('content')

<script src="/tinymce/tinymce.min.js?v=3"></script>
<script>
    tinymce.init({
        selector: '#thongtin',
        theme: 'modern',
        mode: "textareas",
        plugins: [
            'advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker',
            'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
            'save table contextmenu directionality emoticons template paste textcolor responsivefilemanager'
        ],
        toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons qrcode youtube responsivefilemanager ',
        external_filemanager_path: "/filemanager/",
        filemanager_title: "Responsive Filemanager",
        external_plugins: { "filemanager": "/filemanager/plugin.min.js" }
    });
</script>
<form action="" method="post" enctype="multipart/form-data">
    <div class="breadcrumbs">
        <div class="container-fluid">
            <div class="col-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Chỉnh sửa thông tin gia sư</h1>
                    </div>
                </div>
            </div>
            <div class="col-8 ">
                <div class="row d-flex justify-content-end pd10-0">
                    <button type="submit" class="btn btn-primary btn-sm mgr20">
                        <i class="fa fa-dot-circle-o"></i> Lưu lại
                    </button>
                    <button type="reset" class="btn btn-danger btn-sm">
                        <i class="fa fa-refresh"></i> Đặt lại
                    </button>
                </div>
            </div>
        </div>
    </div>
        
    <div class="container-fluid mgb50">
        <input type="hidden" name="_token" value="{{csrf_token()}}" /> 
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @if (session('fail'))
            <div class="alert alert-danger">
                {{ session('fail') }}
            </div>
        @endif
        <div class="row mgt50">
            <div class="col-sm-12 col-md-12 col-lg-12  col-xl-6">
                <div class="row form-group">
                    <div class="col-sm-12 col-md-3">
                        <label for="ten" class=" form-control-label">Tên </label>
                    </div>
                    <div class="col-sm-12 col-md-9">
                        <input type="text" id="ten" name="ten" placeholder="Nhập tên gia sư" class="form-control-success form-control" value="{{$showeditGiasu->ten}}">
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-sm-12 col-md-3">
                        <label for="dienthoai" class=" form-control-label">Điện thoại</label>
                    </div>
                    <div class="col-sm-12 col-md-9">
                        <input type="text" id="dienthoai" name="dienthoai" placeholder="Nhập số điện thoại" class="form-control-success form-control" value="{{$showeditGiasu->dienthoai}}">
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-sm-12 col-md-3">
                        <label for="email" class=" form-control-label">Email</label>
                    </div>
                    <div class="col-sm-12 col-md-9">
                        <input type="text" id="email" name="email" placeholder="Nhập email" class="form-control-success form-control" value="{{$showeditGiasu->email}}">
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-sm-12 col-md-3">
                        <label for="magioithieu" class=" form-control-label">Mã giới thiệu</label>
                    </div>
                    <div class="col-sm-12 col-md-9">
                        <input type="text" id="magioithieu" name="magioithieu" placeholder="Nhập mã giới thiệu" class="form-control-success form-control" value="{{$showeditGiasu->magioithieu}}" >
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-sm-12 col-md-3">
                        <label for="" class=" form-control-label">Trạng thái</label>
                    </div>
                    <div class="col-sm-12 col-md-9">
                        <select name="status" id="status" class="form-control-success form-control">
                            @if($showeditGiasu->status == 0)
                                <option value="0">Khoá</option>
                                <option value="1">Mở</option>
                            @else
                                <option value="1">Mở</option>
                                <option value="0">Khoá</option>
                            @endif
                        </select>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-sm-12 col-md-3">
                        <label for="" class=" form-control-label">Lên trang chủ</label>
                    </div>
                    <div class="col-sm-12 col-md-9">
                        <select name="lentrangchu" id="lentrangchu" class="form-control-success form-control">
                            @if($showeditGiasu->lentrangchu == 0)
                                <option value="0">Không</option>
                                <option value="1">Có</option>
                            @else
                                <option value="1">Có</option>
                                <option value="0">Không</option>
                            @endif
                        </select>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-sm-12 col-md-3">
                        <label for="" class=" form-control-label">Học vị, bằng cấp</label>
                    </div>
                    <div class="col-sm-12 col-md-9 tdropdown">
                        <button type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle form-control text-right"></button>
                        <div  aria-hidden="true" role="menu" class="dropdown-menu form-control chonbangcap" id="chonbangcap">
                            @if($showeditGiasu->bangcap == 1)
                            <span class="label-bangcap form-control" data-ten="Cao đẳng" data-id="2">Cao đẳng</span>
                            <span class="label-bangcap form-control" data-ten="Đại học" data-id="3">Đại học</span>
                            <span class="label-bangcap form-control" data-ten="Thạc sỹ" data-id="4">Thạc sỹ</span>
                            @elseif($showeditGiasu->bangcap == 2)
                            <span class="label-bangcap form-control" data-ten="Sinh viên" data-id="1">Sinh viên</span>
                            <span class="label-bangcap form-control" data-ten="Đại học" data-id="3">Đại học</span>
                            <span class="label-bangcap form-control" data-ten="Thạc sỹ" data-id="4">Thạc sỹ</span> 
                            @elseif($showeditGiasu->bangcap == 3)
                            <span class="label-bangcap form-control" data-ten="Sinh viên" data-id="1">Sinh viên</span>
                            <span class="label-bangcap form-control" data-ten="Cao đẳng" data-id="2">Cao đẳng</span>
                            <span class="label-bangcap form-control" data-ten="Thạc sỹ" data-id="4">Thạc sỹ</span> 
                            @elseif($showeditGiasu->bangcap == 4)
                            <span class="label-bangcap form-control" data-ten="Sinh viên" data-id="1">Sinh viên</span>
                            <span class="label-bangcap form-control" data-ten="Cao đẳng" data-id="2">Cao đẳng</span>
                            <span class="label-bangcap form-control" data-ten="Đại học" data-id="3">Đại học</span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-sm-12 col-md-3">
                       
                    </div>
                    <div class="col-sm-12 col-md-9">
                        <div class="row mg0 area-bangcapchon">
                            @if($showeditGiasu->bangcap == 1)
                            <span data-id="1" data-ten="Sinh viên"  class="label-bangcapchon">
                                <span class="icon icon-x xlabel-bangcapchon"></span>Sinh viên
                                <input class="displaynone" type="number" name="bangcap" id="bangcap" value="1">
                            </span>
                            @elseif($showeditGiasu->bangcap == 2)
                            <span data-id="2" data-ten="Cao đẳng"  class="label-bangcapchon">
                                <span class="icon icon-x xlabel-bangcapchon"></span>Cao đẳng
                                <input class="displaynone" type="number" name="bangcap" id="bangcap" value="2">
                            </span>
                            @elseif($showeditGiasu->bangcap == 3)
                            <span data-id="3" data-ten="Đại học"  class="label-bangcapchon">
                                <span class="icon icon-x xlabel-bangcapchon"></span>Đại học
                                <input class="displaynone" type="number" name="bangcap" id="bangcap" value="3">
                            </span>
                            @elseif($showeditGiasu->bangcap == 4)
                            <span data-id="4" data-ten="Thạc sỹ"  class="label-bangcapchon">
                                <span class="icon icon-x xlabel-bangcapchon"></span>Thạc sỹ
                                <input class="displaynone" type="number" name="bangcap" id="bangcap" value="4">
                            </span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-sm-12 col-md-3">
                        <label for="bangcap" class=" form-control-label">Học vị, bằng cấp</label>
                    </div>
                    <div class="col-sm-12 col-md-9">
                        <input type="text" id="bangcap" name="bangcap" placeholder="Nhập số học vị, bằng cấp" class="form-control-success form-control" value="{{$showeditGiasu->bangcap}}">
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-sm-12 col-md-3">
                        <label for="congtac" class=" form-control-label">Đơn vị công tác</label>
                    </div>
                    <div class="col-sm-12 col-md-9">
                        <input type="text" id="congtac" name="congtac" placeholder="Nhập số đơn vị công tác" class="form-control-success form-control" value="{{$showeditGiasu->congtac}}">
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-sm-12 col-md-3">
                        <label for="thulao" class=" form-control-label">Thù lao</label>
                    </div>
                    <div class="col-sm-12 col-md-9">
                        <input type="number" id="thulao" name="thulao" min="0" placeholder="100000" class="form-control-success form-control w50 inlib" value="{{$showeditGiasu->thulao}}"><span class="fwbold">   VND/giờ</span>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-sm-12 col-md-3">
                        <label for="thongtin" class=" form-control-label">Thông tin</label>
                    </div>
                    <div class="col-sm-12 col-md-9">
                        <textarea name="thongtin" id="thongtin" class="form-control-success form-control" rows="10">{!!$showeditGiasu->thongtin!!}</textarea>
                    </div>
                </div>
                
            </div>
            <div class="col-sm-12 col-md-12 col-lg-12  col-xl-6">
                <div class="row form-group">
                    <div class="col-sm-12 col-md-3">
                        <label for="" class=" form-control-label">Môn học</label>
                    </div>
                    <div class="col-sm-12 col-md-9 tdropdown">
                        <button type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle form-control text-right"></button>
                        <div  aria-hidden="true" role="menu" class="dropdown-menu form-control chonmon" id="chonmon">
                            @foreach($monhoc as $m)
                                @php
                                    $i= 0;
                                @endphp
                                @foreach($metamon as $mt)
                                    @if($m->id == $mt->mon->id)
                                        @php
                                            $i++;
                                        @endphp
                                    @endif
                                @endforeach
                                @if($i == 0)
                                    <span data-id="{{$m->id}}" data-ten="{{$m->ten}}" data-tenbodau="{{$m->tenbodau}}" class="label-mon form-control">{{$m->ten}}</span>
                                @endif
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-sm-12 col-md-3">
                       
                    </div>
                    <div class="col-sm-12 col-md-9">
                        <div class="row mg0 area-monchon">
                            @foreach($metamon as $mt)
                            <span data-id="{{$mt->mon->id}}" data-ten="{{$mt->mon->ten}}" data-tenbodau="{{$mt->mon->tenbodau}}" for="{{$mt->mon->tenbodau}}" class="label-monchon">
                                <span class="icon icon-x xlabel-monchon"></span>{{$mt->mon->ten}}
                                <input class="displaynone" type="checkbox" name="idmon[]" id="{{$mt->mon->tenbodau}}" value="{{$mt->mon->id}}" checked>
                            </span>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-sm-12 col-md-3">
                        <label for="" class=" form-control-label">Quận, huyện</label>
                    </div>
                    <div class="col-sm-12 col-md-9 tdropdown">
                        <button type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle form-control text-right"></button>
                        <div  aria-hidden="true" role="menu" class="dropdown-menu form-control chonquan" id="chonquan">
                            <input type="text" name="" id="timquan" placeholder="Tìm quận" class="mgb5 form-control" onchange="timquan1()" onkeyup="timquan1()">
                            @foreach($quanhuyen as $q)
                                @php
                                    $i= 0;
                                @endphp
                                @foreach($metaquan as $mt)
                                    @if($q->id == $mt->quan->id)
                                        @php
                                            $i++;
                                        @endphp
                                    @endif
                                @endforeach    
                                @if($i==0)
                                    <span data-id="{{$q->id}}" data-ten="{{$q->ten}}" data-tenbodau="{{$q->tenbodau}}" data-tentimkiem="{{$q->tentimkiem}}" class="label-quan form-control">{{$q->ten}} <p class="displaynone">{{$q->tentimkiem}}</p></span>
                                @endif
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-sm-12 col-md-3">
                       
                    </div>
                    <div class="col-sm-12 col-md-9">
                        <div class="row mg0 area-quanchon">
                            @foreach($metaquan as $mt)
                            <span data-id="{{$mt->quan->id}}" data-ten="{{$mt->quan->ten}}" data-tentimkiem="{{$mt->quan->tentimkiem}}" data-tenbodau="{{$mt->quan->tenbodau}}" for="{{$mt->quan->tenbodau}}" class="label-quanchon">
                                <span class="icon icon-x xlabel-quanchon"></span>{{$mt->quan->ten}}
                                <input class="displaynone" type="checkbox" name="idquan[]" id="{{$mt->quan->tenbodau}}" value="{{$mt->quan->id}}" checked>
                            </span>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-sm-12 col-md-3">
                        <label for="" class=" form-control-label">Lớp</label>
                    </div>
                    <div class="col-sm-12 col-md-9 tdropdown">
                        <button type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle form-control text-right"></button>
                        <div  aria-hidden="true" role="menu" class="dropdown-menu form-control chonlop" id="chonlop">
                            @foreach($lop as $m)
                                @php
                                    $i= 0;
                                @endphp
                                @foreach($metalop as $mt)
                                    @if($m->id == $mt->lop->id)
                                        @php
                                            $i++;
                                        @endphp
                                    @endif
                                @endforeach
                                @if($i == 0)
                                    <span data-id="{{$m->id}}" data-ten="{{$m->ten}}" data-tenbodau="{{$m->tenbodau}}" class="label-lop form-control">{{$m->ten}}</span>
                                @endif
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-sm-12 col-md-3">
                       
                    </div>
                    <div class="col-sm-12 col-md-9">
                        <div class="row mg0 area-lopchon">
                            @foreach($metalop as $mt)
                            <span data-id="{{$mt->lop->id}}" data-ten="{{$mt->lop->ten}}" data-tenbodau="{{$mt->lop->tenbodau}}" for="{{$mt->lop->tenbodau}}" class="label-lopchon">
                                <span class="icon icon-x xlabel-lopchon"></span>{{$mt->lop->ten}}
                                <input class="displaynone" type="checkbox" name="idlop[]" id="{{$mt->lop->tenbodau}}" value="{{$mt->lop->id}}" checked>
                            </span>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-sm-12 col-md-3">
                        <label class=" form-control-label">Ảnh đại diện</label>
                    </div>
                    <div class="col-sm-12 col-md-9">
                        <label for="anh" class="orm-control-label label-input-file" >Chọn ảnh khác</label>
                        <input type="file" name="anh" id="anh" class="displaynone" onchange="preview_image(event)">
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-sm-12 col-md-3">
                        
                    </div>
                    <div class="col-sm-12 col-md-9">
                        <img id='showavatar' class='w-50' src="{{url('/')}}/images/anhgiasudangky/{{$showeditGiasu->anh}}">
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
<script>
    function preview_image(event){
        var reader = new FileReader();
        reader.onload = function()
        {
            var output = document.getElementById('showavatar');
            output.src = reader.result;
        }
        reader.readAsDataURL(event.target.files[0]);
    }
</script>

@endsection
@section('script')
<script>
    $( document ).ready(function() {
        $(".label-bangcap").prop('disabled', true);
    });
</script>
@endsection
