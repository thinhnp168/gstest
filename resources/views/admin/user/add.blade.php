@extends('admin.master')
@section('tilte_site', 'Thêm mới Người dùng')
@section('content')
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Thêm mới Người dùng</h1>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">
    <form action="" method="post" enctype="multipart/form-data">
        <input type="hidden" name="_token" value="{{csrf_token()}}" /> 
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="row mgt50">
            <div class="col-sm-12 col-md-12 col-lg-12  col-xl-6">
                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="name" class=" form-control-label">Họ và tên</label>
                    </div>
                    <div class="col-12 col-md-9">
                        <input type="text" id="name" name="name" placeholder="Nhập họ và tên" class="form-control-success form-control">
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="email" class=" form-control-label">Email</label>
                    </div>
                    <div class="col-12 col-md-9">
                        <input type="email" id="email" name="email" placeholder="Nhập email" class="form-control-success form-control">
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="phone" class=" form-control-label">Số điện thoại</label>
                    </div>
                    <div class="col-12 col-md-9">
                        <input type="text" id="phone" name="phone" placeholder="Nhập số điện thoại" class="form-control-success form-control">
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="address" class=" form-control-label">Địa chỉ</label>
                    </div>
                    <div class="col-12 col-md-9">
                        <input type="text" id="address" name="address" placeholder="Nhập dịa chỉ" class="form-control-success form-control">
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="password" class=" form-control-label ">Mật khẩu</label>
                    </div>
                    <div class="col-12 col-md-9 relative">
                        <input type="password" name="password" id="password" placeholder="Nhập mật khẩu" class="form-control-success form-control" >
                        <span class="fa fa-eye-slash" id="showpass" title="Hiện mật khẩu"></span>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="status" class=" form-control-label ">Trạng thái</label>
                    </div>
                    <div class="col-12 col-md-9">
                        <div class="form-check-inline form-check">
                            <label for="status1" class="form-check-label mgr20">
                                <input type="radio" id="status1" name="status" value="1" class="form-check-input" checked>Kích hoạt
                            </label>
                            <label for="status0" class="form-check-label ">
                                <input type="radio" id="status0" name="status" value="0" class="form-check-input">Khoá
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mgt50">
            <div class="col-6">
                <div class="row form-group">
                    <div class="col col-md-3">
                            
                    </div>
                    <div class="col-12 col-md-9 relative">
                        <button type="submit" class="btn btn-primary btn-sm mgr20">
                            <i class="fa fa-dot-circle-o"></i> Thêm mới
                        </button>
                        <button type="reset" class="btn btn-danger btn-sm">
                            <i class="fa fa-refresh"></i> Làm mới
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection
@section('script')
    <script>
        $(document).on('click', '#showpass', function (e) {
            if ($(this).hasClass('fa-eye')) {
                $(this).removeClass('fa-eye');
                $(this).addClass('fa-eye-slash');
                $('#password').attr('type','password');
            }else{
                $(this).addClass('fa-eye');
                $(this).removeClass('fa-eye-slash');
                $('#password').attr('type','text');
            }
        })
    </script>
@endsection