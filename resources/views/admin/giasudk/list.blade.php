@extends('admin.master')
@section('tilte_site', 'Danh sách đăng ký gia sư')
@section('content')
<div class="breadcrumbs">
    <div class="col-sm-12">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Danh sách đăng ký gia sư</h1>
            </div>
        </div>
    </div>
</div>
@if (session('success'))
    <div class="alert alert-success">
        {{ session('success') }}
    </div>
@endif
@if (session('fail'))
    <div class="alert alert-danger">
        {{ session('fail') }}
    </div>
@endif
<div class="container-fluid list-cate">
	<div class="row">
        <div class="col-12">
            <table class="table table-bordered">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Thông tin chung</th>
                        <th scope="col">Thông tin khác</th>
                        <th scope="col">Thông tin lựa chọn</th>
                        <th scope="col">#</th>
                        <th scope="col">#</th>
                    </tr>
                </thead>
                <tbody> 
                    @foreach($listGiasudk as $u)
                        <tr>
                            <th scope="row">{{$u->id}}</th>
                            <td>
                                <p>{{$u->ten}}</p>
                                <p><img class="avatar" src="{{url('/')}}/images/anhgiasudangky/{{$u->anh}}" ></p>
                                <p>{{$u->dienthoai}}</p>
                                <p>{{$u->email}}</p>
                                <p>{{$u->magioithieu}}</p>
                            </td>
                            <td> <p>Bằng cấp: 
                                    @if($u->bangcap == 1)
                                    Sinh viên
                                    @elseif($u->bangcap == 2)
                                    Cao đẳng
                                    @elseif($u->bangcap ==3)
                                    Đại học
                                    @elseif($u->bangcap == 4)
                                    Thạc sỹ
                                    @endif
                                </p>
                                <p> Đơn vị:
                                    {{$u->congtac}}
                                </p>
                                <p> Thông tin:
                                    {!!$u->thongtin!!}
                                </p>
                                <p>Thù lao: 
                                    {{$u->thulao}}
                                </p>
                            </td>
                            <td>
                                <p> Môn:
                                    @foreach($u->metamon as $mm)
                                        <span class="btn btn-outline-success lineh1 mgr5 mgb5">{{$mm->mon->ten }}</span>
                                    @endforeach
                                </p>
                                <p> Quận: 
                                    @foreach($u->metaquan as $mq)
                                        <span class="btn btn-outline-success lineh1 mgr5 mgb5">{{$mq->quan->ten }}</span>
                                    @endforeach
                                </p>
                                <p> Lớp:
                                    @foreach($u->metalop as $mp)
                                        <span class="btn btn-outline-success lineh1 mgr5 mgb5">{{$mp->lop->ten }}</span>
                                    @endforeach
                                </p>
                            </td>
                            
                            <td>
                                <a data-id="{{$u->id}}" href="" class="badge badge-info btn-duyetgs">Duyệt</a>
                            </td>
                            <td>
                                <a  href="{{route('del-Giasudk',$u->id)}}" class="badge badge-danger" onclick="return confirm('Bạn chắc chắn muốn xoá?')">Xoá</a>
                            </td>
                            

                        </tr>
                    @endforeach
                </tbody>
            </table>
            {{$listGiasudk->render()}}
        </div>
    </div>
</div>

@endsection