@extends('admin.master')
@section('tilte_site', 'Danh sách đăng ký')
@section('content')
<div class="breadcrumbs">
    <div class="col-sm-12">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Danh sách Phụ huynh đăng ký tìm gia sư</h1>
            </div>
        </div>
    </div>
</div>
@if (session('success'))
    <div class="alert alert-success">
        {{ session('success') }}
    </div>
@endif
@if (session('fail'))
    <div class="alert alert-danger">
        {{ session('fail') }}
    </div>
@endif
<div class="container-fluid list-cate">
	<div class="row">
        <div class="card-body">
            <table class="table table-bordered">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Tên Phụ huynh</th>
                        <th scope="col">Số điện thoại</th>
                        <th scope="col">Địa chỉ</th>
                        <th scope="col">Lớp</th>
                        <th scope="col">Môn học</th>
                        <th scope="col">Học phí min (VND/2tiếng)</th>
                        <th scope="col">Học phí max (VND/2tiếng)</th>
                        <th scope="col">Học phí (VND/2tiếng)</th>
                        <th scope="col">Yêu cầu trình độ</th>
                        <th scope="col">Ghi chú</th>
                        <th scope="col">Trạng thái</th>
                        <th scope="col"></th>
                        <th scope="col"></th>
                    </tr>
                </thead>
                <tbody> 
                    @foreach($listHocsinh as $u)
                        <tr class="@php echo ($u->trangthai == 0) ?'':'bgrc' @endphp">
                            <th scope="row">{{$u->id}}</th>
                            <td>{{$u->ten}}</td>
                            <td>{{$u->dienthoai}}</td>
                            <td>{{$u->diachi}}</td>
                            <td>{{$u->lop}}</td>
                            <td>{{$u->mon}}</td>
                            <td>{{$u->hocphimin}}</td>
                            <td>{{$u->hocphimax}}</td>
                            <td>{{$u->hocphi}}</td>
                            <td>{{$u->yeucautrinhdo}}</td>
                            <td>{!!$u->ghichu!!}</td>
                            @if($u->trangthai == 0)
                            <td>Chưa xử lý</td>
                            @else
                            <td>Đã xử lý</td>
                            @endif
                            <td>
                                <a  href="{{route('showedit-Hocsinh',$u->id)}}" class="badge badge-info">Sửa</a>
                            </td>
                            <td>
                                <a  href="{{route('del-Hocsinh',$u->id)}}" class="badge badge-danger" onclick="return confirm('Bạn chắc chắn muốn xoá?')">Xoá</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            {{$listHocsinh->render()}}
        </div>
    </div>
</div>

@endsection