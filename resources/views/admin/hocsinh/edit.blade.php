@extends('admin.master')
@section('tilte_site', 'Chỉnh sửa Học sinh')
@section('content')
<script src="/tinymce/tinymce.min.js?v=3"></script>
<script>
    tinymce.init({
        selector: '#ghichu'
    });
</script>
<form action="" method="post" enctype="multipart/form-data">
    <div class="breadcrumbs">
        <div class="container-fluid">
            <div class="col-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Thêm mới Bài viết</h1>
                    </div>
                </div>
            </div>
            <div class="col-8 ">
                <div class="row d-flex justify-content-end pd10-0">
                    <button type="submit" class="btn btn-primary btn-sm mgr20">
                        <i class="fa fa-dot-circle-o"></i> Lưu lại
                    </button>
                    <button type="reset" class="btn btn-danger btn-sm">
                        <i class="fa fa-refresh"></i> Làm mới
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <input type="hidden" name="_token" value="{{csrf_token()}}" /> 
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="row mgt50">
            <div class="col-sm-12 col-md-12 col-lg-12  col-xl-7">
                <div class="row form-group">
                    <div class="col-sm-12 col-md-3">
                        <label for="ten" class=" form-control-label">Tên </label>
                    </div>
                    <div class="col-sm-12 col-md-9">
                        <input type="text" id="ten" name="ten" placeholder="Nhập tên học sinh" class="form-control-success form-control" value="{{$showeditHocsinh->ten}}">
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-sm-12 col-md-3">
                        <label for="dienthoai" class=" form-control-label">Số điện thoại </label>
                    </div>
                    <div class="col-sm-12 col-md-9">
                        <input type="text" id="dienthoai" name="dienthoai" placeholder="Nhập số điện thoại" class="form-control-success form-control" value="{{$showeditHocsinh->dienthoai}}">
                    </div>
                </div>
                
                <div class="row form-group">
                    <div class="col-sm-12 col-md-3">
                        <label for="diachi" class=" form-control-label">Địa chỉ </label>
                    </div>
                    <div class="col-sm-12 col-md-9">
                        <input type="text" id="diachi" name="diachi" placeholder="Nhập địa chỉ" class="form-control-success form-control" value="{{$showeditHocsinh->diachi}}">
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-sm-12 col-md-3">
                        <label for="lop" class=" form-control-label">Lớp</label>
                    </div>
                    <div class="col-sm-12 col-md-9">
                        <input type="text" id="lop" name="lop" placeholder="Nhập lớp" class="form-control-success form-control" value="{{$showeditHocsinh->lop}}">
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-sm-12 col-md-3">
                        <label for="mon" class=" form-control-label">Môn học</label>
                    </div>
                    <div class="col-sm-12 col-md-9">
                        <input type="text" id="mon" name="mon" placeholder="Nhập môn học" class="form-control-success form-control" value="{{$showeditHocsinh->mon}}">
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-sm-12 col-md-3">
                        <label for="yeucautrinhdo" class=" form-control-label">Yêu cầu trình độ</label>
                    </div>
                    <div class="col-sm-12 col-md-9">
                        <input type="text" id="yeucautrinhdo" name="yeucautrinhdo" placeholder="Yêu cầu trình độ" class="form-control-success form-control" value="{{$showeditHocsinh->yeucautrinhdo}}">
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-sm-12 col-md-3">
                        <label for="trangthai" class=" form-control-label">Trạng thái</label>
                    </div>
                    <div class="col-sm-12 col-md-9">
                        <select class="form-control-success form-control" name="trangthai" id="trangthai">
                            <option value="1">Đã xử lý</option>
                            <option value="0">Chưa xử lý</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-5">
                <div class="row form-group">
                    <div class="col-sm-12 col-md-3">
                        <label for="hocphimin" class=" form-control-label">Học phí min</label>
                    </div>
                    <div class="col-sm-12 col-md-9">
                        <input type="number" id="hocphimin" name="hocphimin" placeholder="Nhập học phí min" class="form-control-success form-control" value="{{$showeditHocsinh->hocphimin}}">
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-sm-12 col-md-3">
                        <label for="hocphimax" class=" form-control-label">Học phí max</label>
                    </div>
                    <div class="col-sm-12 col-md-9">
                        <input type="number" id="hocphimax" name="hocphimax" placeholder="Nhập học phí max" class="form-control-success form-control" value="{{$showeditHocsinh->hocphimax}}">
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-sm-12 col-md-3">
                        <label for="hocphi" class=" form-control-label">Học phí </label>
                    </div>
                    <div class="col-sm-12 col-md-9">
                        <input type="number" id="hocphi" name="hocphi" placeholder="Nhập học phí " class="form-control-success form-control" value="{{$showeditHocsinh->hocphi}}">
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-sm-12 col-md-3">
                        <label for="hocphi" class=" form-control-label">Ghi chú </label>
                    </div>
                    <div class="col-sm-12 col-md-9">
                    <textarea class="form-control-success form-control" name="ghichu" id="ghichu">{!!$showeditHocsinh->ghichu!!}</textarea>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>


@endsection
