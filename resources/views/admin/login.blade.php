<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="en">
<!--<![endif]-->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Đăng nhập admin</title>
    <meta name="description" content="Sufee Admin - HTML5 Admin Template">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="apple-icon.png">
    <link rel="shortcut icon" href="favicon.ico">

    <link rel="stylesheet" href="{{url('/')}}/css_admin/vendors/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{url('/')}}/css_admin/vendors/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{url('/')}}/css_admin/vendors/themify-icons/css/themify-icons.css">
    <link rel="stylesheet" href="{{url('/')}}/css_admin/vendors/flag-icon-css/css/flag-icon.min.css">
    <link rel="stylesheet" href="{{url('/')}}/css_admin/vendors/selectFX/css/cs-skin-elastic.css">
    <link rel="stylesheet" href="{{url('/')}}/css_admin/vendors/jqvmap/dist/jqvmap.min.css">
    <link rel="stylesheet" href="{{url('/')}}/public/css_new/themes/theme01/assets/css/web.css">


    <link rel="stylesheet" href="{{url('/')}}/css_admin/assets/css/style.css">

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>

</head>

<body>

    <div id="right-panel" class="right-panel">
          <div class="container">
            <div class="">
                <div class="alert alert-success"><p><strong>Chào bạn đến với trang quản trị mời bạn đăng nhập</strong></p></div>
                
                <div class="col-md-6 offset-md-3">
                <div class="register">
                    @if (session('success'))
                        <div class="alert alert-success">
                            {{ session('success') }}
                        </div>
                    @endif
                    @if (session('fail'))
                        <div class="alert alert-danger">
                            {{ session('fail') }}
                        </div>
                    @endif

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <h4 class="h4dangnhap">Đăng nhập</h4>
                    <br />
                    <form method="POST" >
                        <input type="hidden" name="_token" value="{{csrf_token()}}" />
                        <div class="form-group" data-validate = "Valid email is: a@b.c">
                            <input type="email" class="form-control" id="email" name="email" placeholder="Tài khoản" required="required">
                        </div>
                        <div class="form-group" data-validate="Enter password">
                            <input type="password" class="form-control" id="password" name="password" placeholder="Mật khẩu" required="required">
                        </div>
                        <div class="form-group text-center">
                            <!-- <button class="btn btndnhap">Đăng nhập</button> -->
                            <input type="submit" value="Đăng Nhập" class="btn btn-success btndnhap" />
                        </div>
                       
                    </form>
                </div>
            </div>
            </div>
        </div> 
    </div><!-- /#right-panel -->

    <!-- Right Panel -->

    <script src="{{url('/')}}/css_admin/vendors/jquery/dist/jquery.min.js"></script>
    <script src="{{url('/')}}/css_admin/vendors/popper.js/dist/umd/popper.min.js"></script>
    <script src="{{url('/')}}/css_admin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="{{url('/')}}/css_admin/assets/js/main.js"></script>


    <script src="{{url('/')}}/css_admin/vendors/chart.js/dist/Chart.bundle.min.js"></script>
    <script src="{{url('/')}}/css_admin/assets/js/dashboard.js"></script>
    <script src="{{url('/')}}/css_admin/assets/js/widgets.js"></script>
    <script src="{{url('/')}}/css_admin/vendors/jqvmap/dist/jquery.vmap.min.js"></script>
    <script src="{{url('/')}}/css_admin/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
    <script src="{{url('/')}}/css_admin/vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
    <script>
        (function($) {
            "use strict";

            jQuery('#vmap').vectorMap({
                map: 'world_en',
                backgroundColor: null,
                color: '#ffffff',
                hoverOpacity: 0.7,
                selectedColor: '#1de9b6',
                enableZoom: true,
                showTooltip: true,
                values: sample_data,
                scaleColors: ['#1de9b6', '#03a9f5'],
                normalizeFunction: 'polynomial'
            });
        })(jQuery);
    </script>

</body>

</html>
