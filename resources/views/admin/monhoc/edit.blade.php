@extends('admin.master')
@section('tilte_site', 'Chỉnh sửa Môn học')
@section('content')
<div class="breadcrumbs">
    <div class="col-sm-12 col-md-12 col-lg-12  col-xl-6">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Chỉnh sửa Môn học</h1>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">
    <form action="" method="post" enctype="multipart/form-data">
        <input type="hidden" name="_token" value="{{csrf_token()}}" /> 
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="row mgt50">
            <div class="col-sm-12 col-md-12 col-lg-12  col-xl-6">
                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="ten" class=" form-control-label">Tên </label>
                    </div>
                    <div class="col-12 col-md-9">
                        <input type="text" id="ten" name="ten" placeholder="Nhập tên môn học" class="form-control-success form-control" value="{{$showeditMonhoc->ten}}">
                    </div>
                </div>
            </div>
        </div>
        <div class="row mgb50">
            <div class="col-sm-12 col-md-12 col-lg-12  col-xl-6 d-flex justify-content-center">
                <button type="submit" class="btn btn-primary btn-sm mgr20">
                    <i class="fa fa-dot-circle-o"></i> Lưu lại
                </button>
                <button type="reset" class="btn btn-danger btn-sm">
                    <i class="fa fa-refresh"></i> Làm mới
                </button>
            </div>
        </div>
    </form>
</div>


@endsection
