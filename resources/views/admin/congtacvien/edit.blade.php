@extends('admin.master')
@section('tilte_site', 'Chỉnh sửa thông tin Cộng tác viên')
@section('content')


<form action="" method="post" enctype="multipart/form-data">
    <div class="breadcrumbs">
        <div class="container-fluid">
            <div class="col-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Chỉnh sửa thông tin Cộng tác viên</h1>
                    </div>
                </div>
            </div>
            <div class="col-8 ">
                <div class="row d-flex justify-content-end pd10-0">
                    <button type="submit" class="btn btn-primary btn-sm mgr20">
                        <i class="fa fa-dot-circle-o"></i> Lưu lại
                    </button>
                    <button type="reset" class="btn btn-danger btn-sm">
                        <i class="fa fa-refresh"></i> Đặt lại
                    </button>
                </div>
            </div>
        </div>
    </div>
        
    <div class="container-fluid mgb50">
        <input type="hidden" name="_token" value="{{csrf_token()}}" /> 
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @if (session('fail'))
            <div class="alert alert-danger">
                {{ session('fail') }}
            </div>
        @endif
        <div class="row mgt50">
            <div class="col-sm-12 col-md-12 col-lg-12  col-xl-6">
                <div class="row form-group">
                    <div class="col-sm-12 col-md-3">
                        <label for="ten" class=" form-control-label">Tên </label>
                    </div>
                    <div class="col-sm-12 col-md-9">
                        <input type="text" id="ten" name="ten" placeholder="Nhập tên cộng tác viên" class="form-control-success form-control" value="{{$showeditCongtacvien->ten}}">
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-sm-12 col-md-3">
                        <label for="dienthoai" class=" form-control-label">Điện thoại</label>
                    </div>
                    <div class="col-sm-12 col-md-9">
                        <input type="text" id="dienthoai" name="dienthoai" placeholder="Nhập số điện thoại" class="form-control-success form-control" value="{{$showeditCongtacvien->dienthoai}}">
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-sm-12 col-md-3">
                        <label for="email" class=" form-control-label">Email</label>
                    </div>
                    <div class="col-sm-12 col-md-9">
                        <input type="text" id="email" name="email" placeholder="Nhập email" class="form-control-success form-control" value="{{$showeditCongtacvien->email}}">
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-sm-12 col-md-3">
                        <label for="" class=" form-control-label">Trạng thái</label>
                    </div>
                    <div class="col-sm-12 col-md-9">
                        <select name="status" id="status" class="form-control-success form-control">
                            @if($showeditCongtacvien->status == 0)
                                <option value="0">Khoá</option>
                                <option value="1">Mở</option>
                            @else
                                <option value="1">Mở</option>
                                <option value="0">Khoá</option>
                            @endif
                        </select>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-sm-12 col-md-3">
                        <label for="" class=" form-control-label">Mã giới thiệu</label>
                    </div>
                    
                    <div class="col-sm-12 col-md-9">
                        <input type="text" id="magioithieu" name="magioithieu"  class="form-control-success form-control " value="{{$showeditCongtacvien->magioithieu}}" disabled>
                         <!-- <a href="javascript:void(0)" data-id="{{$showeditCongtacvien->id}}"  class="guimail mgt5 btn btn-outline-success">Gửi mail</a>  -->
                        <!-- <a href="javascript:void(0)" class="taomagioithieu mgt5 btn btn-outline-success">Tạo mã giới thiệu</a> -->
                    </div>
                    
                </div>
            </div>
            <div class="col-sm-12 col-md-12 col-lg-12  col-xl-6">
                
                <div class="row form-group">
                    <div class="col-sm-12 col-md-3">
                        <label class=" form-control-label">Ảnh đại diện</label>
                    </div>
                    <div class="col-sm-12 col-md-9">
                        <label for="anh" class="orm-control-label label-input-file" >Chọn ảnh khác</label>
                        <input type="file" name="anh" id="anh" class="displaynone" onchange="preview_image(event)">
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-sm-12 col-md-3">
                        
                    </div>
                    <div class="col-sm-12 col-md-9">
                        <img id='showavatar' class='w-50' src="{{url('/')}}/images/anhcongtacvien/{{$showeditCongtacvien->anh}}">
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
<script>
    function preview_image(event){
        var reader = new FileReader();
        reader.onload = function()
        {
            var output = document.getElementById('showavatar');
            output.src = reader.result;
        }
        reader.readAsDataURL(event.target.files[0]);
    }
</script>

@endsection

