@extends('admin.master')
@section('tilte_site', 'Danh sách đăng ký Cộng tác viên')
@section('content')
<div class="breadcrumbs">
    <div class="col-sm-12">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Danh sách đăng ký Cộng tác viên</h1>
            </div>
        </div>
    </div>
</div>
@if (session('success'))
    <div class="alert alert-success">
        {{ session('success') }}
    </div>
@endif
@if (session('fail'))
    <div class="alert alert-danger">
        {{ session('fail') }}
    </div>
@endif
<div class="container-fluid list-cate">
	<div class="row">
        <div class="col-12">
            <table class="table table-bordered">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Tên CTV</th>
                        <th scope="col">Ảnh</th>
                        <th scope="col">Số điện thoại</th>
                        <th scope="col">Email</th>
                        <th scope="col">Mã giới thiệu</th>
                        <th scope="col" class="text-center">Trạng thái</th>
                        <th scope="col">#</th>
                        <th scope="col">#</th>
                    </tr>
                </thead>
                <tbody> 
                    @foreach($listCongtacvien as $u)
                        <tr>
                            <th scope="row">{{$u->id}}</th>
                            <td>{{$u->ten}}</td>
                            <td>
                            <img class="avatar" src="{{url('/')}}/images/anhcongtacvien/{{$u->anh}}" >
                            </td>
                            <td>{{$u->dienthoai}}</td>
                            <td>{{$u->email}}</td>
                            <td>{{$u->magioithieu}}</td>
                            @if($u->status == 0)
                                <td class="text-center">
                                    <input type="checkbox" name="" id="" data-id="{{$u->id}}" class="trangthaictv">
                                </td>
                            @else
                                <td class="text-center">
                                    <input type="checkbox" name="" id="" data-id="{{$u->id}}" class="trangthaictv" checked>
                                </td>
                            @endif
                            <td>
                                <a  href="{{route('showedit-Congtacvien',$u->id)}}" class="badge badge-info">Sửa</a>
                            </td>
                            <td>
                                <a  href="" class="badge badge-danger" onclick="return confirm('Bạn chắc chắn muốn xoá?')">Xoá</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            {{$listCongtacvien->render()}}
        </div>
    </div>
</div>

@endsection