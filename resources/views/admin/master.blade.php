<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="en">
<!--<![endif]-->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>@yield('tilte_site')</title>
    <meta name="description" content="Sufee Admin - HTML5 Admin Template">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="__token" content="{{csrf_token()}}">

    <link rel="apple-touch-icon" href="apple-icon.png">
    <link rel="shortcut icon" href="https://vinastudy.vn/favicon.ico">

    <link rel="stylesheet" href="{{url('/')}}/css_admin/vendors/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{url('/')}}/css_admin/vendors/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{url('/')}}/css_admin/vendors/themify-icons/css/themify-icons.css">
    <link rel="stylesheet" href="{{url('/')}}/css_admin/vendors/flag-icon-css/css/flag-icon.min.css">
    <link rel="stylesheet" href="{{url('/')}}/css_admin/vendors/selectFX/css/cs-skin-elastic.css">
    <link rel="stylesheet" href="{{url('/')}}/css_admin/vendors/jqvmap/dist/jqvmap.min.css">

    <link rel="stylesheet" href="{{url('/')}}/css_admin/assets/css/toastr.min.css">
    <link rel="stylesheet" href="{{url('/')}}/css_admin/assets/css/style.css">
    
    <link rel="stylesheet" href="{{url('/')}}/css_admin/assets/css/web.css">

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>

</head>

<body>


    <!-- Left Panel -->

    <aside id="left-panel" class="left-panel">
        <nav class="navbar navbar-expand-sm navbar-default">

            <div class="navbar-header">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand" href="/admin"><img src="{{url('/')}}/css_admin/images/logo.png" alt="Logo"></a>
            </div>

            <div id="main-menu" class="main-menu collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li class="active">
                        <a href="/"> <i class="menu-icon fa fa-dashboard"></i>Back to web </a>
                    </li>
                    <h3 class="menu-title">UI elements</h3><!-- /.menu-title -->
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-user"></i>Quản lý Người dùng</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-plus-square"></i><a href="{{route('showadd-User')}}">Thêm mới người dùng</a></li>
                            <li><i class="fa fa-list-ol"></i><a href="{{route('list-User')}}">Danh sách người dùng</a></li>
                        </ul>
                    </li>
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-list"></i>Quản lý danh mục</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-plus-square"></i><a href="{{route('showadd-Cate')}}">Thêm mới danh mục</a></li>
                            <li><i class="fa fa-list-ol"></i><a href="{{route('list-Cate')}}">Danh sách danh mục</a></li>
                        </ul>
                    </li>
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-edit"></i>Quản lý bài viết</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="menu-icon fa fa-plus-square"></i><a href="{{route('showadd-Product')}}">Thêm mới bài viết</a></li>
                            <li><i class="menu-icon fa fa-list-ol"></i><a href="{{route('list-Product')}}">Danh sách bài viết</a></li>
                        </ul>
                    </li>
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-book"></i>Quản lý môn học</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="menu-icon fa fa-plus-square"></i><a href="{{route('showadd-Monhoc')}}">Thêm mới môn học</a></li>
                            <li><i class="menu-icon fa fa-list-ol"></i><a href="{{route('list-Monhoc')}}">Danh sách môn học</a></li>
                        </ul>
                    </li>
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-truck"></i>Quản lý lớp</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="menu-icon fa fa-plus-square"></i><a href="{{route('showadd-Lop')}}">Thêm mới lớp</a></li>
                            <li><i class="menu-icon fa fa-list-ol"></i><a href="{{route('list-Lop')}}">Danh sách lớp</a></li>
                        </ul>
                    </li>
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-dribbble"></i>Khu vực giảng dạy</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="menu-icon fa fa-plus-square"></i><a href="{{route('showadd-Quanhuyen')}}">Thêm mới quận huyện</a></li>
                            <li><i class="menu-icon fa fa-list-ol"></i><a href="{{route('list-Quanhuyen')}}">Danh sách quận huyện</a></li>
                        </ul>
                    </li>
                    <li class="menu-item-has-children dropdown">
                        <a href="{{route('list-Hocsinh')}}" > <i class="menu-icon fa fa-female"></i>Đăng ký tìm gia sư</a>
                    </li>
                    <li class="menu-item-has-children dropdown">
                        <a href="{{route('list-Giasudk')}}" > <i class="menu-icon fa fa-bookmark-o"></i>Đăng ký làm gia sư</a>
                    </li>
                    <li class="menu-item-has-children dropdown">
                        <a href="{{route('list-Giasu')}}" > <i class="menu-icon fa fa-users"></i>Danh sách gia sư </a>
                    </li>
                    <li class="menu-item-has-children dropdown">
                        <a href="{{route('list-Congtacvien')}}" > <i class="menu-icon fa fa-users"></i>Danh sách cộng tác viên  </a>
                    </li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </nav>
    </aside><!-- /#left-panel -->

    <!-- Left Panel -->

    <!-- Right Panel -->

    <div id="right-panel" class="right-panel">

        <!-- Header-->
        <header id="header" class="header">

            <div class="header-menu">

                <div class="col-sm-7">
                   
                    
                </div>

                <div class="col-sm-5">
                    <div class="user-area dropdown float-right">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <img class="user-avatar rounded-circle" src="{{url('/')}}/css_admin/images/admin.jpg" alt="User Avatar">
                        </a>

                        <div class="user-menu dropdown-menu">
                            @if(Auth::check())
                                <a class="nav-link" href="#"><i class="fa fa-user"></i> {{Auth::User()->name}} </a>
                                <a class="nav-link" href="{{route('logoutad')}}"><i class="fa fa-power-off"></i> Logout</a>
                            @else
                                <a class="nav-link" href="#"><i class="fa fa-user"></i> My Profile</a>

                                <a class="nav-link" href="#"><i class="fa fa-user"></i> Notifications <span class="count">13</span></a>

                                <a class="nav-link" href="#"><i class="fa fa-cog"></i> Settings</a>

                                <a class="nav-link" href="#"><i class="fa fa-power-off"></i> Logout</a>
                            @endif
                        </div>
                    </div>

                </div>
            </div>

        </header><!-- /header -->
        <!-- Header-->

        @yield('content')
    </div><!-- /#right-panel -->

    <!-- Right Panel -->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <script src="{{url('/')}}/css_admin/vendors/jquery/dist/jquery.min.js"></script>
   
        @yield('script')
   
    <script src="{{url('/')}}/css_admin/vendors/popper.js/dist/umd/popper.min.js"></script>
    <script src="{{url('/')}}/css_admin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="{{url('/')}}/css_admin/assets/js/main.js"></script>
    <script src="{{url('/')}}/css_admin/assets/js/webt.js"></script>
    <script src="{{url('/')}}/css_admin/assets/js/toastr.min.js"></script>


    <!-- <script src="{{url('/')}}/css_admin/vendors/chart.js/dist/Chart.bundle.min.js"></script>
    <script src="{{url('/')}}/css_admin/assets/js/dashboard.js"></script>
    <script src="{{url('/')}}/css_admin/assets/js/widgets.js"></script>
    <script src="{{url('/')}}/css_admin/vendors/jqvmap/dist/jquery.vmap.min.js"></script>
    <script src="{{url('/')}}/css_admin/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
    <script src="{{url('/')}}/css_admin/vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
    <script>
        (function($) {
            "use strict";

            jQuery('#vmap').vectorMap({
                map: 'world_en',
                backgroundColor: null,
                color: '#ffffff',
                hoverOpacity: 0.7,
                selectedColor: '#1de9b6',
                enableZoom: true,
                showTooltip: true,
                values: sample_data,
                scaleColors: ['#1de9b6', '#03a9f5'],
                normalizeFunction: 'polynomial'
            });
        })(jQuery);
    </script> -->
    
</body>

</html>
