@extends('user.main')
@section('tilte_site', 'Trở thành gia sư')
@section('description') 
    Vinastudy là website hàng đầu về học trực tuyến với các chương trình ôn, luyện thi violympic, thi toán violympic... dành cho học sinh tiểu học và trung học cơ sở. 
@endsection
@section('keyword')
    hoc truc tuyen, luyen thi violympic, luyen thi toan violympic, luyen thi violympic truc tuyen
@endsection
@section('content')

<div class="container-fluid bgrf7">
    <div class="container bao-form-dangkylamgiasu pd70-0">
        <div class="row d-flex justify-content-center">
            <div class="col-6 text-center pd0">
                <h2 class="title-timgs">Đăng Ký Thông Tin Gia Sư</h2>
                <span class="gs-luuy">Gia sư điền đầy đủ thông tin vào form dưới đây để đăng ký</span>
                <form action="" method="post" enctype="multipart/form-data" >
                    <input type="hidden" name="_token" value="{{csrf_token()}}" /> 
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    @if (session('fail'))
                        <div class="alert alert-danger">
                            {{ session('fail') }}
                        </div>
                    @endif
                    <div class="box-dangky-giasu">
                        <div class="row mgb5">
                            <div class="col-4 d-flex align-items-center pdr0">
                                <label class="label-dangkytimgs" for="ten">Họ và tên gia sư</label>
                            </div>
                            <div class="col-8">
                                <input class="form-giasu-dk" type="text" name="ten" id="ten" placeholder="Nhập họ và tên">
                            </div>
                        </div>
                        <div class="row mgb5">
                            <div class="col-4 d-flex align-items-center pdr0">
                                <label class="label-dangkytimgs" for="dienthoai">Số điện thoại liên hệ</label>
                            </div>
                            <div class="col-8">
                                <input class="form-giasu-dk" type="text" name="dienthoai" id="dienthoai" placeholder="Nhập số điện thoại">
                            </div>
                        </div>
                        <div class="row mgb5">
                            <div class="col-4 d-flex align-items-center pdr0">
                                <label class="label-dangkytimgs" for="email">Email</label>
                            </div>
                            <div class="col-8">
                                <input class="form-giasu-dk" type="text" name="email" id="email" placeholder="Nhập email">
                            </div>
                        </div>
                        <div class="row mgb5">
                            <div class="col-4 d-flex align-items-center pdr0">
                                <label class="label-dangkytimgs" for="">Bằng cấp, học vị</label>
                            </div>
                            <div class="col-8 ">
                                <div class="dropdown">
                                    <button class="form-giasu-dk dropdown-toggle text-in-button drbangcap" type="button" id="dropdownMenuButton drbangcap" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >Chọn 1 bằng cấp cao nhất của bạn
                                    </button>
                                    <div class="dropdown-menu  text-center" id="chonbang" aria-labelledby="dropdownMenuButton">
                                        <span class="labelbangcap" data-ten="Sinh viên" data-tenbodau="sinhvien"  data-id="1">Sinh viên</span>
                                        <span class="labelbangcap" data-ten="Cao đẳng" data-tenbodau="caodang"  data-id="2">Cao đẳng</span>
                                        <span class="labelbangcap" data-ten="Đại học" data-tenbodau="daihoc"  data-id="3">Đại học</span>
                                        <span class="labelbangcap" data-ten="Thạc sỹ" data-tenbodau="thacsy"  data-id="4">Thạc sỹ</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mgb5">
                            <div class="col-4 d-flex align-items-center pdr0">
                            </div>
                            <div class="col-8 d-flex align-items-start ">
                                <div class="row mg0 area-bangcapchon">
                                    
                                </div>
                            </div>
                        </div>
                        <div class="row mgb5">
                            <div class="col-4 d-flex align-items-center pdr0">
                                <label class="label-dangkytimgs" for="congtac">Nơi công tác, học tập</label>
                            </div>
                            <div class="col-8">
                                <input class="form-giasu-dk" type="text" name="congtac" id="congtac" placeholder="Nhập đơn vị công tác">
                            </div>
                        </div>
                        <div class="row mgb5">
                            <div class="col-4 d-flex align-items-center pdr0">
                                <label class="label-dangkytimgs" for="">Môn học có thể dạy</label>
                            </div>
                            <div class="col-8 ">
                                <div class="dropdown">
                                    <button class="form-giasu-dk dropdown-toggle text-in-button" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >Chọn môn học
                                    </button>
                                    <div class="dropdown-menu  text-center" id="chonmon" aria-labelledby="dropdownMenuButton">
                                        @foreach($monhoc as $m)
                                            <span class="labelmon" data-ten="{{$m->ten}}" data-tenbodau="{{$m->tenbodau}}"  data-id="{{$m->id}}">{{$m->ten}}</span>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mgb5">
                            <div class="col-4 d-flex align-items-center pdr0">
                            </div>
                            <div class="col-8 d-flex align-items-start ">
                                <div class="row mg0 area-monchon">
                                    
                                </div>
                            </div>
                        </div>
                        <div class="row mgb5">
                            <div class="col-4 d-flex align-items-center pdr0">
                                <label class="label-dangkytimgs" for="">Khu vực có thể dạy</label>
                            </div>
                            <div class="col-8 ">
                                <div class="dropdown">
                                    <button class="form-giasu-dk dropdown-toggle text-in-button" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Chọn quận, huyện
                                    </button>
                                    <div class="dropdown-menu   text-center" id="chonquan" aria-labelledby="dropdownMenuButton">
                                        <input class="form-giasu-dk timquan" placeholder="Tìm quận" type="text" name="" id="timquan" onchange="timquan1()" onkeyup="timquan1()">
                                        @foreach($quanhuyen as $q)
                                            <span class="labelquan" data-ten="{{$q->ten}}" data-tenbodau="{{$q->tenbodau}}" data-tentimkiem="{{$q->tentimkiem}}" data-id="{{$q->id}}">{{$q->ten}} <p class="displaynone">{{$q->tentimkiem}}</p></span>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mgb5">
                            <div class="col-4 d-flex align-items-center pdr0">
                            </div>
                            <div class="col-8 d-flex align-items-start">
                                <div class="row mg0 area-quanchon">
                                    
                                </div>
                            </div>
                        </div>
                        <div class="row mgb5">
                            <div class="col-4 d-flex align-items-center pdr0">
                                <label class="label-dangkytimgs" for="">Lớp có thể dạy</label>
                            </div>
                            <div class="col-8 ">
                                <div class="dropdown">
                                    <button class="form-giasu-dk dropdown-toggle text-in-button" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Chọn lớp
                                    </button>
                                    <div class="dropdown-menu  text-center" id="chonlop" aria-labelledby="dropdownMenuButton">
                                        @foreach($lop as $q)
                                            <span class="labellop" data-ten="{{$q->ten}}" data-tenbodau="{{$q->tenbodau}}"  data-id="{{$q->id}}">{{$q->ten}}</span>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mgb5">
                            <div class="col-4 d-flex align-items-center pdr0">
                            </div>
                            <div class="col-8 d-flex align-items-start">
                                <div class="row mg0 area-lopchon">
                                    
                                </div>
                            </div>
                        </div>
                        <div class="row mgb5">
                            <div class="col-4 d-flex align-items-center pdr0">
                                <label class="label-dangkytimgs" for="thongtin">Thành tích cá nhân</label>
                            </div>
                            <div class="col-8">
                                <textarea class="form-giasu-dk" name="thongtin" id="thongtin"  rows="5" placeholder="Nhập ngắn gọn thành tích, kinh nghiệm làm việc, học tập"></textarea>
                            </div>
                        </div>
                        <div class="row mgb5">
                            <div class="col-4 d-flex align-items-center pdr0">
                                <label class="label-dangkytimgs" for="thulao">Thù lao mong muốn</label>
                            </div>
                            <div class="col-8">
                                <div class="row align-items-center mg0">
                                    <input type="number" name="thulao" id="thulao" class="form-giasu-dk w50 " min="0" placeholder="100000"> <span class="fwbold">VND/giờ</span>
                                </div>
                            </div>
                        </div>
                        <div class="row mgb5">
                            <div class="col-4 d-flex align-items-center pdr0">
                                <label class="label-dangkytimgs" for="magioithieu">Mã giới thiệu</label>
                            </div>
                            <div class="col-8">
                                <input class="form-giasu-dk" type="text" name="magioithieu" id="magioithieu" placeholder="Nhập mã giới thiệu nếu có">
                            </div>
                        </div>
                        <div class="row mgb5">
                            <div class="col-4 d-flex align-items-center pdr0">
                                <label class="label-dangkytimgs ">Ảnh đại diện</label>
                            </div>
                            <div class="col-8 d-flex justify-content-start ">
                                <label class="text-in-button label-chonanh" for="anh"> <span class="icon icon-upload"></span>Chọn ảnh</label>
                                <input type="file" class="displaynone" name="anh" id="anh" onchange="preview_image(event)">
                            </div>
                        </div>
                        <div class="row mgb5">
                        <div class="col-4 pdr0">
                                
                            </div>
                            <div class="col-8 d-flex justify-content-start">
                               <img id='showavatar' class='showavatar'>
                            </div>
                        </div>
                        <div class="row mgb5 mgt20">
                            <div class="col-4">
                                
                            </div>
                            <div class="col-8 d-flex justify-content-start">
                                <button type="submit" class="btn-guithongtin">Gửi thông tin</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="container pdb50">
        <h2 class="title-danhsachgiasu">Quy trình tuyển dụng gia sư</h2>
        <div class="row justify-content-center" >
            <div class="col-10 bao-quydinh">
                
            {!!$quydinh['mota']!!}

            </div>
        </div>
    </div>
</div>
<script>
    function preview_image(event){
            var reader = new FileReader();
            reader.onload = function()
            {
                var output = document.getElementById('showavatar');
                output.src = reader.result;
            }
            reader.readAsDataURL(event.target.files[0]);
        }
</script>
@endsection
