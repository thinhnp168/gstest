@extends('user.main')
@section('tilte_site', 'Giới thiệu')
@section('description') 
    Vinastudy là website hàng đầu về học trực tuyến với các chương trình ôn, luyện thi violympic, thi toán violympic... dành cho học sinh tiểu học và trung học cơ sở. 
@endsection
@section('keyword')
    hoc truc tuyen, luyen thi violympic, luyen thi toan violympic, luyen thi violympic truc tuyen
@endsection
@section('content')
<div class="container-fluid bgrf7 gioithieu pdb50">
    <div class="container">
        <div class="row">
            <div class="col-9">
                <h1 class="title">Giới thiệu</h1>
            </div>
            <div class="col-3"></div>
        </div>
        <div class="row">
            <div class="col-9">
                <div class="bao-noidung">
                
                    {!!$gioithieu2['mota']!!}
                </div>
            </div>
            <div class="col-3">
                @include('user.danhmucbaiviet.danhmuc')
            </div>
        </div>
    </div>
</div>
@endsection
