@extends('user.main')
@section('tilte_site', 'Trở thành cộng tác viên')
@section('description') 
    Vinastudy là website hàng đầu về học trực tuyến với các chương trình ôn, luyện thi violympic, thi toán violympic... dành cho học sinh tiểu học và trung học cơ sở. 
@endsection
@section('keyword')
    hoc truc tuyen, luyen thi violympic, luyen thi toan violympic, luyen thi violympic truc tuyen
@endsection
@section('content')

<div class="container-fluid bgrf7">
    <div class="container bao-form-dangkylamgiasu pd70-0">
        <div class="row d-flex justify-content-center">
            <div class="col-6 text-center pd0">
                <h2 class="title-timgs">Đăng Ký làm cộng tác viên</h2>
                <div class="quyenloictv pdt30">
                    <p class="title-quyenloi">Quyền lợi khi làm cộng tác viên tại trung tâm Gia sư Thành Long:</p>
                    <ul>
                        <li>Lorem ipsum dolor sit amet consectetur adipisicing elit. Numquam recusandae ducimus reiciendis ea voluptate asperiores fugiat commodi ipsam et, velit, expedita harum! Laboriosam nesciunt, temporibus saepe a est amet aliquam!</li>
                        <li>Fugiat, inventore atque porro officia, enim fuga tempora sunt accusantium, ab autem dolorem harum dolores veniam odio voluptate sint accusamus nihil quam in assumenda ullam perspiciatis consectetur vitae est? Exercitationem.</li>
                        <li>Fuga, nam pariatur. Eligendi obcaecati vitae quod laboriosam velit molestias rerum tempore maiores soluta eos quis reprehenderit facilis ipsam, earum delectus, nisi quidem iusto ut, recusandae deserunt temporibus distinctio illo.</li>
                    </ul>
                </div>
                <form action="" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{csrf_token()}}" /> 
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    @if (session('fail'))
                        <div class="alert alert-danger">
                            {{ session('fail') }}
                        </div>
                    @endif
                    <div class="box-dangky-giasu">
                        <div class="row mgb5">
                            <div class="col-4 d-flex align-items-center pdr0">
                                <label class="label-dangkytimgs" for="ten">Họ và tên</label>
                            </div>
                            <div class="col-8">
                                <input class="form-giasu-dk" type="text" name="ten" id="ten" placeholder="Nhập họ và tên">
                            </div>
                        </div>
                        <div class="row mgb5">
                            <div class="col-4 d-flex align-items-center pdr0">
                                <label class="label-dangkytimgs" for="dienthoai">Số điện thoại liên hệ</label>
                            </div>
                            <div class="col-8">
                                <input class="form-giasu-dk" type="text" name="dienthoai" id="dienthoai" placeholder="Nhập số điện thoại">
                            </div>
                        </div>
                        <div class="row mgb5">
                            <div class="col-4 d-flex align-items-center pdr0">
                                <label class="label-dangkytimgs" for="email">Email</label>
                            </div>
                            <div class="col-8">
                                <input class="form-giasu-dk" type="text" name="email" id="email" placeholder="Nhập email ">
                            </div>
                        </div>
                        <div class="row mgb5">
                            <div class="col-4 d-flex align-items-center pdr0">
                                <label class="label-dangkytimgs ">Ảnh đại diện</label>
                            </div>
                            <div class="col-8 d-flex justify-content-start ">
                                <label class="text-in-button label-chonanh" for="anh"> <span class="icon icon-upload"></span>Chọn ảnh</label>
                                <input type="file" class="displaynone" name="anh" id="anh" onchange="preview_image(event)">
                            </div>
                        </div>
                        <div class="row mgb5">
                        <div class="col-4 pdr0">
                                
                            </div>
                            <div class="col-8 d-flex justify-content-start">
                               <img id='showavatar' class='showavatar'>
                            </div>
                        </div>
                        <div class="row mgb5 mgt20">
                            <div class="col-4">
                                
                            </div>
                            <div class="col-8 d-flex justify-content-start">
                                <button type="submit" class="btn-guithongtin-ctv">Gửi thông tin</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    function preview_image(event){
            var reader = new FileReader();
            reader.onload = function()
            {
                var output = document.getElementById('showavatar');
                output.src = reader.result;
            }
            reader.readAsDataURL(event.target.files[0]);
        }
</script>
@endsection
