<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('tilte_site')</title>
    <meta name="description" content="@yield('description')"/>
    <meta name="keywords" content="@yield('keyword')">
    <meta property="og:url" content="http://giasuthanhlong.com">
    <meta property="og:type" content="website">
    <meta property="og:description" content="@yield('description')">
    <meta property="og:keywords" content="@yield('keyword')">
    <meta property="og:title" content="@yield('tilte_site')">
    <meta property="og:image" content="https://vinastudy.vn/templates/images/banner_fb.jpg">
    <link rel="canonical" href="http://giasuthanhlong.com">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="icon" href="https://vinastudy.vn/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="{{url('/')}}/css_user/asset/css/bootstrap-grid.min.css">
    <link rel="stylesheet" href="{{url('/')}}/css_user/asset/css/toastr.min.css">
    <link rel="stylesheet" href="{{url('/')}}/css_user/asset/css/web.css?v=1">
    
</head>
<body>
    <div class="container-fuild bgr-gradient">
        <div class="container menutop ">
            <div class="row">
                <div class="col-2">
                    <a href="/"><img src="{{url('/')}}/css_user/asset/images/logo.png" alt="Logo" /></a>
                </div>
                <div class="col-8 menu d-flex align-items-center">
                    <ul class="d-flex flex-row mg0">
                        <li><a href="{{route('gioithieu')}}">Giới thiệu</a></li>
                        <li><a href="{{route('showadd-Giasudk')}}">Trở thành gia sư</a></li>
                        <li><a href="{{route('timgiasu')}}">Tìm gia sư</a></li>
                        <li><a href="{{route('tintuc')}}">Tin tức</a></li>
                        <li><a href="{{route('showadd-Congtacvien')}}">Cộng tác viên</a></li>
                    </ul>
                </div>
                <div class="col-2 pdl0 d-flex align-items-center">
                    <a href="tel:0936456113" class="btn-hotline"><span class="icon icon-phone"></span>0936456113</a>
                </div>
            </div>
        </div>
    </div>
    @yield('content')
    <div class="container-fluid bgrfoot">
        <div class="container">
            <div class="row">
                <div class="col-8">
                    <img src="{{url('/')}}/css_user/asset/images/logo.png" alt="Logo" class="logofooter">
                    <ul class="info_company">
                        <li>
                            <p>Cơ quan chủ quản:<br />Công ty TNHH Dịch vụ Giáo dục và Công nghệ Việt Nam - MST 0106817063</p>
                        </li>
                        <li>
                            <span class="icon icon-diachi"></span>
                            <p>Trụ sở chính: Số 10D, Ngõ 325/69/14, Phố Kim Ngưu, Phường Thanh Lương , Quận Hai Bà Trưng, Hà Nội</p>
                        </li>
                        <li>
                            <span class="icon icon-diachi"></span>
                            <p>VP: Nhà 4 C1 khu tập thể C1, tổ dân phố 38 , phố Hoàng Ngọc Phách, Láng Hạ, Đống Đa, Hà Nội</p>
                        </li>
                        <li>
                            <span class="icon icon-dienthoai"></span>
                            <p>0932.39.39.56</p>

                        </li>
                        <li>
                            <span class="icon icon-emailbot"></span>
                            <p>hotro@vinastudy.vn</p>
                        </li>
                        <li>
                            <span class="icon icon-baocao"></span>
                        </li>
                    </ul>
                </div>
                <div class="col-4 ">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14897.745123555056!2d105.813009!3d21.0152224!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xc694e2c5420556fc!2zVmluYXN0dWR5IC0gVHLGsOG7nW5nIEjhu41jIFRy4buxYyBUdXnhur9u!5e0!3m2!1svi!2s!4v1574668148176!5m2!1svi!2s" width="300" height="200" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid bao-timefooter">
        <div class="container">
            <div class="row">
                <div class="col-6">
                    <img src="{{url('/')}}/css_user/asset/images/thoigianmocua.png" alt="Thời gian làm việc">
                </div>
                <div class="col-6">
                    <img src="{{url('/')}}/css_user/asset/images/phucvu247.png" alt="Thời gian làm việc">
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid copy-right">
        <div class="container" itemprop="brand" itemscope itemtype="http://schema.org/Brand" itemref="_logo5">
            <p class="text-center mg0">Bản quyền thuộc về trung tâm <span ><a href="https://vinastudy.vn/"><span itemprop="name">Vinastudy</span></a></span></p>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>
    <script src="{{url('/')}}/css_user/asset/js/toastr.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="{{url('/')}}/css_user/asset/js/web.js?v=1"></script>
    
</body>
</html>