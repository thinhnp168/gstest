@extends('user.main')
@section('tilte_site', 'Danh sách gia sư')
@section('description') 
    Vinastudy là website hàng đầu về học trực tuyến với các chương trình ôn, luyện thi violympic, thi toán violympic... dành cho học sinh tiểu học và trung học cơ sở. 
@endsection
@section('keyword')
    hoc truc tuyen, luyen thi violympic, luyen thi toan violympic, luyen thi violympic truc tuyen
@endsection
@section('content')

<div class="container-fluid bgrf7">
    <div class="container pd70-0">
        <h2 class="title-danhsachgiasu">Danh sách gia sư <input type="text" class="timgiasu" name="" id="timgiasu" placeholder="Tìm gia sư" onchange="searchgiasu()" onkeyup="searchgiasu()"></h2>
        @foreach($giasu as $g)
            <div class="row item-giasu">
                <div class="col-2 d-flex justify-content-center flex-column">
                    <img class="avatar" src="{{url('/')}}/images/anhgiasudangky/{{$g->anh}}" alt="{{$g->ten}}">
                    <p class="ma-gs">Mã GS: GS38{{$g->id}}</p>
                </div>
                <div class="col-10">
                    <div class="thongtin-gs">
                        <div class="row ">
                            <div class="col-6">
                                <p class="text-thongtin"><span>Họ và tên: </span>{{$g->ten}} <span class="displaynone">{{$g->tentimkiem}}</span> </p> 
                                @if($g->bangcap == 1)
                                <p class="text-thongtin"><span>Học vị/Bằng cấp: </span>Sinh viên</p>
                                @elseif($g->bangcap == 2)
                                <p class="text-thongtin"><span>Học vị/Bằng cấp: </span>Cao đẳng</p>
                                @elseif($g->bangcap == 3)
                                <p class="text-thongtin"><span>Học vị/Bằng cấp: </span>Đại học</p>
                                @elseif($g->bangcap == 4)
                                <p class="text-thongtin"><span>Học vị/Bằng cấp: </span>Thạc sỹ</p>
                                @endif
                                <p class="text-thongtin"><span>Môn dạy: </span>@foreach($g->metamon as $m)<label class="mg0">{{$m->mon->ten}}</label> @endforeach</p>
                                
                            </div>
                            <div class="col-6">
                                <p class="text-thongtin"><span>Khu vực giảng dạy: </span>@foreach($g->metaquan as $q)<label class="mg0">{{$q->quan->ten}}</label> @endforeach</p>
                                <p class="text-thongtin"><span>Khối lớp: </span>@foreach($g->metalop as $m)<label class="mg0">{{$m->lop->ten}}</label> @endforeach</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <p class="text-thongtin mgt0"><span>Thành tích cá nhân: </span>{!!$g->thongtin!!}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>
@endsection
