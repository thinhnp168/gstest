@extends('user.main')
@section('tilte_site', 'Gia sư Thành Long')
@section('description') 
    Vinastudy là website hàng đầu về học trực tuyến với các chương trình ôn, luyện thi violympic, thi toán violympic... dành cho học sinh tiểu học và trung học cơ sở. 
@endsection
@section('keyword')
    hoc truc tuyen, luyen thi violympic, luyen thi toan violympic, luyen thi violympic truc tuyen
@endsection
@section('content')
<div class="container-fuild bgr-gradient"> 
        <div class="container">
            <div class="row">
                <div class="col-8 pd0">
                    <h1 class="title-home">Trung tâm gia sư Thành Long</h1>
                    <p class="list-loiich">Trung tâm được sáng lập và điều hành bởi thầy giáo Nguyễn Thành Long.</p>
                    <p class="list-loiich">100% gia sư được đánh giá năng lực chuyên môn và định hướng kĩ năng giảng dạy.</p>
                    <p class="list-loiich">Trung tâm gửi đề kiểm tra định kì hàng tháng và xây dựng giáo trình tự học <br /> theo học lực cho học sinh.</p>
                    <p class="list-loiich">Học sinh được cung cấp khóa học online tại Vinastudy.vn để ôn luyện kiến thức.</p>
                    <p class="pdt50"><a href="{{route('timgiasu')}}" class="btn-timgiasu">Tìm gia sư ngay</a></p>
                </div>
                <div class="col-4 baoanhbia">
                    <img class="anhbia mgt40" src="{{url('/')}}/css_user/asset/images/anhbia.png" alt="" />
                    <p class="thongtin-gv">Thầy giáo: Nguyễn Thành Long</p>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid bgrf7">
        <div class="container bao-form-dangkygiasu">
            <div class="row d-flex justify-content-center">
                <div class="col-6 text-center pd0">
                    <h2 class="title-timgs">Đăng Ký Tìm Gia Sư </h2>
                    <form action="" method="post" enctype="multipart/form-data" autocomplete="off">
                        <input type="hidden" name="_token" value="{{csrf_token()}}" /> 
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <div class="box-dangky">
                            <div class="row mgb5">
                                <div class="col-4 d-flex align-items-center pdr0">
                                    <label class="label-dangkytimgs" for="">Họ và tên phụ huynh</label>
                                </div>
                                <div class="col-8">
                                    <input class="form-giasu" type="text" name="ten" id="ten">
                                </div>
                            </div>
                            <div class="row mgb5">
                                <div class="col-4 d-flex align-items-center pdr0">
                                    <label class="label-dangkytimgs" for="">Số điện thoại liên hệ</label>
                                </div>
                                <div class="col-8">
                                    <input class="form-giasu" type="text" name="dienthoai" id="dienthoai">
                                </div>
                            </div>
                            <div class="row mgb5">
                                <div class="col-4 d-flex align-items-center pdr0">
                                    <label class="label-dangkytimgs" for="">Địa chỉ</label>
                                </div>
                                <div class="col-8">
                                    <input class="form-giasu" type="text" name="diachi" id="diachi">
                                </div>
                            </div>
                            <div class="row mgb5">
                                <div class="col-4 d-flex align-items-center pdr0">
                                    <label class="label-dangkytimgs" for="">Con tôi học lớp</label>
                                </div>
                                <div class="col-8">
                                    <input class="form-giasu" type="text" name="lop" id="lop">
                                </div>
                            </div>
                            <div class="row mgb5">
                                <div class="col-4 d-flex align-items-center pdr0">
                                    <label class="label-dangkytimgs" for="">Môn học cần gia sư</label>
                                </div>
                                <div class="col-8">
                                    <input class="form-giasu" type="text" name="mon" id="mon">
                                </div>
                            </div>
                            <div class="row mgb5">
                                <div class="col-4 d-flex align-items-center pdr0">
                                    <label class="label-dangkytimgs" for="">Học phí (VND/2 tiếng)</label>
                                </div>
                                <div class="col-8">
                                    <div class="row align-items-center mg0">
                                        <span>Mức giá từ</span><input class="inputgia" type="number" name="hocphimin" id="hocphimin"><span>đến</span><input class="inputgia" type="number" name="hocphimax" id="hocphimax">
                                    </div>
                                </div>
                            </div>
                            <div class="row mgb5">
                                <div class="col-4 d-flex align-items-center pdr0">
                                    <label class="label-dangkytimgs" for="">Yêu cầu trình độ</label>
                                </div>
                                <div class="col-8">
                                    <input class="form-giasu" type="text" name="yeucautrinhdo" id="yeucautrinhdo">
                                </div>
                            </div>
                            <div class="row mgb5 mgt20">
                                <div class="col-4">
                                    
                                </div>
                                <div class="col-8 d-flex justify-content-start">
                                    <button type="submit" class="btn-guiyeucau">Gửi yêu cầu</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="container pd70-0">
            <h2 class="title-danhsachgiasu">Danh sách gia sư <a class="text-dangkygiasu" href="{{route('showadd-Giasudk')}}"><i>Đăng ký làm gia sư</i></a></h2>
            @foreach($giasu as $g)
                <div class="row item-giasu">
                    <div class="col-2 d-flex justify-content-center flex-column">
                        <img class="avatar" src="{{url('/')}}/images/anhgiasudangky/{{$g->anh}}" alt="{{$g->ten}}">
                        <p class="ma-gs">Mã GS: GS38{{$g->id}}</p>
                    </div>
                    <div class="col-10">
                        <div class="thongtin-gs">
                            <div class="row ">
                                <div class="col-6">
                                    <p class="text-thongtin"><span>Họ và tên: </span>{{$g->ten}}</p>
                                    @if($g->bangcap == 1)
                                    <p class="text-thongtin"><span>Học vị/Bằng cấp: </span>Sinh viên</p>
                                    @elseif($g->bangcap == 2)
                                    <p class="text-thongtin"><span>Học vị/Bằng cấp: </span>Cao đẳng</p>
                                    @elseif($g->bangcap == 3)
                                    <p class="text-thongtin"><span>Học vị/Bằng cấp: </span>Đại học</p>
                                    @elseif($g->bangcap == 4)
                                    <p class="text-thongtin"><span>Học vị/Bằng cấp: </span>Thạc sỹ</p>
                                    @endif
                                    <p class="text-thongtin"><span>Môn dạy: </span>@foreach($g->metamon as $m)<label class="mg0">{{$m->mon->ten}}</label> @endforeach</p>
                                    
                                </div>
                                <div class="col-6">
                                    <p class="text-thongtin"><span>Khu vực giảng dạy: </span>@foreach($g->metaquan as $q)<label class="mg0">{{$q->quan->ten}}</label> @endforeach</p>
                                    <p class="text-thongtin"><span>Khối lớp: </span>@foreach($g->metalop as $m)<label class="mg0">{{$m->lop->ten}}</label> @endforeach</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <p class="text-thongtin mgt0"><span>Thành tích cá nhân: </span>{!!$g->thongtin!!}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
            <div class="row">
                <div class="col-12 d-flex justify-content-end pdr30">
                    <a class="xemtoanbo-giasu" href="{{route('frlist-Giasudk')}}">Xem toàn bộ</a>
                </div>
            </div>
        </div>
    </div>
@endsection
