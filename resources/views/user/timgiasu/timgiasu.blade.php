@extends('user.main')
@section('tilte_site', 'Tìm gia sư')
@section('description') 
    Vinastudy là website hàng đầu về học trực tuyến với các chương trình ôn, luyện thi violympic, thi toán violympic... dành cho học sinh tiểu học và trung học cơ sở. 
@endsection
@section('keyword')
    hoc truc tuyen, luyen thi violympic, luyen thi toan violympic, luyen thi violympic truc tuyen
@endsection
@section('content')
<div class="container-fluid bgrf7">
    <div class="container bao-form-dangkygiasu">
        <div class="row d-flex justify-content-center">
            <div class="col-6 text-center pd0">
                <h2 class="title-timgs">Đăng Ký Tìm Gia Sư </h2>
                <form action="" method="post" enctype="multipart/form-data" autocomplete="off">
                    <input type="hidden" name="_token" value="{{csrf_token()}}" /> 
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="box-dangky">
                        <div class="row mgb5">
                            <div class="col-4 d-flex align-items-center pdr0">
                                <label class="label-dangkytimgs" for="">Họ và tên phụ huynh</label>
                            </div>
                            <div class="col-8">
                                <input class="form-giasu" type="text" name="ten" id="ten">
                            </div>
                        </div>
                        <div class="row mgb5">
                            <div class="col-4 d-flex align-items-center pdr0">
                                <label class="label-dangkytimgs" for="">Số điện thoại liên hệ</label>
                            </div>
                            <div class="col-8">
                                <input class="form-giasu" type="text" name="dienthoai" id="dienthoai">
                            </div>
                        </div>
                        <div class="row mgb5">
                            <div class="col-4 d-flex align-items-center pdr0">
                                <label class="label-dangkytimgs" for="">Địa chỉ</label>
                            </div>
                            <div class="col-8">
                                <input class="form-giasu" type="text" name="diachi" id="diachi">
                            </div>
                        </div>
                        <div class="row mgb5">
                            <div class="col-4 d-flex align-items-center pdr0">
                                <label class="label-dangkytimgs" for="">Con tôi học lớp</label>
                            </div>
                            <div class="col-8">
                                <input class="form-giasu" type="text" name="lop" id="lop">
                            </div>
                        </div>
                        <div class="row mgb5">
                            <div class="col-4 d-flex align-items-center pdr0">
                                <label class="label-dangkytimgs" for="">Môn học cần gia sư</label>
                            </div>
                            <div class="col-8">
                                <input class="form-giasu" type="text" name="mon" id="mon">
                            </div>
                        </div>
                        <div class="row mgb5">
                            <div class="col-4 d-flex align-items-center pdr0">
                                <label class="label-dangkytimgs" for="">Học phí (VND/2 tiếng)</label>
                            </div>
                            <div class="col-8">
                                <div class="row align-items-center mg0">
                                    <span>Mức giá từ</span><input class="inputgia" type="number" name="hocphimin" id="hocphimin"><span>đến</span><input class="inputgia" type="number" name="hocphimax" id="hocphimax">
                                </div>
                            </div>
                        </div>
                        <div class="row mgb5">
                            <div class="col-4 d-flex align-items-center pdr0">
                                <label class="label-dangkytimgs" for="">Yêu cầu trình độ</label>
                            </div>
                            <div class="col-8">
                                <input class="form-giasu" type="text" name="yeucautrinhdo" id="yeucautrinhdo">
                            </div>
                        </div>
                        <div class="row mgb5 mgt20">
                            <div class="col-4">
                                
                            </div>
                            <div class="col-8 d-flex justify-content-start">
                                <button type="submit" class="btn-guiyeucau">Gửi yêu cầu</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
