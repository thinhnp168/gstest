<div class="bao-danhmuc">
    <h2 class="title-dm">Danh mục bài viết</h2>
    <ul class="pdl20 pdt20">
        @foreach($cate as $c)
            @if($c->danhmucme == 0)
                <li class="parent"><a href="{{route('tintucdanhsach',[$c->link,$c->id])}}">{{$c->ten}}</a></li>
                @foreach($cate as $a)
                    @if($a->danhmucme == $c->id)
                        <li class="childr"><a href="{{route('tintucdanhsach',[$a->link,$a->id])}}">{{$a->ten}}</a></li>
                    @endif
                @endforeach
            @endif
        @endforeach
    </ul>
</div>