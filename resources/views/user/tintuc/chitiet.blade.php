@extends('user.main')
@section('tilte_site') 
    {{$chitiettintuc->title}}
@endsection
@section('description') 
    {{$chitiettintuc->description}}
@endsection
@section('keyword')
    {{$chitiettintuc->keywords}}
@endsection
@section('content')
<div class="container-fluid bgrf7 gioithieu pdb50">
    <div class="container">
        <div class="row">
            <div class="col-9">
                <h1 class="title">{{$chitiettintuc->ten}}</h1>
            </div>
            <div class="col-3"></div>
        </div>
        <div class="row">
            <div class="col-9">
                <div class="bao-chitiet-noidung">
                    {!!$chitiettintuc->noidung!!}
                </div>
            </div>
            <div class="col-3 bao-danhmuc">
               @include('user.danhmucbaiviet.danhmuc')
            </div>
        </div>
    </div>
</div>
@endsection
