@extends('user.main')

@section('tilte_site') 
    @if(isset($seocate))
        {{$seocate->ten}}
    @else
        Tin tức VinaStudy
    @endif
@endsection
@section('description') 
    @if(isset($seocate))
        {{$seocate->description}}
    @else
        Vinastudy là website hàng đầu về học trực tuyến - Học online với các chương trình ôn thi, luyện thi dành cho học sinh tiểu học và trung học cơ sở...
    @endif
    
@endsection
@section('keyword')
    @if(isset($seocate))
        {{$seocate->keywords}}
    @else
        hoc truc tuyen, luyen thi violympic, luyen thi toan violympic, luyen thi violympic truc tuyen
    @endif
@endsection
@section('content')
<div class="container-fluid bgrf7 gioithieu pdb50">
    <div class="container">
        <div class="row">
            <div class="col-9">
                <h1 class="title">Tin tức</h1>
            </div>
            <div class="col-3"></div>
        </div>
        <div class="row">
            <div class="col-9">
                <div class="bao-noidung">
                    @if(isset($tintucdanhsach))
                        @if(sizeof($tintucdanhsach) == 0 )
                            <div class="row item-tin">
                                <div class="col-4">
                                
                                </div>
                                <div class="col-8">
                                    <p class="title-tintuc">Chưa có thông tin</p>  
                                </div>
                            </div>
                            @else
                                @foreach($tintucdanhsach as $t)
                                    <div class="row item-tin">
                                        <div class="col-4">
                                            <img class="anhbiatintuc" src="{{url('/')}}/images/anhbiabaiviet/{{$t->baiviet->anh}}" alt="{{$t->baiviet->ten}}">
                                        </div>
                                        <div class="col-8">
                                            <a href="{{route('chitiettintuc',[$t->baiviet->link,$t->baiviet->id])}}">
                                                <p class="title-tintuc">{{$t->baiviet->ten}}</p>
                                                <p class="mota-tintuc">{{$t->baiviet->cutmota}}</p>
                                            </a>
                                        </div>
                                    </div>
                                @endforeach
                            {{$tintucdanhsach->render()}}
                        @endif
                    @else
                        @foreach($tintuc as $t)
                            <div class="row item-tin">
                                <div class="col-4">
                                    <img class="anhbiatintuc" src="{{url('/')}}/images/anhbiabaiviet/{{$t->anh}}" alt="{{$t->ten}}">
                                </div>
                                <div class="col-8">
                                    <a href="{{route('chitiettintuc',[$t->link, $t->id])}}">
                                        <p class="title-tintuc">{{$t->ten}}</p>
                                        <p class="mota-tintuc">{{$t->cutmota}}</p>
                                    </a>
                                </div>
                            </div>
                        @endforeach
                        {{$tintuc->render()}}
                    @endif   
                </div>
            </div>
            <div class="col-3 ">
               @include('user.danhmucbaiviet.danhmuc')
            </div>
        </div>
    </div>
</div>
@endsection
