<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lop extends Model
{
    protected $table = 'lops';
    protected $fillable = [
        'id','ten','created_at','updated_at'
    ];
}
