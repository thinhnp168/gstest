<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Hocsinh;
use App\Giasu;
use App\Baiviet;
use App\Danhmucbaiviet;
use Validator;

class HomeController extends Controller
{
    public function Index(){
        $giasu = Giasu::where('status',1)->where('lentrangchu',1)->with([
            'metamon' => function($query){
                $query->with('mon');
            },
            'metalop' => function($query){
                $query->with('lop');
            },
            'metaquan' => function($query){
                $query->with('quan');
            }
        ])->inRandomOrder()->limit(5)->get();  //cái này sau phải sửa lại thêm trường
        return view('user.home.index',[
            'giasu'=>$giasu
        ]);
    }
    public function camon(){
        return view('user.camon.camon');
    }
    public function addHocsinh(Request $request){
        $validator = Validator::make($request->all(), [
            'ten'=>'required',
            'dienthoai'=>'required',
            'diachi'=>'required'
            
        ],[
            'ten.required' => "Tên không được để trống!" ,
            'dienthoai.required' => "Số điện thoại không được để trống!" ,
            'diachi.required' => "Địa chỉ không được để trống!" 
            
        ]);
        if ($validator->fails()) {
            return response()->json([
                'type' => 2,
                'mess' => $validator->errors()->first()
            ]);
        }else{
            if($request->lop == null){
                $request->lop = '';
            }
            if($request->mon == null){
                $request->mon = '';
            }
            if($request->hocphimin == null || $request->hocphimin < 0){
                $request->hocphimin = 0;
            }
            if($request->hocphimax == null || $request->hocphimax <0){
                $request->hocphimax = 0;
            }
            if($request->yeucautrinhdo == null){
                $request->yeucautrinhdo = '';
            }
            
            $addHocsinh = new Hocsinh;
            $addHocsinh->ten = $request->ten;
            $addHocsinh->dienthoai = $request->dienthoai;
            $addHocsinh->diachi = $request->diachi;
            $addHocsinh->lop = $request->lop;
            $addHocsinh->mon = $request->mon;
            $addHocsinh->hocphimin = $request->hocphimin;
            $addHocsinh->hocphimax = $request->hocphimax;
            $addHocsinh->yeucautrinhdo = $request->yeucautrinhdo;
            $addHocsinh->hocphi = 0;
            $addHocsinh->ghichu = '';
            $addHocsinh->trangthai = 0; 
            $addHocsinh->save();
            return response()->json([
                'type' => 1,
                'mess' => 'Gửi thông tin thành công'
            ]);
        }
    }
    public function gioithieu(){
        $gioithieu = Danhmucbaiviet::where('loai',1)->where('xapxep',1)->first();
        $cate= Danhmucbaiviet::where('trangthai',1)->where('loai',0)->get();
        return view('user.gioithieu.gioithieu',[
            'gioithieu2' => $gioithieu,
            'cate' => $cate
        ]);
    }
    public function timgiasu(){
        return view('user.timgiasu.timgiasu');
    }
    
}
