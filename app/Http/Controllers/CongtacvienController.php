<?php

namespace App\Http\Controllers;

use App\Congtacvien;
use Illuminate\Http\Request;
use Validator;

class CongtacvienController extends Controller
{
   public function showaddCongtacvien(){
       return view('user.congtacvien.congtacvien');
   }
   public function addCongtacvien(Request $request){
    $randommagioithieu = Congtacvien::randomMgt();
    $validator = Validator::make($request->all(), [
        'ten'=>'required',
        'dienthoai'=>'required',
        'email'=>'required|email',
    ],[
        'ten.required' => "Tên không được để trống!" ,
        'dienthoai.required' => "Số điện thoại không được để trống" ,
        'email.required' => "Địa chỉ không được để trống!",
        'email.email' => "Email bạn nhập không đúng định dạng!" 
    ]);
    if ($validator->fails()) {
        return response()->json([
            'type' => 2,
            'mess' => $validator->errors()->first()
        ]);
    }else{
        $anh='';
        if( $request->anh != "undefined"){
            $fileimage = $request->anh;
            $tenanh =time().$fileimage->getClientOriginalName();
            $fileimage->move(base_path('public/images/anhcongtacvien/'),$tenanh);
            $anh=$tenanh;
            $addCongtacvien = new Congtacvien;
            $addCongtacvien->ten = $request->ten;
            $addCongtacvien->dienthoai = $request->dienthoai;
            $addCongtacvien->email = $request->email;
            $addCongtacvien->status = 0;
            $addCongtacvien->magioithieu = $randommagioithieu;
            $addCongtacvien->anh = $anh;
            $addCongtacvien->save();
    
            return response()->json([
                'type' => 1,
                'mess' => 'Gửi thông tin thành công'
            ]);
        }else{
            return response()->json([
                'type' => 3,
                'mess' => 'Ảnh đại diện không được để trống.'
            ]);
        }
    }
   }
}
