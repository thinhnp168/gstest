<?php

namespace App\Http\Controllers;

use App\Danhmucbaiviet;
use Illuminate\Http\Request;

class DanhmucbaivietController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Danhmucbaiviet  $danhmucbaiviet
     * @return \Illuminate\Http\Response
     */
    public function show(Danhmucbaiviet $danhmucbaiviet)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Danhmucbaiviet  $danhmucbaiviet
     * @return \Illuminate\Http\Response
     */
    public function edit(Danhmucbaiviet $danhmucbaiviet)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Danhmucbaiviet  $danhmucbaiviet
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Danhmucbaiviet $danhmucbaiviet)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Danhmucbaiviet  $danhmucbaiviet
     * @return \Illuminate\Http\Response
     */
    public function destroy(Danhmucbaiviet $danhmucbaiviet)
    {
        //
    }
}
