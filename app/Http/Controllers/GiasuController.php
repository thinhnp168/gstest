<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Giasudangky;
Use App\Monhoc;
Use App\Lop;
use App\Giasu;
Use App\Quanhuyen;
Use App\Metaquandk;
Use App\Metalopdk;
Use App\Metamondk;
use App\Danhmucbaiviet;
use Validator;


class GiasuController extends Controller
{
    public function showaddGiasudk(){
        $quydinh = Danhmucbaiviet::where('loai',1)->where('xapxep',2)->first();
        $lop = Lop::all();
        $quanhuyen = Quanhuyen::all();
        $monhoc = Monhoc::all();
        foreach($quanhuyen as $d){
            $d->tentimkiem =strtolower(Quanhuyen::removeAccent($d->ten)); 
            $d->tenbodau = str_replace('-','',Quanhuyen::removeTitle($d->ten));
        }
        foreach($monhoc as $m){
            $m->tenbodau = str_replace('-','',Quanhuyen::removeTitle($m->ten));
        }
        foreach($lop as $p){
            $p->tenbodau = str_replace('-','',Quanhuyen::removeTitle($p->ten));
        }
        return view('user.giasu.giasu',[
            'lop' => $lop,
            'quanhuyen' => $quanhuyen,
            'monhoc' => $monhoc,
            'quydinh'=> $quydinh
        ]);
    }
    public function addGiasudk(Request $request){
        $validator = Validator::make($request->all(), [
            'ten'=>'required',
            'bangcap'=>'required',
            'congtac'=>'required',
            'thulao'=>'required',
            'dienthoai'=>'required',
            'email'=>'required|email',
            'thongtin'=>'required'
        ],[
            'ten.required' => "Tên không được để trống!" ,
            'bangcap.required' => "Học vị, bằng cấp  không được để trống!" ,
            'congtac.required' => "Đơn vị công tác không được để trống!" ,
            'thulao.required' => "Mức thù lao không được để trống!" ,
            'dienthoai.required' => "Số điện thoại không được để trống!" ,
            'email.required' => "Email không được để trống!" ,
            'email.email' => "Email bạn nhập không đúng định dạng!" ,
            'thongtin.required' => "Thông tin không được để trống!" 
        ]);
       
        if ($validator->fails()) {
            return response()->json([
                'type' => 2,
                'mess' => $validator->errors()->first()
            ]);
        }else{
            if($request->magioithieu == null){
                $request->magioithieu = '';
            }
            $anh='';
            if( $request->anh != "undefined"){
                $fileimage = $request->anh;
                $tenanh =time().$fileimage->getClientOriginalName();
                $fileimage->move(base_path('public/images/anhgiasudangky/'),$tenanh);
                $anh=$tenanh;
                $giasudangky = new Giasudangky;
                $mon = explode(',', $request->idmon);
                $quan = explode(',', $request->idquan);
                $lop = explode(',', $request->idlop);
                if($mon != null && $quan != null && $lop != null){
                    $giasudangky->ten = $request->ten;
                    $giasudangky->email = $request->email;
                    $giasudangky->magioithieu = $request->magioithieu;
                    $giasudangky->bangcap = $request->bangcap;
                    $giasudangky->congtac = $request->congtac;
                    $giasudangky->thulao = $request->thulao;
                    $giasudangky->thongtin = $request->thongtin;
                    $giasudangky->anh = $anh;
                    $giasudangky->status = 0;
                    $giasudangky->dienthoai = $request->dienthoai;
                    $giasudangky->save();
                    foreach($mon as $k=>$v){
                        $metamondk = new Metamondk;
                        $metamondk->idgiasu = $giasudangky->id;
                        $metamondk->idmon= $v;
                        $metamondk->save();
                    }
                    foreach($quan as $k=>$v){
                        $metaquandk = new Metaquandk;
                        $metaquandk->idgiasu = $giasudangky->id;
                        $metaquandk->idquan = $v;
                        $metaquandk->save();
                    }
                    foreach($lop as $k=>$v){
                        $metalopdk = new Metalopdk;
                        $metalopdk->idgiasu = $giasudangky->id;
                        $metalopdk->idlop = $v;
                        $metalopdk->save();
                    }
        
                    return response()->json([
                        'type' => 1,
                        'mess' => 'Gửi thông tin thành công'
                    ]);
                
                }else{
                    return response()->json([
                        'type' => 0,
                        'mess' => 'Bạn cần chọn ít nhất 1 Môn học, 1 Khu vực và 1 Phương tiện.'
                    ]);
                }
            }else{
                return response()->json([
                    'type' => 3,
                    'mess' => 'Ảnh đại diện không được để trống.'
                ]);
            }
        }
    }
    public function frlistGiasudk(){
        $giasu = Giasu::where('status',1)->with([
            'metamon' => function($query){
                $query->with('mon');
            },
            'metalop' => function($query){
                $query->with('lop');
            },
            'metaquan' => function($query){
                $query->with('quan');
            }
        ])->get();
        
        foreach ($giasu as $g){
            $g->tentimkiem =strtolower(Quanhuyen::removeAccent($g->ten));
        }
        return view('user.danhsachgiasu.danhsach',[
            'giasu'=>$giasu
        ]);
    }
}
