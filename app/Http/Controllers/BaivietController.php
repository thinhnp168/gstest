<?php

namespace App\Http\Controllers;

use App\Baiviet;
use App\Lienket;
use App\Danhmucbaiviet;
use Illuminate\Http\Request;

class BaivietController extends Controller
{
    public function tintuc(){
        $cate= Danhmucbaiviet::where('trangthai',1)->where('loai',0)->get();
        $tintuc = Baiviet::where('trangthai',1)->paginate(10);
        return view('user.tintuc.tintuc',[
            'tintuc' => $tintuc,
            'cate' => $cate
        ]);
    }
    public function chitiettintuc($slug, $id){
        $chitiettintuc = Baiviet::Find($id);
        $cate= Danhmucbaiviet::where('trangthai',1)->where('loai',0)->get();
        return view('user.tintuc.chitiet',[
            'chitiettintuc' => $chitiettintuc,
            'cate' => $cate
        ]);
    }
    public function tintucdanhsach($slug,$id){
        $cate= Danhmucbaiviet::where('trangthai',1)->where('loai',0)->get();
        $seocate = Danhmucbaiviet::find($id);
        $tintucdanhsach = Lienket::where('iddanhmuc', $id)
            ->with('baiviet')
            ->paginate(10);
        return view('user.tintuc.tintuc',[
            'tintucdanhsach' => $tintucdanhsach,
            'cate' => $cate,
            'seocate'=> $seocate
        ]);
    }
}
