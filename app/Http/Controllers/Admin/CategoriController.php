<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Danhmucbaiviet;
use File;
use Illuminate\Support\Facades\Input;
use Paginate;
use App\Lienket;

class CategoriController extends Controller
{
    public function removeAccent($mystring){
        $marTViet=array(
            // Chữ thường
            "à","á","ạ","ả","ã","â","ầ","ấ","ậ","ẩ","ẫ","ă","ằ","ắ","ặ","ẳ","ẵ",
            "è","é","ẹ","ẻ","ẽ","ê","ề","ế","ệ","ể","ễ",
            "ì","í","ị","ỉ","ĩ",
            "ò","ó","ọ","ỏ","õ","ô","ồ","ố","ộ","ổ","ỗ","ơ","ờ","ớ","ợ","ở","ỡ",
            "ù","ú","ụ","ủ","ũ","ư","ừ","ứ","ự","ử","ữ",
            "ỳ","ý","ỵ","ỷ","ỹ",
            "đ","Đ","'",
    
            // Chữ hoa
            "À","Á","Ạ","Ả","Ã","Â","Ầ","Ấ","Ậ","Ẩ","Ẫ","Ă","Ằ","Ắ","Ặ","Ẳ","Ẵ",
            "È","É","Ẹ","Ẻ","Ẽ","Ê","Ề","Ế","Ệ","Ể","Ễ",
            "Ì","Í","Ị","Ỉ","Ĩ",
            "Ò","Ó","Ọ","Ỏ","Õ","Ô","Ồ","Ố","Ộ","Ổ","Ỗ","Ơ","Ờ","Ớ","Ợ","Ở","Ỡ",
            "Ù","Ú","Ụ","Ủ","Ũ","Ư","Ừ","Ứ","Ự","Ử","Ữ",
            "Ỳ","Ý","Ỵ","Ỷ","Ỹ",
            "Đ","Đ","'"
        );
        $marKoDau=array(
            /// Chữ thường
            "a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a",
            "e","e","e","e","e","e","e","e","e","e","e",
            "i","i","i","i","i",
            "o","o","o","o","o","o","o","o","o","o","o","o","o","o","o","o","o",
            "u","u","u","u","u","u","u","u","u","u","u",
            "y","y","y","y","y",
            "d","D","",
            //Chữ hoa
            "A","A","A","A","A","A","A","A","A","A","A","A","A","A","A","A","A",
            "E","E","E","E","E","E","E","E","E","E","E",
            "I","I","I","I","I",
            "O","O","O","O","O","O","O","O","O","O","O","O","O","O","O","O","O",
            "U","U","U","U","U","U","U","U","U","U","U",
            "Y","Y","Y","Y","Y",
            "D","D","",
        );
        return str_replace($marTViet, $marKoDau, $mystring);
    }
    
    public function removeTitle($string,$keyReplace='-'){
        $string		= html_entity_decode($string, ENT_COMPAT, 'UTF-8');
        $string		= mb_strtolower($string, 'UTF-8');
        $string		= $this->removeAccent($string);
        $string 	= trim(preg_replace("/[^A-Za-z0-9]/i"," ",$string)); // khong dau
        $string 	= str_replace(" ","-",$string);
        $string		= str_replace("--","-",$string);
        $string		= str_replace("--","-",$string);
        $string		= str_replace("--","-",$string);
        $string		= str_replace($keyReplace,"-",$string);
        return $string;
    
    }
    public function showaddCate(){
        $danhmucme = Danhmucbaiviet::where('danhmucme',0)->get();
        return view('admin.danhmuc.add',[
            'danhmucme' => $danhmucme
        ]);
    }
    public function addCate(Request $request){
        $this->validate($request,[
            'ten'=>'required',
            'anh'=>'required',
            'title'=>'required',
            'description'=>'required',
            'keywords'=>'required',
            'xapxep'=>'required',
        ],[
            'ten.required' => "Tên người dùng không được để trống!",
            'anh.required' => "Ảnh không được để trống!",
            'title.required' => "Title không được để trống!",
            'description.required' => "Description không được để trống!",
            'keywords.required' => "Keywords không được để trống!",
            'xapxep.required' => "Xắp xếp không được để trống!",
        ]);
        if($request->mota == null){
            $request->mota= '';
        }
        $anh='';
        if( $request->has('anh')){
            $fileimage = $request->anh;
            $tenanh =time().$fileimage->getClientOriginalName();
            $fileimage->move(base_path('public/images/anhbiadanhmuc/'),$tenanh);
            $anh=$tenanh;
        }
        $addCate = new Danhmucbaiviet;
        $addCate->ten = $request->ten;
        $addCate->link =$this->removeTitle($request->ten,'-');
        $addCate->danhmucme = $request->danhmucme;
        
        $addCate->mota = $request->mota;
        $addCate->description = $request->description;
        $addCate->title = $request->title;
        $addCate->keywords = $request->keywords;
        $addCate->xapxep = $request->xapxep;
        $addCate->loai = $request->loai;
        $addCate->anh = $anh;
        $addCate->trangthai = $request->trangthai;
        $addCate->save();
        return redirect()->route('list-Cate')->with('success','Thêm mới thành công.');
    }
    public function listCate(){
        $listCate = Danhmucbaiviet::paginate(10);
        return view('admin.danhmuc.list',[
            'listCate'=>$listCate
        ]);
    }
    public function showeditCate($id){
        $showeditCate = Danhmucbaiviet::Find($id);
        $danhmucme = Danhmucbaiviet::where('danhmucme',0)->get();
        $timdanhmucme = Danhmucbaiviet::Find($showeditCate->danhmucme);
        return view('admin.danhmuc.edit',[
            'showeditCate' => $showeditCate,
            'timdanhmucme' => $timdanhmucme,
            'danhmucme' => $danhmucme
        ]);
    }
    public function editCate($id, Request $request){
        $editCate = Danhmucbaiviet::Find($id);

        $this->validate($request,[
            'ten'=>'required',
            'link'=>'required',
            'title'=>'required',
            'description'=>'required',
            'keywords'=>'required',
            'xapxep'=>'required',
        ],[
            'ten.required' => "Tên người dùng không được để trống!",
            'link.required' => "Tên người dùng không được để trống!",
            'title.required' => "Title không được để trống!",
            'description.required' => "Description không được để trống!",
            'keywords.required' => "Keywords không được để trống!",
            'xapxep.required' => "Xắp xếp không được để trống!",
        ]);
        if($request->mota == null){
            $request->mota= '';
        }
        if( $request->has('anh')){
            $tenanhcu = $editCate->anh;
            unlink(base_path('public/images/anhbiadanhmuc/'.$tenanhcu));
            $fileimage = $request->anh;
            $tenanh =time().$fileimage->getClientOriginalName();
            $fileimage->move(base_path('public/images/anhbiadanhmuc/'),$tenanh);
            $anh=$tenanh;
        }else{
            $anh=$editCate->anh;
        }

        $editCate->ten = $request->ten;
        $editCate->link =$this->removeTitle($request->link,'-');
        $editCate->danhmucme = $request->danhmucme;
        $editCate->mota = $request->mota;
        $editCate->description = $request->description;
        $editCate->title = $request->title;
        $editCate->keywords = $request->keywords;
        $editCate->xapxep = $request->xapxep;
        $editCate->loai = $request->loai;
        $editCate->anh = $anh;
        $editCate->trangthai = $request->trangthai;
        $editCate->save();
        return redirect()->route('list-Cate')->with('success','Chỉnh sửa danh mục thành công.');
    }
    public function deleteCate($id){
        
        $deleteCate = Danhmucbaiviet::Find($id);
        $issetdmc = Danhmucbaiviet::where('danhmucme',$id)->get()->toArray();
        if(sizeof($issetdmc) == 0){
            $lienket = Lienket::where('iddanhmuc',$id)->get();
            foreach($lienket as $l){
                $dellk= Lienket::find($l->id);
                $dellk->delete();
            }
            $tenanhcu = $deleteCate->anh;
            unlink(base_path('public/images/anhbiadanhmuc/'.$tenanhcu));
            $deleteCate->delete();
            return redirect()->route('list-Cate')->with('success','Xoá danh mục thành công.');
        }else{
            return redirect()->back()->with('fail','Danh mục này có danh mục con, không thể xoá được.');
        }
    }
}
