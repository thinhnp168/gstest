<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
    public function showaddUser(){
        return view('admin.user.add');
    }
    public function addUser(Request $request){
        $thanhvien = new User;
        $this->validate($request,[
            'name'=>'required',
            'email'=>'required|email|unique:users,email',
            'password'=>'required|min:6|max:32',
            'phone' => 'required',
            'address'=>'required'
        ],[
             'name.required' => "Tên người dùng không được để trống!" ,
             'email.required' => "Email không được để trống!" ,
             'email.email' => "Email bạn nhập không đúng định dạng!" ,
             'email.unique' => "Email đã tồn tại!" ,
             'password.required' => "Mật Khẩu không được để trống!" ,
             'password.min' => "Mật khẩu phải có ít nhất 3 ký tự!" ,
             'password.max' => "Mật khẩu có nhiều nhất 32 ký tự!" ,
             'phone.required' => "Số điện thoại không được để trống!" ,
             'address.required' => "Địa chỉ không được để trống!"  
        ]);
        $thanhvien->name = $request->name;
        $thanhvien->email = $request->email;
        $thanhvien->password = bcrypt($request->password);
        $thanhvien->phone = $request->phone;
        $thanhvien->address = $request->address;
        $thanhvien->status = $request->status;
        $thanhvien->save();
        return redirect()->route('list-User')->with('success','Thêm mới thành công');
    }
    public function listUser(){
        $user = User::all();
        return view('admin.user.list',[
            'user'=>$user
        ]);
    }
    public function showeditUser($id){
        $showeditUser = User::Find($id);
        return view('admin.user.edit',[
            'showeditUser'=>$showeditUser
        ]);
    }
    public function editUser($id, Request $request){
        if($request->password==''){
            $this->validate($request,[
                'name'=>'required',
                'phone' => 'required',
                'address'=>'required'
            ],[
                'name.required' => "Tên người dùng không được để trống!" ,
                'email.email' => "Email bạn nhập không đúng định dạng!" ,
                'email.unique' => "Email đã tồn tại!" ,
                'phone.required' => "Số điện thoại không được để trống!" ,
                'address.required' => "Địa chỉ không được để trống!" 
            ]);
        }else{
            $this->validate($request,[
                'name'=>'required',
                'password'=>'required|min:6|max:32',
                'phone' => 'required',
                'address'=>'required'
            ],[
                'name.required' => "Tên người dùng không được để trống!" ,
                'password.required' => "Mật Khẩu không được để trống!" ,
                'password.min' => "Mật khẩu phải có ít nhất 3 ký tự!" ,
                'password.max' => "Mật khẩu có nhiều nhất 32 ký tự!" ,
                'phone.required' => "Số điện thoại không được để trống!" ,
                'address.required' => "Địa chỉ không được để trống!" 
            ]);
        }
        $editUser = User::Find($id);
        $editUser->name = $request->name;
        if($request->password == ''){
            $editUser->password = $editUser->password;
        }else{
            $editUser->password = bcrypt($request->password);
        }
        $editUser->phone = $request->phone;
        $editUser->status = $request->status;
        $editUser->address = $request->address;
        $editUser->save();
        return redirect()->route('list-User')->with('success','Chỉnh sửa thành công');
    }
    public function delUser($id){
        $deluser = User::Find($id);
        $deluser-> delete();
        return redirect()->route('list-User')->with('success','Xoá thành công');
    }
}
