<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Danhmucbaiviet;
use App\User;
use App\Baiviet;
use App\Lienket;
use File;
use Auth;

class ProductController extends Controller
{
    public function removeAccent($mystring){
        $marTViet=array(
            // Chữ thường
            "à","á","ạ","ả","ã","â","ầ","ấ","ậ","ẩ","ẫ","ă","ằ","ắ","ặ","ẳ","ẵ",
            "è","é","ẹ","ẻ","ẽ","ê","ề","ế","ệ","ể","ễ",
            "ì","í","ị","ỉ","ĩ",
            "ò","ó","ọ","ỏ","õ","ô","ồ","ố","ộ","ổ","ỗ","ơ","ờ","ớ","ợ","ở","ỡ",
            "ù","ú","ụ","ủ","ũ","ư","ừ","ứ","ự","ử","ữ",
            "ỳ","ý","ỵ","ỷ","ỹ",
            "đ","Đ","'",
    
            // Chữ hoa
            "À","Á","Ạ","Ả","Ã","Â","Ầ","Ấ","Ậ","Ẩ","Ẫ","Ă","Ằ","Ắ","Ặ","Ẳ","Ẵ",
            "È","É","Ẹ","Ẻ","Ẽ","Ê","Ề","Ế","Ệ","Ể","Ễ",
            "Ì","Í","Ị","Ỉ","Ĩ",
            "Ò","Ó","Ọ","Ỏ","Õ","Ô","Ồ","Ố","Ộ","Ổ","Ỗ","Ơ","Ờ","Ớ","Ợ","Ở","Ỡ",
            "Ù","Ú","Ụ","Ủ","Ũ","Ư","Ừ","Ứ","Ự","Ử","Ữ",
            "Ỳ","Ý","Ỵ","Ỷ","Ỹ",
            "Đ","Đ","'"
        );
        $marKoDau=array(
            /// Chữ thường
            "a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a",
            "e","e","e","e","e","e","e","e","e","e","e",
            "i","i","i","i","i",
            "o","o","o","o","o","o","o","o","o","o","o","o","o","o","o","o","o",
            "u","u","u","u","u","u","u","u","u","u","u",
            "y","y","y","y","y",
            "d","D","",
            //Chữ hoa
            "A","A","A","A","A","A","A","A","A","A","A","A","A","A","A","A","A",
            "E","E","E","E","E","E","E","E","E","E","E",
            "I","I","I","I","I",
            "O","O","O","O","O","O","O","O","O","O","O","O","O","O","O","O","O",
            "U","U","U","U","U","U","U","U","U","U","U",
            "Y","Y","Y","Y","Y",
            "D","D","",
        );
        return str_replace($marTViet, $marKoDau, $mystring);
    }
    
    public function removeTitle($string,$keyReplace='-'){
        $string		= html_entity_decode($string, ENT_COMPAT, 'UTF-8');
        $string		= mb_strtolower($string, 'UTF-8');
        $string		= $this->removeAccent($string);
        $string 	= trim(preg_replace("/[^A-Za-z0-9]/i"," ",$string)); // khong dau
        $string 	= str_replace(" ","-",$string);
        $string		= str_replace("--","-",$string);
        $string		= str_replace("--","-",$string);
        $string		= str_replace("--","-",$string);
        $string		= str_replace($keyReplace,"-",$string);
        return $string;
    
    }
    public function showaddProduct(){
        $danhmuc= Danhmucbaiviet::all();
        foreach($danhmuc as $d){
            $d->tentimkiem =strtolower($this->removeAccent($d->ten));
        }
        return view('admin.baiviet.add',[
            'danhmuc' => $danhmuc
        ]);
    }
    public function addProduct(Request $request){
        $this->validate($request,[
            'ten'=>'required',
            'xapxep'=>'required',
            'anh'=>'required',
            'title' => 'required',
            'description'=>'required',
            'keywords'=>'required',
            'mota'=>'required',
            'noidung'=>'required',
        ],[
            'ten.required' => "Tên bài viết không được để trống!" ,
            'xapxep.required' => "Xắp xếp không được để trống!" ,
            'anh.required' => "Ảnh đại diện không được để trống!" ,
            'title.required' => "Tiêu đề không được để trống!" ,
            'description.required' => "Mô tả không được để trống!" , 
            'keywords.required' => "Keywords không được để trống!" , 
            'mota.required' => "Mô tả không được để trống!" , 
            'noidung.required' => "Nội dung không được để trống!" 
        ]);
        $anh='';
        if( $request->has('anh')){
            $fileimage = $request->anh;
            $tenanh =time().$fileimage->getClientOriginalName();
            $fileimage->move(base_path('public/images/anhbiabaiviet/'),$tenanh);
            $anh=$tenanh;
        }
        $addProduct = new Baiviet;
        $dm = $request->iddanhmuc;
        if($dm != null){
            $addProduct->ten = $request->ten;
            $addProduct->idnguoiviet = Auth::User()->id;
            $addProduct->xapxep = $request->xapxep;
            $addProduct->link =$this->removeTitle($request->ten,'-');
            $addProduct->title = $request->title;
            $addProduct->description = $request->description;
            $addProduct->keywords = $request->keywords;
            $addProduct->mota = $request->mota;
            $addProduct->noidung = $request->noidung;
            $addProduct->trangthai = $request->trangthai;
            $addProduct->anh = $anh;
            $addProduct->save();
            foreach($dm as $key=>$value){
                $addlienket = new Lienket;
                $addlienket->idbaiviet = $addProduct->id;
                $addlienket->iddanhmuc = $value;
                $addlienket->save();
            }
            return redirect()->route('list-Product')->with('success','Thêm mới thành công.');
        }else{
            return redirect()->back()->with('fail','Bạn cần phải chọn ít nhất 1 danh mục cho bài viết');
        }
        
        
    }
    public function listProduct(){
        $listProduct = Baiviet::paginate(10);
        return view('admin.baiviet.list',[
            'listProduct' => $listProduct
        ]);
    }
    public function showeditProduct($id){
        $danhmuc= Danhmucbaiviet::all();
        $showeditProduct = Baiviet::Find($id);
        // $lienket = Lienket::where('idbaiviet',$id)->get();
        $lienket = Lienket::getidbaiviet($id);
        foreach($danhmuc as $d){
            $d->tentimkiem =strtolower($this->removeAccent($d->ten));
        }
        return view('admin.baiviet.edit',[
            'showeditProduct' => $showeditProduct,
            'danhmuc' => $danhmuc,
            'lienket' => json_encode($lienket)
        ]);
    }
    public function editProduct($id, Request $request){
        $editProduct = Baiviet::Find($id);
       
        $this->validate($request,[
            'ten'=>'required',
            'xapxep'=>'required',
            'link'=>'required',
            'title' => 'required',
            'description'=>'required',
            'keywords'=>'required',
            'mota'=>'required',
            'noidung'=>'required',
        ],[
            'ten.required' => "Tên bài viết không được để trống!" ,
            'xapxep.required' => "Xắp xếp không được để trống!" ,
            'link.required' => "Link không được để trống!" ,
            'title.required' => "Tiêu đề không được để trống!" ,
            'description.required' => "Mô tả không được để trống!" , 
            'keywords.required' => "Keywords không được để trống!" , 
            'mota.required' => "Mô tả không được để trống!" , 
            'noidung.required' => "Nội dung không được để trống!" 
        ]);
        if( $request->has('anh')){
            $tenanhcu = $editProduct->anh;
            unlink(base_path('public/images/anhbiabaiviet/'.$tenanhcu));
            $fileimage = $request->anh;
            $tenanh =time().$fileimage->getClientOriginalName();
            $fileimage->move(base_path('public/images/anhbiabaiviet/'),$tenanh);
            $anh=$tenanh;
        }else{
            $anh=$editProduct->anh;
        }
        $lienketcu = Lienket::where('idbaiviet',$id)->get()->toArray();
            foreach($lienketcu as $k=>$v){
                $dellk = Lienket::Find($v['id']);
                $dellk->delete();
            }
        $dm = $request->iddanhmuc;
        if($dm != null){
            $editProduct->ten = $request->ten;
            $editProduct->idnguoiviet = Auth::User()->id;
            $editProduct->xapxep = $request->xapxep;
            $editProduct->link =$this->removeTitle($request->link,'-');
            $editProduct->title = $request->title;
            $editProduct->description = $request->description;
            $editProduct->keywords = $request->keywords;
            $editProduct->noidung = $request->noidung;
            $editProduct->mota = $request->mota;
            $editProduct->trangthai = $request->trangthai;
            $editProduct->anh = $anh;
            $editProduct->save();
            foreach($dm as $key=>$value){
                $addlienket = new Lienket;
                $addlienket->idbaiviet = $id;
                $addlienket->iddanhmuc = $value;
                $addlienket->save();
            }
            return redirect()->route('list-Product')->with('success','Chỉnh sửa thành công.');
        }else{
            return redirect()->back()->with('fail','Bạn cần phải chọn ít nhất 1 danh mục cho bài viết');
        }
    }
    public function delProduct($id){
        $delProduct = Baiviet::Find($id);
        $tenanhcu = $delProduct->anh;
        unlink(base_path('public/images/anhbiabaiviet/'.$tenanhcu));
        $delProduct->delete();
        $lienketcu = Lienket::where('idbaiviet',$id)->get()->toArray();
        foreach($lienketcu as $k=>$v){
            $dellk = Lienket::Find($v['id']);
            $dellk->delete();
        }
        return redirect()->route('list-Product')->with('success','Xoá bài viết thành công.');
    }
}
