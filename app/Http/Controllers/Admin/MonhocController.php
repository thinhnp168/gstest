<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Monhoc;


class MonhocController extends Controller
{
    public function showaddMonhoc(){
        return view('admin.monhoc.add');
    }
    public function addMonhoc(Request $request){
        $this->validate($request,[
            'ten'=>'required'
        ],[
            'ten.required' => "Tên môn học không được để trống!"  
        ]);

        $addMonhoc = new Monhoc;
        $addMonhoc->ten = $request->ten;
        $addMonhoc->save();
        return redirect()->route('list-Monhoc')->with('success','Thêm mới môn học thành công.');
    }
    public function listMonhoc(){
        $listMonhoc = Monhoc::all();
        return view('admin.monhoc.list',[
            'listMonhoc' => $listMonhoc
        ]);
    }
    public function showeditMonhoc($id){
        $showeditMonhoc = Monhoc::Find($id);
        return view('admin.monhoc.edit',[
            'showeditMonhoc' => $showeditMonhoc
        ]);
    }
    public function editMonhoc($id, Request $request){
        $this->validate($request,[
            'ten'=>'required'
        ],[
            'ten.required' => "Tên môn học không được để trống!"  
        ]);
        $editMonhoc= Monhoc::Find($id);
        $editMonhoc->ten = $request->ten;
        $editMonhoc->save();
        return redirect()->route('list-Monhoc')->with('success','Chỉnh sửa môn học thành công!');
    }
    public function delMonhoc($id){
        $delMonhoc = Monhoc::Find($id);
        $delMonhoc->delete();
        return redirect()->route('list-Monhoc')->with('success','Xoá môn học thành công!');
    }
}
