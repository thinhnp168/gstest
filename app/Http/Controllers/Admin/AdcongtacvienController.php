<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Congtacvien;
use Paginate;
use Mail;

class AdcongtacvienController extends Controller
{
    public function listCongtacvien(){
        $listCongtacvien = Congtacvien::orderBy('congtacviens.created_at','DESC')->paginate(20);
        return view('admin.congtacvien.list',[
            'listCongtacvien' => $listCongtacvien
        ]);
    }
    public function showeditCongtacvien($id){
        $showeditCongtacvien = Congtacvien::find($id);
        return view('admin.congtacvien.edit',[
            'showeditCongtacvien' => $showeditCongtacvien
        ]);
    }
    public function editCongtacvien($id, Request $request){
        $editCongtacvien = Congtacvien::find($id);
        $this->validate($request,[
            'ten'=>'required',
            'dienthoai'=>'required',
            'email'=>'required'
        ],[
            'ten.required' => "Tên không được để trống!",
            'dienthoai.required' => "Số điện thoại không được để trống!",
            'email.required' => "Email không được để trống!"
        ]);
        $anh='';
        if( $request->has('anh')){
            $tenanhcu = $editCongtacvien->anh;
            unlink(base_path('public/images/anhcongtacvien/'.$tenanhcu));
            $fileimage = $request->anh;
            $tenanh =time().$fileimage->getClientOriginalName();
            $fileimage->move(base_path('public/images/anhcongtacvien/'),$tenanh);
            $anh=$tenanh;
        }else{
            $anh=$editCongtacvien->anh;
        }
        $editCongtacvien->ten = $request->ten;
        $editCongtacvien->dienthoai = $request->dienthoai;
        $editCongtacvien->email = $request->email;
        $editCongtacvien->status = $request->status;
        $editCongtacvien->anh = $anh;
        $editCongtacvien->save();
       
        return redirect()->route('list-Congtacvien')->with('success','Chỉnh sửa thành công');
    }
    public function guimailCongtacvien($id){
        $guimailCongtacvien = Congtacvien::find($id);
        Mail::send('user.mail.mail', ['guimailCongtacvien' => $guimailCongtacvien], function ($m) use ($guimailCongtacvien) {
            $m->from('thinhpt.vinastudy@gmail.com');
            $m->to($guimailCongtacvien->email,'Cộng tác viên')->subject('Gia sư Thành Long - Mã giới thiệu');
        });
        return response()->json([
            'type' => 1,
            'mess' => 'Gửi thành công.'
        ]);
    }
    public function duyetCongtacvien($id){
        $duyetCongtacvien = Congtacvien::find($id);
        if($duyetCongtacvien->status == 0){
            $duyetCongtacvien->status = 1;
        }else{
            $duyetCongtacvien->status = 0;
        }
        $duyetCongtacvien->save();
        Mail::send('user.mail.mail', ['duyetCongtacvien' => $duyetCongtacvien], function ($m) use ($duyetCongtacvien) {
            $m->from('thinhpt.vinastudy@gmail.com');
            $m->to($duyetCongtacvien->email,$duyetCongtacvien->ten)->subject('Gia sư Thành Long - Mã giới thiệu');
        });
        return response()->json([
            'type' => 1,
            'mess' => 'Đổi trạng thái và gửi mail thành công.'
        ]);
    }
    
}
