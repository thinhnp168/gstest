<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Lop;

class LopController extends Controller
{
    public function showaddLop(){
        return view('admin.lop.add');
    }
    public function addlop(Request $request){
        $this->validate($request,[
            'ten'=>'required'
        ],[
            'ten.required' => "Tên phương tiện không được để trống!"  
        ]);
        $addlop = new Lop;
        $addlop->ten = $request->ten;
        $addlop->save();
        return redirect()->route('list-Lop')->with('success','Thêm mới phương tiện thành công.');
    }
    public function listLop(){
        $listLop = Lop::all();
        return view('admin.lop.list',[
            'listLop' => $listLop
        ]);
    }
    public function showeditLop($id){
        $showeditLop = Lop::Find($id);
        return view('admin.lop.edit',[
            'showeditLop' => $showeditLop
        ]);
    }
    public function editLop($id, Request $request){
        $this->validate($request,[
            'ten'=>'required'
        ],[
            'ten.required' => "Tên phương tiện không được để trống!"  
        ]);
        $editLop = Lop::Find($id);
        $editLop->ten = $request->ten;
        $editLop->save();
        return redirect()->route('list-Lop')->with('success','Chỉnh sửa phương tiện thành công.');
    }
    public function delLop($id){
        $delLop = Lop::Find($id);
        $delLop->delete();
        return redirect()->route('list-Lop')->with('success','Xoá phương tiện thành công.');
    }
}
