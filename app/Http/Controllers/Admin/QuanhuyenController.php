<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Quanhuyen;

class QuanhuyenController extends Controller
{
    public function showaddQuanhuyen(){
        return view('admin.quanhuyen.add');
    }
    public function addQuanhuyen(Request $request){
        $this->validate($request,[
            'ten'=>'required'
        ],[
            'ten.required' => "Tên quận huyện không được để trống!" 
        ]);
        $addQuanhuyen = new Quanhuyen;
        $addQuanhuyen->ten = $request->ten;
        $addQuanhuyen->save();
        return redirect()->route('list-Quanhuyen')->with('success','Thêm quận huyện thành công.');
    }
    public function listQuanhuyen(){
        $listQuanhuyen = Quanhuyen::all();
        return view('admin.quanhuyen.list',[
            'listQuanhuyen' => $listQuanhuyen
        ]);
    }
    public function showeditQuanhuyen($id){
        $showeditQuanhuyen = Quanhuyen::Find($id);
        return view('admin.quanhuyen.edit',[
            'showeditQuanhuyen' => $showeditQuanhuyen
        ]);
    }
    public function editQuanhuyen($id, Request $request){
        $this->validate($request,[
            'ten'=>'required'
        ],[
            'ten.required' => "Tên quận huyện không được để trống!" 
        ]);
        $editQuanhuyen = Quanhuyen::Find($id);
        $editQuanhuyen->ten = $request->ten;
        $editQuanhuyen->save();
        return redirect()->route('list-Quanhuyen')->with('success','Chỉnh sửa quận huyện thành công.');
    }
    public function delQuanhuyen($id){
        $delQuanhuyen = Quanhuyen::Find($id);
        $delQuanhuyen->delete();
        return redirect()->route('list-Quanhuyen')->with('success','Xoá quận huyện thành công.');
    }
}
