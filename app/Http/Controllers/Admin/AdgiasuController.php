<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Giasudangky;
use App\Giasu;
Use App\Monhoc;
Use App\Lop;
Use App\Quanhuyen;
use App\Metamon;
use App\Metamondk;
use App\Metaquandk;
use App\Metaquan;
use App\Metalopdk;
use App\Metalop;
use App\Congtacvien;
use App\Madoimoictv;
use App\Metactvgs;
use Paginate;
use Mail;

class AdgiasuController extends Controller
{
    public function listGiasudk(){
        $listGiasudk = Giasudangky::orderBy('status','DESC')->orderBy('giasudangkies.created_at','DESC')
            ->with([
                'metamon' => function($query){
                    $query->with('mon');
                },
                'metalop' => function($query){
                    $query->with('lop');
                },
                'metaquan' => function($query){
                    $query->with('quan');
                },
                    

            ])
            ->paginate(10);
        return view('admin.giasudk.list',[
            'listGiasudk' => $listGiasudk
        ]);
    }
    
    public function listGiasu(Request $request){
        $monhoc = Monhoc::all();
        $quanhuyen = Quanhuyen::all();
        $lop = Lop::all();
        $listGiasu= Giasu::orderBy('giasus.status','DESC')->orderBy('giasus.created_at','DESC')
        ->select(['giasus.*']);
        if($request->has('mon') ){
            $idmon = $request->mon;
            $listGiasu->join('metamons', 'giasus.id', '=', 'metamons.idgiasu')
                ->where('metamons.idmon', $idmon)
                ->with(['metamon' => function($query){
                    $query->select(['id', 'idmon', 'idgiasu'])
                    ->with('mon');
                }]);
        }
        if($request->has('quan')){
            $idquan = $request->quan;
            $listGiasu->join('metaquans', 'giasus.id', '=', 'metaquans.idgiasu')
                ->where('metaquans.idquan', $idquan)
                ->with(['metaquan' => function($query){
                    $query->select(['id', 'idquan', 'idgiasu'])
                        ->with('quan');
                }]);
        }
        if($request->has('lop')){
            $idlop = $request->lop;
            $listGiasu->join('metalops', 'giasus.id', '=', 'metalops.idgiasu')
                ->where('metalops.idlop', $idlop)
                ->with(['metalop' => function($query){
                    $query->select(['id', 'idlop', 'idgiasu'])
                        ->with('lop');
                }]);
        }          
        $listGiasu = $listGiasu->get();
        return view('admin.giasu.list',[
            'listGiasu' => $listGiasu,
            'monhoc'=> $monhoc,
            'quanhuyen'=> $quanhuyen,
            'lop'=>$lop
        ]);
    }
    public function duyetGiasu($id){
        $giasudk = Giasudangky::Find($id);
        $giasu = new Giasu;
        $giasu->ten = $giasudk->ten;
        $giasu->email = $giasudk->email;
        $giasu->magioithieu = $giasudk->magioithieu;
        $giasu->dienthoai = $giasudk->dienthoai;
        $giasu->bangcap = $giasudk->bangcap;
        $giasu->congtac = $giasudk->congtac;
        $giasu->thulao = $giasudk->thulao;
        $giasu->thongtin = $giasudk->thongtin;
        $giasu->anh = $giasudk->anh;
        $giasu->status = 1;
        $giasu->lentrangchu = 0;
        $giasu->save();
        $giasudk->delete();
        
        $metamondk= Metamondk::where('idgiasu',$id)->get();
        
        foreach($metamondk as $k=>$v){
            $metamon = new Metamon;
            $metamon->idgiasu= $giasu->id;
            $metamon->idmon = $v->idmon;
            $metamon->save();
            $metamondkcu = Metamondk::find($v->id);
            $metamondkcu->delete();
        }
        $metaquandk= Metaquandk::where('idgiasu',$id)->get();
        
        foreach($metaquandk as $k=>$v){
            $metaquan = new Metaquan;
            $metaquan->idgiasu= $giasu->id;
            $metaquan->idquan = $v->idquan;
            $metaquan->save();
            $metaquandkcu = Metaquandk::find($v->id);
            $metaquandkcu->delete();
        }
        $metalopdk = Metalopdk::where('idgiasu',$id)->get();
        foreach($metalopdk as $k=>$v){
            $metalop = new Metalop;
            $metalop->idgiasu = $giasu->id;
            $metalop->idlop = $v->idlop;
            $metalop->save();
            $metalopdkcu = Metalopdk::find($v->id);
            $metalopdkcu->delete();
        }
        $congtacvien = Congtacvien::where('magioithieu',$giasu->magioithieu)->first();
        if($congtacvien != null && $congtacvien->status == 1){
            $checkmeta = Metactvgs::where('idgiasu',$id)->get();
            if(sizeof($checkmeta)==0){
                $metactvgs = new Metactvgs;
                $metactvgs->idgiasu = $giasu->id;
                $metactvgs->idcongtacvien = $congtacvien['id'];
                $metactvgs->magioithieu = $giasu->magioithieu;
                $metactvgs->status = 1;
                $metactvgs->save();
            }
            $check = Metactvgs::where('status',1)->get();
            if(sizeof($check) == 5){
                foreach($check as $k){
                    $suameta = Metactvgs::find($k->id);
                    $suameta->status = 0;
                    $suameta->save();
                }
                $madoimoictv = new Madoimoictv;
                $madoimoictv->idcongtacvien = $congtacvien['id'];
                $madoimoictv->madoimoi = Madoimoictv::quickRandomstring();
                $madoimoictv->save();
            
                Mail::send('user.mail.mail-madoimoi', ['congtacvien' => $congtacvien,'madoimoictv'=>$madoimoictv], function ($m) use ($congtacvien,$madoimoictv) {
                    $m->from('thinhpt.vinastudy@gmail.com');
                    $m->to($congtacvien->email,'Cộng tác viên')->subject('Gia sư Thành Long - Thông báo mã đổi mối');
                });
            }
            Mail::send('user.mail.mail-duyetgiasu', ['congtacvien' => $congtacvien,'giasu'=>$giasu], function ($m) use ($congtacvien,$giasu) {
                $m->from('thinhpt.vinastudy@gmail.com');
                $m->to($congtacvien->email,'Cộng tác viên')->subject('Gia sư Thành Long - Thông báo duyệt gia sư');
            });
        }
        
        return response()->json([
            'type' => 1,
            'mess' => 'Duyệt thành công.'
        ]);
    }
    public function showeditGiasu($id){
        $showeditGiasu= Giasu::find($id);
        $lop = Lop::all();
        $quanhuyen = Quanhuyen::all();
        $monhoc = Monhoc::all();
        foreach($quanhuyen as $d){
            $d->tentimkiem =strtolower(Quanhuyen::removeAccent($d->ten)); 
            $d->tenbodau = str_replace('-','',Quanhuyen::removeTitle($d->ten));
        }
        foreach($monhoc as $m){
            $m->tenbodau = str_replace('-','',Quanhuyen::removeTitle($m->ten));
        }
        foreach($lop as $m){
            $m->tenbodau = str_replace('-','',Quanhuyen::removeTitle($m->ten));
        }

        $metamon = Metamon::where('idgiasu',$id)->with('mon')->get();
        foreach($metamon as $m){
            $m->mon->tenbodau = str_replace('-','',Quanhuyen::removeTitle($m->mon->ten));
        }
        $metaquan = Metaquan::where('idgiasu',$id)->with('quan')->get();
        foreach($metaquan as $m){
            $m->quan->tentimkiem =strtolower(Quanhuyen::removeAccent($m->quan->ten)); 
            $m->quan->tenbodau = str_replace('-','',Quanhuyen::removeTitle($m->quan->ten));
        }
        $metalop= Metalop::where('idgiasu',$id)->with('lop')->get();
        foreach($metalop as $m){
            $m->lop->tenbodau = str_replace('-','',Quanhuyen::removeTitle($m->lop->ten));
        }
        return view('admin.giasu.edit',[
            'showeditGiasu'=> $showeditGiasu,
            'lop'=> $lop,
            'quanhuyen'=> $quanhuyen,
            'monhoc'=> $monhoc,
            'metamon'=> $metamon,
            'metaquan'=> $metaquan,
            'metalop'=>$metalop
        ]);
    }
    public function editGiasu($id, Request $request){
        $editGiasu = Giasu::find($id);
        $this->validate($request,[
            'ten'=>'required',
            'bangcap'=>'required',
            'congtac'=>'required',
            'thulao'=>'required',
            'dienthoai'=>'required',
            'email'=>'required|email',
            'thongtin'=>'required'
        ],[
            'ten.required' => "Tên không được để trống!",
            'bangcap.required' => "Học vị, bằng cấp  không được để trống!",
            'congtac.required' => "Đơn vị công tác không được để trống!",
            'thulao.required' => "Mức thù lao không được để trống!",
            'dienthoai.required' => "Số điện thoại không được để trống!",
            'email.required' => "Email không được để trống!" ,
            'email.email' => "Email bạn nhập không đúng định dạng!" ,
            'thongtin.required' => "Thông tin gia sư không được để trống!"
        ]);
    
        $metamoncu = Metamon::where('idgiasu',$id)->get()->toArray();
        $metaquancu = Metaquan::where('idgiasu',$id)->get()->toArray();
        $metalopcu = Metalop::where('idgiasu',$id)->get()->toArray();
        $metamonmoi = $request->idmon;
        $metaquanmoi = $request->idquan;
        $metalopmoi = $request->idlop;
        if($metamonmoi == null || $metaquanmoi == null || $metalopmoi == null){
            return redirect()->back()->with('fail','Môn học, quận huyện,lớp không được để trống.');
        }else{
            if( $request->has('anh')){
                $tenanhcu = $editGiasu->anh;
                unlink(base_path('public/images/anhgiasudangky/'.$tenanhcu));
                $fileimage = $request->anh;
                $tenanh =time().$fileimage->getClientOriginalName();
                $fileimage->move(base_path('public/images/anhgiasudangky/'),$tenanh);
                $anh=$tenanh;
            }else{
                $anh=$editGiasu->anh;
            }
            if($request->magioithieu == null ){
                $request->magioithieu = '';
            }
            $editGiasu->ten=$request->ten;
            $editGiasu->email=$request->email;
            $editGiasu->magioithieu=$request->magioithieu;
            $editGiasu->bangcap=$request->bangcap;
            $editGiasu->congtac=$request->congtac;
            $editGiasu->thulao=$request->thulao;
            $editGiasu->dienthoai=$request->dienthoai;
            $editGiasu->thongtin=$request->thongtin;
            $editGiasu->status= $request->status;
            $editGiasu->lentrangchu= $request->lentrangchu;
            $editGiasu->anh=$anh;
            $editGiasu->save();
            foreach($metamoncu as $k=>$v){
                $delmetamoncu = Metamon::find($v['id']);
                $delmetamoncu->delete();
            }
            foreach($metamonmoi as $k=>$v){
                $metamon = new Metamon;
                $metamon->idgiasu= $id;
                $metamon->idmon = $v;
                $metamon->save();
            }
            foreach($metaquancu as $k=>$v){
                $delmetaquancu = Metaquan::find($v['id']);
                $delmetaquancu->delete();
            }
            foreach($metaquanmoi as $k=>$v){
                $metaquanmoi = new Metaquan;
                $metaquanmoi->idgiasu= $id;
                $metaquanmoi->idquan = $v;
                $metaquanmoi->save();
            }
            foreach($metalopcu as $k=>$v){
                $delmetalopcu = Metalop::find($v['id']);
                $delmetalopcu ->delete();
            }
            foreach($metalopmoi as $k=>$v){
                $metalopmoi = new Metalop;
                $metalopmoi->idgiasu= $id;
                $metalopmoi->idlop = $v;
                $metalopmoi->save();
            }
            return redirect()->route('list-Giasu')->with('success','Chỉnh sửa thành công');
        }
    }
    public function delGiasudk($id){
        $delGiasudk = Giasudangky::Find($id);
        $metamoncu = Metamondk::where('idgiasu',$id)->get()->toArray();
        $metaquancu = Metaquandk::where('idgiasu',$id)->get()->toArray();
        $metalopcu = Metalopdk::where('idgiasu',$id)->get()->toArray();
        foreach($metamoncu as $k=>$v){
            $delmetamoncu = Metamondk::find($v['id']);
            $delmetamoncu->delete();
        }
        foreach($metaquancu as $k=>$v){
            $delmetaquancu = Metaquandk::find($v['id']);
            $delmetaquancu->delete();
        }
        foreach($metalopcu as $k=>$v){
            $delmetalopcu = Metalopdk::find($v['id']);
            $delmetalopcu ->delete();
        }
        $tenanhcu = $delGiasudk->anh;
        unlink(base_path('public/images/anhgiasudangky/'.$tenanhcu));
        $delGiasudk->delete();
        return redirect()->route('list-Giasudk')->with('success','Xoá thành công');
    }
    public function delGiasu($id){
        $delGiasu = Giasu::find($id);
        $metamon = Metamon::where('idgiasu',$id)->get()->toArray();
        $metaquan = Metaquan::where('idgiasu',$id)->get()->toArray();
        $metalop = Metalop::where('idgiasu',$id)->get()->toArray();
        foreach($metamon as $k=>$v){
            $delmetamon = Metamon::find($v['id']);
            $delmetamon->delete();
        }
        foreach($metaquan as $k=>$v){
            $delmetaquan = Metaquan::find($v['id']);
            $delmetaquan->delete();
        }
        foreach($metalop as $k=>$v){
            $delmetalop = Metalop::find($v['id']);
            $delmetalop ->delete();
        }
        $tenanhcu = $delGiasu->anh;
        unlink(base_path('public/images/anhgiasudangky/'.$tenanhcu));
        $delGiasu->delete();
        return redirect()->route('list-Giasu')->with('success','Xoá thành công');
    }
    public function lentrangchuGiasu($id){
        $lentrangchuGiasu = Giasu::find($id);
        if($lentrangchuGiasu->lentrangchu == 0){
            $lentrangchuGiasu->lentrangchu =1;
        }else{
            $lentrangchuGiasu->lentrangchu =0;
        }
        $lentrangchuGiasu->save();
        return response()->json([
            'type' => 1,
            'mess' => 'Chuyển thành công.'
        ]);
    }
    public function chuyentrangthaiGiasu($id){
        $chuyentrangthaiGiasu = Giasu::find($id);
        $congtacvien = Congtacvien::where('magioithieu',$chuyentrangthaiGiasu->magioithieu)->first();
        if($chuyentrangthaiGiasu->status == 0){
            $chuyentrangthaiGiasu->status = 1;
            if($congtacvien != null && $congtacvien->status == 1){
                Mail::send('user.mail.mail-khoa', ['congtacvien' => $congtacvien,'chuyentrangthaiGiasu'=>$chuyentrangthaiGiasu], function ($m) use ($congtacvien,$chuyentrangthaiGiasu) {
                    $m->from('thinhpt.vinastudy@gmail.com');
                    $m->to($congtacvien->email,'Cộng tác viên')->subject('Gia sư Thành Long - Thông báo mở khoá gia sư');
                });
            }
            $chuyentrangthaiGiasu->save();
            return response()->json([
                'type' => 1,
                'mess' => 'Chuyển trạng thái thành công.'
            ]);
        }else{
            $chuyentrangthaiGiasu->status = 0;
            if($congtacvien != null && $congtacvien->status == 1){
                Mail::send('user.mail.mail-mo', ['congtacvien' => $congtacvien,'chuyentrangthaiGiasu'=>$chuyentrangthaiGiasu], function ($m) use ($congtacvien,$chuyentrangthaiGiasu) {
                    $m->from('thinhpt.vinastudy@gmail.com');
                    $m->to($congtacvien->email,'Cộng tác viên')->subject('Gia sư Thành Long - Thông báo khoá gia sư');
                });
            }
            $chuyentrangthaiGiasu->save();
            return response()->json([
                'type' => 1,
                'mess' => 'Chuyển trạng thái thành công.'
            ]);
        }
        return response()->json([
            'type' => 0,
            'mess' => 'Có lỗi sảy ra hãy thử lại!'
        ]);
        
    }
}
