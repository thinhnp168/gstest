<?php

namespace App\Http\Controllers;

use App\Lienket;
use Illuminate\Http\Request;

class LienketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Lienket  $lienket
     * @return \Illuminate\Http\Response
     */
    public function show(Lienket $lienket)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Lienket  $lienket
     * @return \Illuminate\Http\Response
     */
    public function edit(Lienket $lienket)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Lienket  $lienket
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Lienket $lienket)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Lienket  $lienket
     * @return \Illuminate\Http\Response
     */
    public function destroy(Lienket $lienket)
    {
        //
    }
}
