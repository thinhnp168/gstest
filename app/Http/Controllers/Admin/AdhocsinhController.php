<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Hocsinh;
use Paginate;

class AdhocsinhController extends Controller
{
    public function listHocsinh(){
        $listHocsinh = Hocsinh::orderBy('updated_at','ASC')->paginate(10);
        return view('admin.hocsinh.list',[
            'listHocsinh' => $listHocsinh
        ]);
    }
    public function showeditHocsinh($id){
        $showeditHocsinh = Hocsinh::find($id);
        return view('admin.hocsinh.edit',[
            'showeditHocsinh'=>$showeditHocsinh
        ]);
    }
    public function editHocsinh($id, Request $request){

        $this->validate($request,[
            'ten'=>'required',
            'dienthoai'=>'required',
            'diachi'=>'required',
            'lop'=>'required',
            'mon'=>'required',
            'hocphi'=>'required'
        ],[
            'ten.required' => "Tên không được để trống!",
            'dienthoai.required' => "Số điện thoại  không được để trống!",
            'diachi.required' => "Địa chỉ không được để trống!",
            'lop.required' => "Lớp không được để trống!",
            'mon.required' => "Môn học không được để trống!",
            'hocphi.required' => "Học phí không được để trống!"
        ]);

        if($request->hocphimax == null){
            $request->hocphimax= 0;
        }
        if($request->hocphimin == null){
            $request->hocphimin= 0;
        }
        if($request->ghichu == null){
            $request->ghichu= '';
        }
        if($request->yeucautrinhdo == null){
            $request->yeucautrinhdo= '';
        }
        $editHocsinh = Hocsinh::find($id);
        $editHocsinh->ten = $request->ten;
        $editHocsinh->dienthoai = $request->dienthoai;
        $editHocsinh->diachi = $request->diachi;
        $editHocsinh->mon = $request->mon;
        $editHocsinh->hocphimin = $request->hocphimin;
        $editHocsinh->hocphi = $request->hocphi;
        $editHocsinh->hocphimax = $request->hocphimax;
        $editHocsinh->yeucautrinhdo = $request->yeucautrinhdo;
        $editHocsinh->ghichu = $request->ghichu;
        $editHocsinh->trangthai = $request->trangthai;
        $editHocsinh->save();
        return redirect()->route('list-Hocsinh')->with('success','Chỉnh sửa thành công.');
    }
    public function delHocsinh($id){
        $delHocsinh = Hocsinh::find($id);
        $delHocsinh->delete();
        return redirect()->route('list-Hocsinh')->with('success','Xoá học sinh thành công.');
    }
}
