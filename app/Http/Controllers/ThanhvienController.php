<?php

namespace App\Http\Controllers;

use App\Thanhvien;
use Illuminate\Http\Request;
use Auth;

class ThanhvienController extends Controller
{
    public function showlogin(){
        return view('admin.login');
    }
    public function loginadmin(Request $request){
        $this->validate($request,[
            'email'=>'required|email',
            'password'=>'required'
            
        ],[
            'email.required' => "Email không được để trống!" ,
            'email.email' => "Email bạn nhập không đúng định dạng!" ,
            'password.required' => "Mật Khẩu không được để trống!" 
        ]);
        $login=array('email'=>$request->email,'password'=>$request->password);
        if(Auth::attempt($login)){
            return redirect()->route('admin-home')->with('success','Đăng nhập thành công');
        }
        else {
            return redirect()->route('showloginad')->with('fail','Sai tên tài khoản hoặc mật khẩu'); 
        }
    }
    public function logoutadmin(){
        Auth::logout();
        return redirect()->route('showloginad')->with('success','Đăng xuất thành công');
    }
}
