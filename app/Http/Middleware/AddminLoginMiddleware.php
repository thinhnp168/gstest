<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class AddminLoginMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check()) {
            if (Auth::user()->status == 1) {
                return $next($request);
            } else{
                return redirect()->route('showloginad')->with('fail','Bạn không phải admin');
            }  
        } else{
            return redirect()->route('showloginad');
        } 
    }
}
