<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Giasudangky extends Model
{
    protected $table = 'giasudangkies';
    protected $fillable = [
        'id','ten','dienthoai','bangcap','congtac','email','magioithieu','thulao','thongtin','status','created_at','updated_at'
    ];
    public function metamon(){
        return $this->hasMany('App\Metamondk','idgiasu','id');
    }
    public function metaquan(){
        return $this->hasMany('App\Metaquandk','idgiasu','id');
    }
    public function metalop(){
        return $this->hasMany('App\Metalopdk','idgiasu','id');
    }
}
