<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Giasu extends Model
{
    protected $table = 'giasus';
    protected $fillable = [
        'id','ten','dienthoai','bangcap','congtac','email','magioithieu','thulao','thongtin','status','lentrangchu','created_at','updated_at'
    ];
    public function metamon(){
        return $this->hasMany('App\Metamon','idgiasu','id');
    }
    public function metaquan(){
        return $this->hasMany('App\Metaquan','idgiasu','id');
    }
    public function metalop(){
        return $this->hasMany('App\Metalop','idgiasu','id');
    }
    
}
