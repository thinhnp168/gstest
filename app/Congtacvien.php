<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Congtacvien extends Model
{
    protected $table = 'congtacviens';
    protected $fillable = [
        'id','ten','dienthoai','email','anh','magioithieu','status','created_at','updated_at'
    ];
    public static function randomMgt(){
        while(true){
            $randId = rand(100000,999999);
            $ctv = Congtacvien::where('magioithieu',$randId)->get();
            if(sizeof($ctv) > 0){
               $this->randomMgt();
            }
            break;
        }
        return $randId;
    }
}
