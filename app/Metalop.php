<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Metalop extends Model
{
    protected $table = 'metalops';
    protected $fillable = [
        'id','idgiasu','idlop','created_at','updated_at'
    ];
    public function lop(){
        return $this->hasOne('App\Lop','id', 'idlop');
    }
}
