<?php 
namespace App\Helper;

use Illuminate\Http\Request;

class Help 
{
    public static function cut_string($str, $length, $char=" ..."){
        $strlen	= mb_strlen($str, "UTF-8");
        if($strlen <= $length) return $str;
        $substr	= mb_substr($str, 0, $length, "UTF-8");
        if(mb_substr($str, $length, 1, "UTF-8") == " ") return $substr . $char;
        $strPoint= mb_strrpos($substr, " ", "UTF-8");
        if($strPoint < $length - 20) return $substr . $char;
        else return mb_substr($substr, 0, $strPoint, "UTF-8") . $char;
    }
    
}
 ?>