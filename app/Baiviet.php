<?php

namespace App;

use App\Helper\help;
use Illuminate\Database\Eloquent\Model;

class Baiviet extends Model
{
    protected $table = 'baiviets';
    protected $fillable = [
        'id','ten','idnguoiviet','mota','xapxep','link','anh','title','description','keywords','noidung','trangthai','created_at','updated_at'
    ];

    protected $appends = ['cutmota'];
    function danhmucvspro(){
    	return $this->hasOne('\App\Danhmucbaiviet',
                            'id', //khoá chính lấy từ bảng Danhmucbaiviet
                            'iddanhmuc'); //khoá ngoại từ bảng baiviets
    }
    function uservspro(){
    	return $this->hasOne('\App\User',
                            'id', //khoá chính lấy từ bảng user
                            'idnguoiviet'); //khoá ngoại từ bảng baiviets
    }
   
    public function getCutmotaAttribute(){
        return Help::cut_string($this->mota, 75);
    }
}
