<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lienket extends Model
{
    protected $table = 'lienkets';
    protected $fillable = [
        'id','idbaiviet','iddanhmuc','created_at','updated_at'
    ];
    public static function getidbaiviet($id){
        $lienket = Lienket::where('idbaiviet',$id)->get();
        return $lienket;
    }
    public function baiviet(){
        return $this->hasOne('App\Baiviet','id','idbaiviet');
    }
}
