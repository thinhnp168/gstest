<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Madoimoictv extends Model
{
    protected $table = 'madoimoictvs';
    protected $fillable = [
        'id','idcongtacvien','madoimoi','created_at','updated_at'
    ];
    public static function quickRandomstring($length = 16)
{
    $pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

    return substr(str_shuffle(str_repeat($pool, 5)), 0, $length);
}
}
