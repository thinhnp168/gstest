<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Metalopdk extends Model
{
    protected $table = 'metalopdks';
    protected $fillable = [
        'id','idgiasu','idlop','created_at','updated_at'
    ];
    
    public function lop(){
        return $this->hasOne('App\Lop','id', 'idlop');
    }
}
