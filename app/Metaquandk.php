<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Metaquandk extends Model
{
    protected $table = 'metaquandks';
    protected $fillable = [
        'id','idgiasu','idquan','created_at','updated_at'
    ];

    public function quan(){
        return $this->hasOne('App\Quanhuyen', 'id', 'idquan');
    }
}
