<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Danhmucbaiviet extends Model
{
    protected $table = 'danhmucbaiviets';
    protected $fillable = [
        'id','ten','link','anh','trangthai','title','mota','description','keywords','danhmucme','loai','xapxep','created_at','updated_at'
    ];
    public function lienket(){
        return $this->hasMany('App\Lienket','iddanhmuc','id');
    }
}
