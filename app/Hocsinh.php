<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hocsinh extends Model
{
    protected $table = 'hocsinhs';
    protected $fillable = [
        'id','ten','dienthoai','diachi','lop','mon','hocphimin','hocphimax','yeucautrinhdo','trangthai','created_at','updated_at'
    ];
    public static function check_phone($phone){
        $pattern = "/^0([1-9]{1})([0-9]{8,9})$/";
        return preg_match($pattern,$phone);
    }
}
