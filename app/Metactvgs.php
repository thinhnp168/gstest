<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Metactvgs extends Model
{
    protected $table = 'metactvgs';
    protected $fillable = [
        'id','idgiasu','idcongtacvien','status','magioithieu','created_at','updated_at'
    ];
}
