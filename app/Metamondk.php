<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Metamondk extends Model
{
    protected $table = 'metamondks';
    protected $fillable = [
        'id','idgiasu','idmon','created_at','updated_at'
    ];

    public function mon(){
        return $this->hasOne('App\Monhoc', 'id', 'idmon');
    }
}
