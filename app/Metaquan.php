<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Metaquan extends Model
{
    protected $table = 'metaquans';
    protected $fillable = [
        'id','idgiasu','idquan','created_at','updated_at'
    ];
    public function quan(){
        return $this->hasOne('App\Quanhuyen', 'id', 'idquan');
    }
}
